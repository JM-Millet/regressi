{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit graphfft;

  {$MODE Delphi}

interface

uses Classes, Graphics, Messages, Forms, Controls, Menus, ExtCtrls,
     Dialogs, Types, StdCtrls, Grids, Printers, ComCtrls,
     math, spin, fft, sysutils, LCLType,
     laz2_DOM, laz2_XMLRead,  laz2_XMLUtils,
     constreg, maths, regutil, compile, graphker;

const
  margeDebutFin = 5;
  minPeriodeFFT = 32;

type

  { TFGrapheFFT }

  TFGrapheFFT = class(TForm)
    PanelCourbe: TPanel;
    PaintBoxTemps: TPaintBox;
    PaintBoxFrequence: TPaintBox;
    PopupMenuFFT: TPopupMenu;
    OptionsFourierItem: TMenuItem;
    CouleursItem: TMenuItem;
    VariablesItem: TMenuItem;
    CopierGrapheItem: TMenuItem;
    FFTToolBar: TToolBar;
    OptionsBtn: TToolButton;
    ZoomBtn: TToolButton;
    TableauBtn: TToolButton;
    TempsBtn: TToolButton;
    limitesBtn: TToolButton;
    FenetrageBtn: TToolButton;
    ImprimeBtn: TToolButton;
    FFTImageList: TImageList;
    PopupMenuGrid: TPopupMenu;
    CopyGridBtn: TMenuItem;
    PopupMenuTemps: TPopupMenu;
    CopierTempsWMF: TMenuItem;
    CacherTemps: TMenuItem;
    CacherGrid: TMenuItem;
    PeriodeFFTEdit: TEdit;
    FinFFTspin: TSpinEdit;
    DebutFFTspin: TSpinEdit;
    SplitterTemps: TSplitter;
    FenetrageMenu: TPopupMenu;
    HammingBtn: TMenuItem;
    RectBtn: TMenuItem;
    FlatTopBtn: TMenuItem;
    ZoomAutoBtn: TToolButton;
    Imprimer1: TMenuItem;
    Imprimer2: TMenuItem;
    MenuDessin: TPopupMenu;
    DessinSupprimerItem: TMenuItem;
    ProprietesMenu: TMenuItem;
    SelectBtn: TToolButton;
    ValeurCouranteH: TStatusBar;
    LabelX: TLabel;
    LabelY: TLabel;
    SauverLigneItem: TMenuItem;
    SaveFreqItem: TMenuItem;
    SourisMenu: TPopupMenu;
    StandardItem: TMenuItem;
    TexteItem: TMenuItem;
    ReticuleItem: TMenuItem;
    LigneItem: TMenuItem;
    N1: TMenuItem;
    BlackmanBtn: TMenuItem;
    NbreHarmAffSpin: TSpinEdit;
    NaturelleCorrBtn: TMenuItem;
    ToolBarGrandeurs: TToolBar;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    OptionsSonagramme: TPanel;
    DecadeSE: TSpinEdit;
    DecadeLabel: TLabel;
    DecibelCB: TCheckBox;
    PasSonAffEdit: TLabeledEdit;
    FreqmaxEdit: TLabeledEdit;
    FaussesCouleursCB: TCheckBox;
    SonagrammeBtn: TToolButton;
    DureeMenu: TPopupMenu;
    Priode1: TMenuItem;
    out1: TMenuItem;
    PasSonCalculSE: TSpinEdit;
    Label2: TLabel;
    CurseurGrid: TStringGrid;
    Enregistrer1: TMenuItem;
    SaveDialog: TSaveDialog;
    Enregistrergraphe1: TMenuItem;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    harmoniqueAffItem: TMenuItem;
    FreqMaxSonUD: TUpDown;
    procedure FrequenceMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBoxFrequenceClick(Sender: TObject);
    procedure ToolBarGrandeursClick(Sender: TObject);
    procedure ZoomAvantItemClick(Sender: TObject);
    procedure FrequenceMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FrequenceMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure zoomAutoItemClick(Sender: TObject);
    procedure TableauItemClick(Sender: TObject);
    procedure TempsItemClick(Sender: TObject);
    procedure FrequencePaint(Sender: TObject);
    procedure TempsPaint(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OptionsFourierItemClick(Sender: TObject);
    procedure CouleursItemClick(Sender: TObject);
    procedure PopupMenuFFTPopup(Sender: TObject);
    procedure TempsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TempsMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure TempsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PeriodeBtnClick(Sender: TObject);
    procedure PaintBoxFrequenceDblClick(Sender: TObject);
    procedure ToutBtnClick(Sender: TObject);
    procedure FrequenceReduiteCBClick(Sender: TObject);
    procedure copierTableauItemClick(Sender: TObject);
    procedure ImprimeBtnClick(Sender: TObject);
    procedure FenetreBtnClick(Sender: TObject);
    procedure ValeursGridDblClick(Sender: TObject);
    procedure ValeursGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SplitterGridCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure CacherTempsClick(Sender: TObject);
    procedure CacherGridClick(Sender: TObject);
    procedure DebutFFTEditKeyPress(Sender: TObject; var Key: Char);
    procedure DebutFFTEditExit(Sender: TObject);
    procedure DebutFFTspinDownClick(Sender: TObject);
    procedure DebutFFTspinUpClick(Sender: TObject);
    procedure FinFFTspinUpClick(Sender: TObject);
    procedure FinFFTspinDownClick(Sender: TObject);
    procedure CopierItemClick(Sender: TObject);
    procedure ZoomManuelBtnClick(Sender: TObject);
    procedure FenetrageMenuPopup(Sender: TObject);
    procedure DessinSupprimerItemClick(Sender: TObject);
    procedure ProprietesMenuClick(Sender: TObject);
    procedure SelectBtnClick(Sender: TObject);
    procedure TextBtnClick(Sender: TObject);
    procedure ReticuleBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure SaveFreqItemClick(Sender: TObject);
    procedure ReticuleItemClick(Sender: TObject);
    procedure TexteItemClick(Sender: TObject);
    procedure StandardItemClick(Sender: TObject);
    procedure LigneItemClick(Sender: TObject);
    procedure ZoomDebutFinItemClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure NbreHarmAffSpinDownClick(Sender: TObject);
    procedure NbreHarmAffSpinUpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PasSonAffEditExit(Sender: TObject);
    procedure DecibelCBClick(Sender: TObject);
    procedure DecadeSEChange(Sender: TObject);
    procedure FreqmaxEditExit(Sender: TObject);
    procedure pasSonCalculEditExit(Sender: TObject);
    procedure FaussesCouleursCBClick(Sender: TObject);
    procedure SonagrammeBtnClick(Sender: TObject);
    procedure Enregistrer1Click(Sender: TObject);
    procedure Enregistrergraphe1Click(Sender: TObject);
    procedure bitmap1Click(Sender: TObject);
    procedure JPEG1Click(Sender: TObject);
    procedure metafile1Click(Sender: TObject);
    procedure CopierTempsWMFClick(Sender: TObject);
    procedure harmoniqueAffItemClick(Sender: TObject);
    procedure ToolButtonClick(Sender: TObject);
    procedure FreqMaxSonUDChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Integer; Direction: TUpDownDirection);
  
  private
      doubleClick : boolean;
      listePic : array[indiceOrdonnee] of TlistePic;
      indexFonction : integer;
      CourbeCourante : integer;
      CurseurF : TcurseurFrequence;
      CurseurT : TcurseurTemps;
      ZoomEnCours,avecPoint : boolean;
      rectDeplace : Trect;
      ListeY : TstringList;
      PointOld : Tpoint;
      iCourant : integer;
      Reference : Trect;
      XdebutInt,XfinInt : Integer;
      Frequence,Amplitude : vecteur;
      Xdeplace : integer;
      ordonneeFFTgr : TordonneeFFT;
      CurMovePermis : boolean;
      xCurseurEchelleFreq : integer;
      ContinuInclus : boolean;
      majRaiesAfaire : boolean;
      indexSonagramme : integer;
      positionPB : integer;
      MaJTempsAfaire : boolean;
      grapheUtilise : boolean;
      Procedure SetCoordonnee;
      procedure SetCurseurF(c : TcurseurFrequence);
      Procedure TraceReference;
      Procedure AffecteDebutFin;
      procedure TraceDebutFin;
      procedure ChercheReference;
      Procedure MajGrapheFFT;
      function VerifPeriodeFFT(Sender: TObject) : boolean;
      procedure SauverFrequence(f,a : double);
      procedure changeEchelleSon(xnew, xold: integer);
      procedure TracePosition;
      procedure calculSonagramme;
      procedure AffecteToolBar;
  protected
      procedure WMRegMaj(var Msg : TWMRegMessage); message WM_Reg_Maj;
  public
      GrapheFrequence,GrapheTemps : TgrapheReg;
      enveloppeSpectre : boolean;
      procedure litConfig;
      procedure ecritConfigXML(ANode : TDOMNOde);
      procedure litConfigXML(ANode : TDOMNOde);
      procedure ImprimerGraphe(var bas : integer);
      procedure VersLatex(NomFichier : string);
  end;

var FGrapheFFT : TFGrapheFFT;

implementation

uses optFFT, optcolordlg, graphvar, saveHarm;

const
   IconeW : array [Tfenetre] of integer =(8,29,10,30,31);

  {$R *.lfm}

procedure TFgrapheFFT.VersLatex(NomFichier : string);
begin
    if PaintBoxTemps.visible then begin
         grapheTemps.withDebutFin := true;
         grapheTemps.versLatex(nomFichier,'T');
         grapheTemps.withDebutFin := false;
    end;
    if not SonagrammeBtn.down then
         grapheFrequence.versLatex(nomFichier,'F')
end;

procedure TFGrapheFFT.FrequenceMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);

procedure AffichePosition;

procedure PicProche(var x, y: integer;var xr,yr : double);
var
  i,i1,i2 : integer;
  deltaX,X1,X2 : double;
begin with grapheFrequence do
//  if (courbeCourante<0) or (courbeCourante>=courbes.count) then courbeCourante := 0;
  with courbes[CourbeCourante] do begin
  deltaX := (monde[mondeX].maxi-monde[mondeX].mini)/128;
  mondeRT(x,y, iMondeC, xr, yr);
  X1 := xr-deltaX;
  if X1<monde[mondeX].mini then X1 := monde[mondeX].mini;
  X2 := xr+deltaX;
  i1 := debutC-1;
  repeat
      inc(i1);
  until (valX[i1]>=X1);
  i2 := finC+1;
  repeat
      dec(i2);
  until (i1=i2) or (valX[i2]<=X2);
  i := (i1+i2) div 2;
  if i>=Pages[pageCourante].nmes then exit;  // débordement ??
  xr := valX[i];
  yr := valY[i];
  windowRT(xr,yr, iMondeC, x, y);
  for i := i1 to i2 do begin
      if valY[i] > yr then begin
          xr := valX[i];
          yr := valY[i];
      end;
  end;
  if yr<monde[imondeC].maxi/128 then yr := 0;
end end;

var Xr,Yr : double;
    XX,YY : string;
    decale : integer;
begin with GrapheFrequence do begin
      if not(monde[mondeX].defini) or not(monde[mondeY].defini) then exit;
      mondeRT(X,Y,mondeY,Xr,Yr);
      if SonagrammeBtn.down
         then XX := courbes.items[courbeCourante].varX.formatNomEtUnite(xr)
         else XX := grandeurs[cFrequence].FormatNomEtUnite(xr);
      if SonagrammeBtn.down
      then begin
          yr := round(yr/pasFreqSonagramme)*pasFreqSonagramme;
          YY := courbes.items[courbeCourante].varY.formatNomEtUnite(yr);
      end
      else begin
          PicProche(x,y,xr,yr);
          XX := grandeurs[cFrequence].FormatNomEtUnite(xr);
          if monde[mondeY].graduation=gdB
              then YY := courbes.items[courbeCourante].varY.nom+'='+
                      floatToStrF(20*log10(yr),ffGeneral,3,0)+' dB'
              else YY := courbes.items[courbeCourante].varY.formatNomEtUnite(yr);
      end;
      if curseurF=curReticuleF then begin
         decale := PaintBoxFrequence.Top;
         labelX.transparent := false;
         labelY.transparent := false;
         labelX.caption := ' '+XX+' ';
         labelY.caption := ' '+YY+' ';
         labelX.top := limiteCourbe.bottom+4+decale;
         labelY.top := y-8+decale;
         labelY.left := limiteFenetre.left+4;
         labelX.left := x-(labelX.width div 2);
     end;
     AffecteStatusPanel(valeurCouranteH,2,pad(XX,25));
     AffecteStatusPanel(ValeurCouranteH,3,Pad(YY,25));
end end;

var deplacement,isMaxi : boolean;
    hintLoc : string;
    deltaM : double;
begin  // FrequenceMouseMove
   if not grapheFrequence.grapheOK then exit;
   deplacement := ssLeft in Shift;
   hintLoc := hClicDroit;
   with PaintBoxFrequence,grapheFrequence do
   case curseurF of
        curZoomF : if zoomEnCours then begin
            RectangleGr(rectDeplace); // efface l'ancien
            rectDeplace.right := X;
            if SonagrammeBtn.down then rectDeplace.bottom := Y;
            RectangleGr(rectDeplace); // trace le nouveau
        end;
        curFmaxi : if abs(x-xCurseurEchelleFreq)>8 then begin
             changeEchelleX(x,xCurseurEchelleFreq,true);
             xCurseurEchelleFreq := x;
             PaintBoxFrequence.invalidate;
        end;
        curFmaxiSon : if abs(y-xCurseurEchelleFreq)>8 then begin
             changeEchelleSon(y,xCurseurEchelleFreq);
             xCurseurEchelleFreq := y;
        end;
        curReticuleF : begin
           TraceReticule(1); // efface
           affichePosition;
           curseurOsc[1].xc := x;
           curseurOsc[1].yc := y;
           TraceReticule(1); // nouvelle position
           hintLoc := stBarreReticuleFFT;
        end;
        curMoveF : if deplacement
        then begin
            with monde[mondeX] do begin
                 deltaM := (x-rectDeplace.left)/a;
                 if deltaM>mini then deltaM := mini; // ne pas dépasser f=0
                 mini := mini-deltaM;
                 maxi := maxi-deltaM;
                 defini := true;
            end;
            if abs(x-rectDeplace.left)>8 then begin
               paintBoxFrequence.invalidate;
               rectDeplace := rect(X,Y,X,Y);
            end;
        end
        else setCurseurF(curSelectF);
        curSelectF : if deplacement
                  then begin if dessinCourant<>nil then with DessinCourant do begin
                    if posDessinCourant=sdCadre
                      then begin
                         RectangleGr(rectDeplace); // efface
                         AffecteCentreRect(x,y,rectDeplace);
                         RectangleGr(rectDeplace);
                      end
                      else begin
                         LineGr(rectDeplace); // efface
                         affectePosition(x,y,posDessinCourant,shift);
                         rectDeplace := rect(x1i,y1i,x2i,y2i);
                         LineGr(rectDeplace);
                      end;
                  end // with dessinCourant
                  end // deplacement
                  else begin
                      SetDessinCourant(x,y);
                      AffichePosition;
                      if posDessinCourant<>sdNone then begin
                          cursor := crSize;
                          hintLoc := hGlisser;
                          if posDessinCourant=sdCadre then
                              hintLoc := hintLoc+hDbletexte;
                      end
                      else if isAxeX(x,y,isMaxi) and
                              not SonagrammeBtn.down
                         then begin
                            hintLoc := hEchelleFreq;
                            cursor := crHsplit;
                         end
                         else if SonagrammeBtn.down and
                                (x<limiteCourbe.left) then begin
                               hintLoc := hEchelleFreq;
                               cursor := crVsplit;
                         end
                         else cursor := crDefault;
                  end;
        curTexteF,curLigneF : if deplacement then
            with Canvas,dessinCourant do begin
                MoveTo(x1i,y1i);
                LineTo(x2i,y2i); // efface l'ancienne
                AffectePosition(x,y,sdPoint2,shift);
                MoveTo(x1i,y1i);
                LineTo(x2i,y2i);
           end;
   end; // case curseurF
   TgraphicControl(sender).hint := hintLoc;
end; // FrequenceMouseMove

procedure TFGrapheFFT.PaintBoxFrequenceClick(Sender: TObject);
begin

end;

procedure TFGrapheFFT.ToolBarGrandeursClick(Sender: TObject);
begin

end;

procedure TFGrapheFFT.ZoomAvantItemClick(Sender: TObject);
begin
    zoomEnCours := false;
    setCurseurF(curZoomF);
end;

procedure TFGrapheFFT.SetCurseurF(c : TcurseurFrequence);
var i : integer;
begin with PaintBoxFrequence.Canvas do begin
  Pen.style := psSolid;
  Pen.mode := pmNotXor;
  Brush.style := bsClear;
  Brush.Color := clWindow;
  curseurF := c;
  case c of
       curZoomF : begin
          PaintBoxFrequence.cursor := crDefault; //Zoom;
          Pen.style := psDash;
       end;
       curFmaxi : begin
          PaintBoxFrequence.cursor := crHsplit;
       end;
       curFmaxiSon : begin
          PaintBoxFrequence.cursor := crVsplit;
       end;
       curTexteF : begin
          PaintBoxFrequence.cursor := crDefault; //Lettre;
          selectBtn.imageIndex := 25;
       end;
       curLigneF : begin
          PaintBoxFrequence.cursor := crDefault; //Pencil;
          selectBtn.imageIndex := 32;
       end;
       curReticuleF : begin
           PaintBoxFrequence.cursor := crNone;
           ReticuleItem.checked := true;
           selectBtn.imageIndex := 23;
           with grapheFrequence do begin
              for i := 1 to 3 do
                  curseurOsc[i].grandeurCurseur := monde[mondeY].axe;
              cCourant := 1;
           end;   
           ligneRappelCourante := lrReticule;
       end;
       curSelectF : begin
          PaintBoxFrequence.cursor := crDefault;
          selectBtn.imageIndex := 26;
          StandardItem.checked := true;
       end;
       curMoveF : PaintBoxFrequence.cursor := crSizeAll;
    end;{case}
    labelX.Visible := curseurF=curReticuleF;
    labelY.Visible := curseurF=curReticuleF;
    LabelX.Color := fondReticule;
    LabelY.Color := fondReticule;
end end; // setCurseurF

procedure TFGrapheFFT.FrequenceMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var j : integer;
    isMaxi : boolean;
begin
   if grapheFrequence.grapheOK then with grapheFrequence do case curseurF of
       curZoomF : if not(zoomEnCours) and (sender=PaintBoxFrequence) then begin
            if SonagrammeBtn.down
               then rectDeplace := rect(X,Y,X,Y)
               else rectDeplace := rect(X,10,X,PaintBoxFrequence.height-10);
            zoomEnCours := true;
            RectangleGr(rectDeplace);
       end;
       curFmaxi,curFmaxiSon : ;
       curReticuleF : ;
       curSelectF : begin
           if (Button=mbRight) then begin
              grapheFrequence.SetDessinCourant(x,y);
              if dessinCourant=nil
                 then paintBoxFrequence.Popupmenu := popupmenufft
                 else paintBoxFrequence.Popupmenu := menuDessin;
              exit; // clic droit donc menu contextuel
           end;
          for j := 1 to grapheFrequence.NbreOrdonnee do
            SetDessinCourant(x,y);
            if dessinCourant<>nil then with dessinCourant do
               if posDessinCourant=sdcadre
                  then begin
                    rectDeplace := cadre;
                    RectangleGr(rectDeplace);
                  end
                  else begin
                      rectDeplace := rect(x1i,y1i,x2i,y2i);
                      LineGr(rectDeplace);
                  end;
           if curMovePermis and
              (dessinCourant=nil) then begin
                  setCurseurF(curMoveF);
                  rectDeplace := rect(X,Y,X,Y);
              end;
           if (dessinCourant=nil) and
               not SonagrammeBtn.down and
               isAxeX(x,y,ismaxi) then begin
                 xCurseurEchelleFreq := x;
                 SetCurseurF(curFmaxi);
            end;
           if SonagrammeBtn.down and
              (dessinCourant=nil) and
              (x<limiteCourbe.left) then begin
                 xCurseurEchelleFreq := y;
                 SetCurseurF(curFmaxiSon);
            end;
       end;
       curTexteF,curLigneF : if dessinCourant<>nil then
       with dessinCourant do  begin
           affectePosition(x,y,sdPoint1,shift);
           x2i := x1i;
           y2i := y1i;
       end;
  end
end; // FrequenceMouseDown

procedure TFGrapheFFT.FrequenceMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);

Procedure affecteZoom;
begin with grapheFrequence do begin
    RectangleGr(RectDeplace);// efface
    rectDeplace.right := X;
    with rectDeplace do
         if (right=left) then begin
            setCurseurF(CurSelectF);
            exit;
         end;
    affecteZoomAvant(rectDeplace,SonagrammeBtn.down);
    monde[mondeX].ZeroInclus := false;
    monde[mondeX].defini := true;
    monde[mondeX].minDefaut := monde[mondeX].mini;
    monde[mondeX].maxDefaut := monde[mondeX].maxi;
    useDefautX := true;
    if SonagrammeBtn.down then begin
        monde[mondeY].ZeroInclus := false;
        monde[mondeY].defini := true;
        monde[mondeY].minDefaut := monde[mondeY].mini;
        monde[mondeY].maxDefaut := monde[mondeY].maxi;
    end;
    ZoomEnCours :=  false;
    curMovePermis := true;
    setCurseurF(CurSelectF);
    PaintBoxFrequence.invalidate;
end end; // affecteZoom

procedure AffecteDessin;
label fin;
begin
if grapheFrequence.dessinCourant<>nil then
with grapheFrequence.dessinCourant do begin
   with PaintBoxFrequence.Canvas do begin
        MoveTo(x1i,y1i);
        LineTo(x2i,y2i); // efface 
   end;
   if isTexte then if not litOption(grapheFrequence) then begin
      grapheFrequence.dessinCourant.free;
      goto fin;
   end
   else begin
        if (curseurF=curLigneF) and
           ((abs(x1i-x2i)+abs(y1i-y2i))<3) then begin
            afficheErreur(erLigneNulle,0);
            grapheFrequence.dessinCourant.free;
            goto fin;
        end;
   end;
   AffectePosition(x,y,sdPoint2,shift);
   GrapheFrequence.dessins.Add(grapheFrequence.DessinCourant);
   draw;
end;
   fin:
   grapheFrequence.dessinCourant := nil;
   setCurseurF(curSelectF);
end;

var P : Tpoint;
begin // FrequenceMouseUp
with grapheFrequence do begin
   if not grapheOK then exit;
   if Button=mbRight then begin
      P := PaintBoxFrequence.ClientToScreen(point(X,Y));
      if dessinCourant<>nil then MenuDessin.popUp(P.x,P.y);
      exit;
   end;
   case curseurF of
       curZoomF : if zoomEnCours then affecteZoom;
       curFmaxi : begin
           changeEchelleX(x,xCurseurEchelleFreq,true);
           setCurseurF(curSelectF);
           PaintBoxFrequence.invalidate;
       end;
       curFmaxiSon : begin
           changeEchelleSon(y,xCurseurEchelleFreq);
           PaintBoxFrequence.invalidate;
           setCurseurF(curSelectF);
       end;
       curTexteF,curLigneF : affecteDessin;
       curReticuleF : ;
       curMoveF : begin
          setCurseurF(curSelectF);
          monde[mondeX].defini := true;
          monde[mondeX].minDefaut := monde[mondeX].mini;
          monde[mondeX].maxDefaut := monde[mondeX].maxi;
          useDefautX := true;
       end;
       curSelectF : if doubleClick
          then begin
             SetDessinCourant(x,y);
             if (dessinCourant<>nil) then begin
                dessinCourant.litOption(grapheFrequence);
                PaintBoxFrequence.invalidate;
                DessinCourant := nil;
              end;
          end
          else if dessinCourant<>nil then with dessinCourant do begin
               if posDessinCourant=sdCadre
                  then RectangleGr(rectDeplace)
                  else LineGr(rectDeplace);// efface
               AffectePosition(x,y,posDessinCourant,shift);
               dessinCourant := nil;
               paintBoxFrequence.invalidate;
         end;
  end;
  PointOld := point(X,Y);
  doubleClick := false;
end end; // FrequenceMouseUp

procedure TFGrapheFFT.FormCreate(Sender: TObject);
var i : indiceOrdonnee;
begin
  PanelCourbe.DoubleBuffered := True; // pour pas de scintillement ? mais toolBar noire
  enveloppeSpectre := false;
  AvecPoint := true;
  doubleClick := false;
  CurseurGrid.ColWidths[0] := 80;
  CurseurGrid.ColWidths[1] := 80;
  ListeY := TstringList.Create;
  for i := Low(indiceOrdonnee) to High(indiceOrdonnee) do
      listePic[i] := TlistePic.Create;
  GrapheFrequence := TgrapheReg.create;
  GrapheFrequence.optionGraphe := [OgPseudo3D];
  GrapheFrequence.canvas := PaintBoxFrequence.canvas;
  GrapheFrequence.paintBox := PaintBoxFrequence;
  GrapheFrequence.labelYcurseur := labelY;
  GrapheFrequence.modif := [gmXY];
  MajRaiesAfaire := true;
  curseurF := curSelectF;
  curseurT := curNormal;
  GrapheTemps := TgrapheReg.create;
  GrapheTemps.Monde[MondeX].zeroInclus := false;
  GrapheTemps.paintBox := PaintBoxTemps;
  GrapheTemps.canvas := PaintBoxTemps.canvas;
  setLength(Frequence,MaxVecteurDefaut+1);
  setLength(Amplitude,MaxVecteurDefaut+1);
  if (NbreGrandeurs>0) and
     (nomGrandeurTri='') and
     (grandeurs[cFrequence].nom='f') then begin
       if grandeurs[indexTri].nom='f' then grandeurs[cFrequence].nom := 't';
       if grandeurs[indexTri].nom=lambdaMin then grandeurs[cFrequence].nom := sigmaMin;
   end;
   OptionsFFTdlg := TOptionsFFTdlg.Create(self);
   PaintBoxFrequence.canvas.copyMode := cmSrcCopy;
   PaintBoxTemps.canvas.copyMode := cmSrcCopy;
   FaussesCouleursCB.Checked := UseFaussesCouleurs;
   FenetrageBtn.imageIndex := IconeW[fenetre];
   PasSonAffEdit.text := formatReg(pasSonagrammeAff);
   PasSonCalculSE.value := pasSonCalcul;
   freqMaxEdit.text := formatReg(freqMaxSonagramme);
   freqMaxSonUD.position := round(freqMaxSonagramme);
   freqMaxSonUD.increment := round(freqMaxSonagramme/10);
end;

procedure TFGrapheFFT.SetCoordonnee;

procedure SetCoordonneeFFT;
var indexY : integer;
    decale : integer;
    indicePic : indiceOrdonnee;
    avecEnveloppe : boolean;
    freqMax : double;

Procedure AjouteFFT(Acouleur : Tcolor;Apage,colF : integer;Amonde : indiceMonde);
var Ay,Ax : vecteur; // sert au graphe et au tableau
    AyMax : double;

Procedure RemplitColonne(colCourante : integer;nbre : integer);
var ligneCourante : integer;

procedure ajouteUneLigne(Ligne : integer);
begin
      listePic[indicePic].Add(ax[ligne],ay[ligne]);
      inc(ligneCourante);
end;

var ligne,ligne0 : integer;
    z,zprecedent,zsuivant : double;
    maxiA : double;
    i0 : integer;
begin with pages[Apage] do begin
      colCourante := 2*colCourante+1;
      i0 := getFondamental(Nbre,Ay);
      maxiA := Ay[i0]*precisionFFT/1000;
      ligne0 := i0 div 8;
      if ligne0=0 then ligne0 := 1;
      z := Ay[ligne0];
      zsuivant := Ay[ligne0+1];
      listePic[indicePic].index := indexY;
      listePic[indicePic].pasFreq := Ax[1];
      ligneCourante := 2;
      for ligne := succ(ligne0) to (Nbre-2) do begin
           zprecedent := z;
           z := zsuivant;
           zsuivant := Ay[ligne+1];
           if z>maxiA
              then if avecPeriode and (fenetre=rectangulaire)
                 then ajouteUneLigne(ligne)
                 else if (z>zprecedent) and (z>zsuivant)
                      then ajouteUneLigne(ligne);
       end;
       listePic[indicePic].Tri; // selon la valeur alors que le remplissage est selon la fréquence
end end; // RemplitColonne

Procedure AjouteCourbeFreq(var Nbre : integer);
var Acourbe : Tcourbe;

Procedure AddCourbe;
begin with GrapheFrequence do begin
     Acourbe := AjouteCourbe(Ax,Ay,Amonde,
         Nbre,
         grandeurs[cFrequence],grandeurs[indexY],Apage);
     monde[Amonde].graduation := monde[mondeY].graduation;
     monde[Amonde].MinidB := monde[mondeY].MinidB;
     Acourbe.setStyle(Acouleur,psSolid,mCroix);
     if continuInclus
        then Acourbe.debutC := 0
        else Acourbe.debutC := 1;
     Acourbe.decalage := decale;
end end; // AddCourbe

var i : integer;
    harmMax : double;
begin // AjouteCourbeFreq
     if grandeurs[indexY].fonct.genreC<>g_definitionFiltre then begin
          if NbreHarmoniqueOptimise then begin
                harmMax := precisionFFT*Aymax/1000;
                while (Ay[Nbre]<harmMax) do dec(Nbre);
                Nbre := Puiss2Sup(Nbre);
          end;
          if freqMax<Ax[pred(Nbre)] then freqMax := Ax[pred(Nbre)];
// en cas de zoom (avec non puissance de 2)
// soit zero padding et donc la période de calcul est plus grande que la période affichée
// soit extrapolation et donc la fréquence d'échantillonage (et donc le maxi de freq) est
// plus grande que celle initiale
     end;
     if ordonneeFFTgr=oReduite then
         for i := 1 to pred(Nbre) do
             Ay[i] := Ay[i]/AYmax;
     if ordonneeFFTgr=oPuissance then
         for i := 0 to pred(Nbre) do
             Ay[i] := sqr(Ay[i]);
     AddCourbe;
     Acourbe.Adetruire := true;
     if (grandeurs[indexY].fonct.genreC=g_definitionFiltre) or
        (grapheFrequence.monde[mondeY].graduation=gdB)
        then Acourbe.trace := [trLigne]
        else begin
           Acourbe.trace := [trPoint];
           Acourbe.motif := mSpectre;
        end;
end; // AjouteCourbeFreq

Procedure AjouteCourbeEnv;
var Acourbe : Tcourbe;
    i,Nbre,maxi : integer;
    Aa,Af : vecteur;

Procedure CherchePicEnv;
var ligne,i0 : integer;
    z,zprecedent,zsuivant : double;
    maxiA : double;
    ligne0 : integer;
begin
      listePic[indicePic].Nbre := 0;
      i0 := getFondamental(Nbre,Aa);
      maxiA := Aa[i0]*precisionFFT/1000;
      ligne0 := i0 div 8;
      if ligne0=0 then ligne0 := 1;
      z := Aa[ligne0];
      zsuivant := Aa[ligne0+1];
      for ligne := succ(ligne0) to (Nbre-2) do begin
          zprecedent := z;
          z := zsuivant;
          zsuivant := Aa[ligne+1];
          if (z>maxiA) and
             (z>zprecedent) and
             (z>zsuivant) then begin
                listePic[indicePic].Add(Af[ligne],Aa[ligne]);
             end;
      end;  // tous les pics plus grands que maxi*precision
      listePic[indicePic].pasFreq := Af[1];
      listePic[indicePic].Nettoie(FreqMax);
      ListePic[indicePic].Tri;
end; // CherchePicEnv

begin // AjouteCourbeEnv
     with pages[Apage] do begin
         Maxi := 16*NbreFFT;
         if Maxi>MaxMaxVecteur then Maxi := MaxMaxVecteur;
         if Maxi<4096 then Maxi := 4096;
         setlength(Af,Maxi);
         setlength(Aa,Maxi);
         enveloppeFourier(NbreFFT,Maxi,PeriodeFFT,fenetreFourier[indexY],Af,Aa);
         Nbre := Maxi div 2;
     end;
     Acourbe := GrapheFrequence.AjouteCourbe(Af,Aa,Amonde,Nbre,
         grandeurs[cFrequence],grandeurs[indexY],Apage);
     Acourbe.setStyle(Acouleur,psSolid,mLosange);
     if ordonneeFFTgr=oReduite then
         for i := 0 to pred(Nbre) do
             Aa[i] := Aa[i]/aymax;
     if ordonneeFFTgr=oPuissance then
         for i := 0 to pred(Nbre) do
             Aa[i] := sqr(Aa[i]);
     Acourbe.decalage := decale;
     i := 0;
     if not continuInclus then begin
        repeat inc(i)
        until (i>Nbre div 64) or (Aa[i]>Aa[i-1]) or (Aa[i]<aymax);
           // fin du pic continu
     end;
     Acourbe.debutC := i;
     Acourbe.trace := [trLigne];
     Acourbe.dimPointC := 1;
     Acourbe.penWidthC := 1;
     Acourbe.Adetruire := true;
     if Apage=pageCourante then cherchePicEnv;
end; // AjouteCourbeEnv

Procedure AjouteCourbeTemps;
var Acourbe : Tcourbe;
begin
if grandeurs[indexY].fonct.genreC=g_definitionFiltre then exit;
with Pages[Apage] do begin
     If Fenetre in [Hamming,Flattop,Blackman] then begin
        Acourbe := GrapheTemps.AjouteCourbe(valeurLisse[indexTri],fenetreFourier[indexY],
            Amonde,NbreFFT,grandeurs[indexTri],grandeurs[indexY],Apage);
        Acourbe.setStyle(GetCouleurPale(Acouleur),psSolid,mCroix);
        Acourbe.trace := [trLigne];
     end;
     Acourbe := GrapheTemps.AjouteCourbe(valeurVar[indexTri],valeurVar[indexY],
            Amonde,Nmes,grandeurs[indexTri],grandeurs[indexY],Apage);
     Acourbe.setStyle(Acouleur,psSolid,mCroix);
     Acourbe.trace := [trLigne];
end end;

var i,nbre : integer; // ajouteFFT
begin with GrapheFrequence,pages[Apage] do begin
     tri;
     genereSpectreFourierP(indexY);
     if fftReel[indexY]=nil then exit;
     setLength(Ay,NbreFFT div 2 +1);
     for i := 0 to pred(NbreFFT div 2) do
         AY[i] := fftAmpl[indexY,i];
     setLength(Ax,NbreFFT div 2+1);
     copyVecteur(Ax,grandeurs[cFrequence].valeur);
     AYmax := getMaxiFFT(NbreFFT div 2,fftAmpl[indexY]);
     if (colF=0) and (Apage=pageCourante) then if (Ay[0]>AYmax/128)
        then AffecteStatusPanel(ValeurCouranteH,0,
           stContinu+grandeurs[indexY].FormatNomEtUnite(Ay[0]))
        else AffecteStatusPanel(ValeurCouranteH,0,'');
     avecEnveloppe := enveloppeSpectre and
            (NbreFFT<(MaxMaxVecteur div 2));
     Nbre := NbreFFT div 2;
     AjouteCourbeFreq(nbre);
     AjouteCourbeTemps;
     if avecEnveloppe
        then AjouteCourbeEnv
        else if Apage=pageCourante
             then RemplitColonne(colF,nbre);
     inc(decale,decalageFFT);
end end; // ajouteFFT

var
  j : integer;
  couleur,page : integer;
  mondeLoc : indiceMonde;
begin // SetCoordonneeFFT
    freqMax := 0;
    mondeLoc := mondeY;
    courbeCourante := 0;
    with GrapheFrequence do begin
       couleur := 1;
       decale := 0;
       for j := 1 to maxOrdonnee do
           Coordonnee[j].iMondeC := mondeY;
       NbreOrdonnee := listeY.count;
       if NbreOrdonnee>MaxOrdonnee then NbreOrdonnee := MaxOrdonnee;
       for j := 1 to NbreOrdonnee do begin
           coordonnee[j].nomY := ListeY[j-1];
           indexY := IndexNom(listeY[j-1]);
           if j=1 then indexFonction := indexY;
           indicePic := j;
           if superposePage and (NbrePages>1)
             then begin
                for page := 1 to NbrePages do
                  if pages[page].active then begin
                      AjouteFFT(getCouleurPages(page),page,page*(j-1),mondeLoc);
                      if page=pageCourante then courbeCourante := pred(courbes.Count);
                      if OgAnalyseurLogique in OptionGraphe then inc(mondeLoc);
                  end;
             end
             else begin
                AjouteFFT(couleurInit[couleur],pageCourante,j-1,MondeLoc);
                if OgAnalyseurLogique in OptionGraphe then inc(mondeLoc);
                inc(couleur);
             end;
        end;
        grapheOK := true;
        if freqMax>0 then begin
           if (gmXY in modif)
              then begin
                 monde[mondeX].defini := false;
                 monde[mondeX].mini := 0;
                 monde[mondeX].maxi := freqMax;
                 monde[mondeX].minDefaut := 0;
                 monde[mondeX].maxDefaut := freqMax;
                 useDefautX := false;
                 MaJTempsAfaire := tempsBtn.down;
              end
              else if not UseDefautX then begin
                  monde[mondeX].mini := 0;
                  monde[mondeX].maxi := freqMax;
              end;
           if not useDefautX then
              for j := 0 to pred(courbes.count) do with courbes[j] do
                  finC := round(finC*freqMax/valX[finc]);
        end;
        modif := [];
    end; // GrapheFrequence
    with grapheTemps do begin
       grapheOK := true;
       modif := [];
    end; // GrapheTemps
    MajRaiesAfaire := true;
    optionsSonagramme.visible := false;
    optionsBtn.enabled := true;
    fenetrageBtn.enabled := true;
    tableauBtn.enabled := true;
    limitesBtn.enabled := true;
    NbreHarmAffSpin.enabled := true;
    zoomBtn.enabled := true;
    zoomAutoBtn.enabled := true;
    NbreHarmAffSpin.Hint :=
       'Nombre de valeurs d''harmoniques affichées';
end; // SetCoordonneeFFT

procedure SetCoordonneeSonagramme;
var indexY : integer;

Procedure AjouteCourbeSonagramme;
var Acourbe : Tcourbe;
begin with Pages[pageCourante] do begin
     if not decibelCB.checked then DecadeDB := 0; // linéaire
     finFFT := pred(nmes);
     debutFFT := 0;
     indexSonagramme := indexY;
     calculSonagramme;
     Acourbe := GrapheFrequence.AjouteCourbe(
         valeurVar[indexTri],
         grandeurs[cFrequence].valeur,mondeY,Nmes,
         grandeurs[indexTri],grandeurs[cFrequence],pageCourante);
     grapheFrequence.monde[mondeY].graduation := gLin;
     Acourbe.trace := [trSonagramme];
     GrapheFrequence.useDefautX := false;
end end; // AjouteCourbeSonagramme;

var Acourbe : Tcourbe;
begin // SetCoordonneeSonagramme
    with GrapheFrequence do begin
       Coordonnee[1].iMondeC := mondeY;
       NbreOrdonnee := 1;
       coordonnee[1].nomY := ListeY[0];
       indexY := IndexNom(listeY[0]);
       AjouteCourbeSonagramme;
       exclude(OptionGraphe,OgAnalyseurLogique);
       grapheOK := true;
       courbes[0].finC := pred(pages[pageCourante].nmes);
       modif := [];
    end; // GrapheFrequence
    with grapheTemps do begin
       Acourbe := GrapheTemps.AjouteCourbe(pages[pageCourante].valeurVar[indexTri],
                  pages[pageCourante].valeurVar[indexY],
                  mondeY,pages[pageCourante].Nmes,
                  grandeurs[indexTri],grandeurs[indexY],pageCourante);
       Acourbe.setStyle(couleurInit[1],psSolid,mCroix);
       Acourbe.trace := [trLigne];
       grapheOK := true;
       modif := [];
    end; // GrapheTemps
    MajRaiesAfaire := true;
    optionsSonagramme.visible := true;
    optionsBtn.enabled := false;
    fenetrageBtn.enabled := false;
    tableauBtn.enabled := false;
    limitesBtn.enabled := false;
    zoomBtn.enabled := false;
    zoomAutoBtn.enabled := false;
    NbreHarmAffSpin.Hint := 'Indisponible pour le sonagramme';
//  'Zoom temporel|Cliquer sur les flèches pour changer la durée totale du sonagramme';
end; // SetCoordonneeSonagramme

procedure VerifListe;
var i,j,jmax,codeY : integer;
    nomY : string;
    bouton : TToolButton;
begin
   i := 0;
   while i<ListeY.count do begin
       codeY := indexNom(ListeY[i]);
       if (codeY=grandeurInconnue) or
          (codeY=0) or // 0=temps
          (grandeurs[codeY].genreG<>variable)
          then ListeY.delete(i)
          else inc(i)
   end;
   if ListeY.Count=0 then begin
        nomY := '';
        if FgrapheVariab.graphes[1].courbes.count>0
           then nomY := FgrapheVariab.graphes[1].courbes[0].varY.nom;
        if nomY='' then nomY := grandeurs[indexVariab[1]].nom;
        ListeY.add(nomY);
   end;
   jmax := pred(NbreVariab);
   if jmax>=ToolBarGrandeurs.ButtonCount
      then jmax := ToolBarGrandeurs.ButtonCount-1;
   for j := 0 to jmax do begin
       bouton := ToolBarGrandeurs.Buttons[j];
       bouton.down := false;
       for i := 0 to pred(listeY.count) do
          if listeY[i]=bouton.caption then begin
               bouton.down := true;
               break;
          end;
   end;
end;

begin  //  SetCoordonnee
     DefinitGrandeurFrequence;
     if gmXY in grapheFrequence.modif
        then begin
           if sonagrammeBtn.down then tempsBtn.down := true;
           GrapheTemps.reset;
           GrapheFrequence.reset;
           if sonagrammeBtn.down then begin
              GrapheFrequence.monde[mondeX].ZeroInclus := false;
              GrapheFrequence.monde[mondeY].ZeroInclus := true;
           end;
        end
        else begin
           GrapheTemps.courbes.clear;
           GrapheFrequence.courbes.clear;
        end;
     if NbreVariab<2 then begin
        afficheErreur(erNbreVarInf2,0);
        exit;
     end;
     VerifListe;
     if sonagrammeBtn.down
        then SetCoordonneeSonagramme
        else SetCoordonneeFFT;
     tempsItemClick(nil);
end; // SetCoordonnee

procedure TFGrapheFFT.FormResize(Sender: TObject);
begin
    curseurGrid.Top := PanelCourbe.Top;
    curseurGrid.Height := (curseurGrid.rowCount+1)*curseurGrid.rowHeights[1];
    curseurGrid.Left := PanelCourbe.Left+PanelCourbe.width-CurseurGrid.width;
    Refresh
end;

procedure TFGrapheFFT.zoomAutoItemClick(Sender: TObject);
var i,j : integer;
begin with GrapheFrequence do begin
     monde[mondeX].defini := false;
     monde[mondeX].ZeroInclus := true;
     curMovePermis := false;
     useDefautX := false;
     useDefaut := false;
     for i := 0 to pred(courbes.count) do
         with courbes.items[i] do begin
           if continuInclus
              then debutC := 0
              else debutC := 1;
           j := 0;
           repeat inc(j) until valY[j]=0;
           finC := j-1;
           if trace=[trLigne] then
              // cas de la courbe enveloppe à traiter
              finC := MaxMaxVecteur div 2;
     end;
     PaintBoxFrequence.invalidate
end end;

procedure TFGrapheFFT.WMRegMaj(var Msg : TWMRegMessage);
begin with msg do begin
      GrapheFrequence.Modif := [];
      case TypeMaj of
          MajChangePage : if not grapheFrequence.superposePage then
              GrapheFrequence.Modif := [gmValeurs];
          MajSelectPage : if codeMaj<>0 then begin
              GrapheTemps.SuperposePage := CodeMaj>1;
              GrapheFrequence.SuperposePage := CodeMaj>1;
              GrapheFrequence.Modif := [gmValeurs];
          end;
          MajGroupePage :  GrapheFrequence.Modif := [gmValeurs];
          MajGrandeur : begin
               affecteToolBar;
               GrapheFrequence.Modif := [gmXY];
          end;
          MajSuperposition : PaintBoxFrequence.invalidate;
          MajTri : if active then exit else GrapheFrequence.Modif := [gmValeurs];
          MajFichier : begin

             FenetrageBtn.imageIndex := IconeW[fenetre];
             GrapheFrequence.Modif := [gmXY];
             avecPeriode := false;
          end;
          MajValeur,MajValeurConst :  GrapheFrequence.Modif := [gmValeurs];
      end;
      if GrapheFrequence.Modif<>[] then begin
         GrapheTemps.Modif := grapheFrequence.modif;
         MajGrapheFFT;
         if PaintBoxTemps.visible then PaintBoxTemps.Invalidate;
      end;
end end; // WMRegMaj

procedure TFGrapheFFT.TableauItemClick(Sender: TObject);
begin
     GrapheFrequence.monde[mondeX].defini := false;
     refresh;
end;

procedure TFGrapheFFT.TempsItemClick(Sender: TObject);
begin
     TempsBtn.down := not TempsBtn.down;
     repaint
end;

procedure TFGrapheFFT.FrequencePaint(Sender: TObject);

Procedure MajRaies;
var deltay : double;

Procedure TraceMarque(i : integer;j : indiceOrdonnee);
var Dessin : Tdessin;
    xx,yy : double;
begin
    XX := listePic[j].pics[i].valeurF;
    if xx>GrapheFrequence.Monde[mondeX].maxi then exit;
    // pour éviter repliement ?
    YY := ListePic[j].pics[i].valeurH;
    Dessin := Tdessin.create(grapheFrequence);
    with Dessin do begin
            isTexte := true;
            avecLigneRappel := false;
            MotifTexte := mtNone;
            hauteur := 2;
            NumPage := 0;
            vertical := false;
            pen.color := grapheFrequence.coordonnee[j].couleur;
            identification := identRaie;
            x1 := xx;
            if grapheFrequence.monde[mondeY].graduation=gdB
              then yy := 1.03*yy
              else yy := yy+deltay;
            y1 := yy;
            x2 := xx;
            y2 := yy;
            if OgAnalyseurLogique in grapheFrequence.OptionGraphe then begin
               iMonde := mondeY+j-1;
            end;
            Agraphe := grapheFrequence;
            grapheFrequence.dessins.add(Dessin);
            texte.add(grandeurs[cFrequence].formatValeurEtUnite(xx));
      end;
      with grandeurs[cFrequence] do
         CurseurGrid.Cells[0,i+2] := formatNombre(XX*coeffAff);
      with grandeurs[listePic[j].index] do
         CurseurGrid.Cells[1,i+2] := formatNombre(YY*coeffAff);
end; // TraceMarque

Procedure SupprTout;
var i : integer;
begin with grapheFrequence do begin
    i := 0;
    curseurGrid.Visible := false;
    while (i<dessins.count) do
       if dessins[i].identification=identRaie
          then Dessins.remove(dessins[i])
          else inc(i);
end end;

var i,imax : integer;
    j : indiceOrdonnee;
begin // MajRaies
    SupprTout;
    if (NbreHarmoniqueAff=0) or
       not(HarmoniqueAff) or
       SonagrammeBtn.down then exit;
    deltaY := grapheFrequence.monde[mondeY].maxi/32;
     // pour séparation verticale du texte
    if NbreHarmoniqueAff>listePic[1].Nbre
       then imax := listePic[1].nbre
       else imax := NbreHarmoniqueAff;
    CurseurGrid.RowCount := imax+2;
    with grandeurs[cFrequence] do begin
       CurseurGrid.Cells[0,0] := nom;
       CurseurGrid.Cells[0,1] := NomAff(PuissAff);
    end;
    with grandeurs[listePic[1].index] do begin
       CurseurGrid.Cells[1,0] := nom;
       CurseurGrid.Cells[1,1] := NomAff(PuissAff);
    end;
    for j := 1 to grapheFrequence.NbreOrdonnee do begin
        if NbreHarmoniqueAff>listePic[j].Nbre
           then imax := listePic[j].nbre
           else imax := NbreHarmoniqueAff;
        for i := 0 to pred(imax) do traceMarque(i,j);
    end;
    curseurGrid.Visible := true;
    curseurGrid.Top := PanelCourbe.Top;
    curseurGrid.Height := (curseurGrid.rowCount+1)*curseurGrid.rowHeights[1];
    curseurGrid.Left := PanelCourbe.Left+PanelCourbe.width-CurseurGrid.width;
    MajRaiesAfaire := false;
end; // MajRaies

var precFreq,pasFreq : double;
    i,indexMax : integer;
label finProc;
begin // FrequencePaint
if (pageCourante=0) or (NbrePages=0) or
   lectureFichier or lecturePage or grapheUtilise then exit;
if (pages[pageCourante].nmes<16) then begin
       PaintBoxFrequence.Canvas.textOut(PaintBoxFrequence.width div 2,
                                        PaintBoxFrequence.height div 2,
                                        erNmesInf16);
       exit;
end;
SonagrammeBtn.Enabled := pages[pageCourante].nmes >  NbrePointsSonagramme*8;
largeurEcran := (sender as TPaintBox).Width;
if not SonagrammeBtn.Enabled then SonagrammeBtn.down := false;
MajRaiesAfaire := MajRaiesAfaire or (HarmoniqueAffItem.checked<>HarmoniqueAff);
harmoniqueAff := HarmoniqueAffItem.checked;
tailleEcran := PanelCourbe.Height;
with grapheFrequence do begin
        canvas := PaintBoxFrequence.canvas;
        limiteFenetre := PaintBoxFrequence.clientRect;
        superposePage :=  superposePage and (NbrePages>1);
        screen.cursor := crHourGlass;
        if modif<>[] then curseurF := curSelectF;
        if (gmXY in modif) or
           (gmEchelle in modif) or
           (gmValeurs in modif) or
           (pages[pageCourante].periodeFFT=0)
              then setCoordonnee;
        if (pages[pageCourante].periodeFFT=0) and
           not (sonagrammeBtn.down) then begin
             PaintBoxFrequence.Canvas.textOut(PaintBoxFrequence.width div 2,
                                              PaintBoxFrequence.height div 2,
                                              erNmesInf16);
             goto finProc;
        end;
        if not grapheOK or (courbes.count=0) then
           goto finProc;
        Brush.style := bsSolid;
        Brush.color := clWindow;
        if SonagrammeBtn.down then begin
           indexMax := round(FreqMaxSonagramme/pasFreqSonagramme);
           for i := 0 to indexMax do
               grandeurs[cFrequence].valeur[i] := i*pasFreqSonagramme;
           for i := succ(indexMax) to high(grandeurs[cFrequence].valeur) do
               grandeurs[cFrequence].valeur[i] := 0;
           NbreHarmAffSpin.enabled := false;
        end;
        chercheMonde;
        if MajRaiesAfaire then MajRaies;
        canvas.Pen.mode := pmCopy;
        grapheFrequence.draw;
        setCurseurF(curseurF);
        if active then case curseurF of
           curZoomF : if zoomEnCours then RectangleGr(rectDeplace);
           curFmaxi,curFmaxiSon : ;
           curReticuleF : traceReticule(1);
        end; // case curseurF
        try
        if SonagrammeBtn.down then begin
           AffecteStatusPanel(ValeurCouranteH,1,
                DeltaMaj+grandeurs[cFrequence].FormatNomEtUnite(pasFreqSonagramme))
        end
        else begin
           pasFreq := 1/pages[pageCourante].periodeFFT;
           PrecFreq := power(10,floor(log10(pasFreq))-1);
           pasFreq := precFreq*round(pasFreq/precFreq);
           AffecteStatusPanel(ValeurCouranteH,1,
                DeltaMaj+grandeurs[cFrequence].FormatNomEtUnite(pasFreq));
        end;
        except
        end;
        finProc :
        screen.cursor := crDefault;
        if MaJTempsAfaire then PaintBoxTemps.invalidate;
        Modif := [];
end end; // FrequencePaint

procedure TFGrapheFFT.TempsPaint(Sender: TObject);
var zut,finLoc : Integer;
begin // TempsPaint
if (pageCourante=0) or (pages[pageCourante].nmes<16) or
   (NbrePages=0) or LectureFichier or
   not (PaintBoxTemps.visible) or grapheUtilise then exit;
GrapheTemps.canvas := PaintBoxTemps.canvas;
largeurEcran := (sender as TPaintBox).Width;
with PaintBoxTemps.Canvas,grapheTemps do begin
      if (gmXY in modif) or
         (gmEchelle in modif) or
         (gmValeurs in modif) then setCoordonnee;
      if not grapheOK then exit;
      Pen.mode := pmCopy;
      Pen.Color := GetCouleurPages(1);
      Brush.style := bsSolid;
      Brush.color := clWindow;
      limiteFenetre := PaintBoxTemps.clientRect;
      chercheMonde;
      with pages[pageCourante] do begin
          if finFFT<(nmes-2) then finLoc := finFFT+1 else finLoc := pred(nmes);
          // de manière  à ce que le graphe représente la durée
          // alors que le dernier point est à (durée - deltat)
          windowXY(valeurVar[indexTri,finLoc],valeurVar[indexFonction,finLoc],mondeY,XfinInt,zut);
          windowXY(valeurVar[indexTri,debutFFT],valeurVar[indexFonction,debutFFT],mondeY,XdebutInt,zut);
      end;
      draw;
      TraceDebutFin;
      MajTempsAfaire := false;
end end; // TempsPaint

procedure TFGrapheFFT.FormPaint(Sender: TObject);
var avecDebutFin : boolean;
begin
     if TempsBtn.down
        then begin // pas le même ordre !
             PaintBoxTemps.visible := true;
             SplitterTemps.visible := true;
             if PaintBoxTemps.Height<ClientHeight div 3 then
                  PaintBoxTemps.Height := ClientHeight div 3;
             avecDebutFin := not(sonagrammeBtn.down);
        end
        else begin
             SplitterTemps.visible := false;
             PaintBoxTemps.visible := false;
             avecDebutFin := false;
        end;
      periodeFFTedit.enabled := avecDebutFin;
      periodeFFTedit.text := grandeurs[indexTri].FormatValeurEtUnite(pages[pageCourante].periodeFFT);
      finFFTspin.enabled := avecDebutFin;
      debutFFTspin.enabled := avecDebutFin;
      if avecDebutFin then curseurT := curNormal;
end;

Procedure TFGrapheFFT.TraceReference;
begin
     with Reference do
          PaintBoxTemps.Canvas.Rectangle(left,top,right,bottom);
end;

procedure TFGrapheFFT.FormDestroy(Sender: TObject);
var i : indiceOrdonnee;
begin
    GrapheFrequence.Free ;
    GrapheTemps.Free;
    ListeY.Free;
    Frequence := nil;
    Amplitude := nil;
    OptionsFFTDlg.close;
    FGrapheFFT := nil;
    for i := Low(indiceOrdonnee) to High(indiceOrdonnee) do begin
       listePic[i].Clear;
       listePic[i].Free;
    end;
    inherited
end;

Procedure TFgrapheFFT.litConfig;
var i,imax,zint : integer;
    lignesLues : boolean;
    Adessin : Tdessin;
    zbyte : byte;
    zreel : double;
begin
   while (Length(LigneWin)>0) and (ligneWin[1]=symbReg2) do begin
      imax := NbreLigneWin(ligneWin);
      lignesLues := false;
      if pos('Y',ligneWin)<>0 then for i := 1 to imax do begin
           litLigneWin;
           if listeY.count<maxOrdonnee then listeY.add(ligneWin);
           lignesLues := true;
      end;
      if pos('OPTIONS',ligneWin)<>0 then begin
         grapheFrequence.SuperposePage := litBooleanWin;
         grapheTemps.SuperposePage := grapheFrequence.SuperposePage;
         readln(fichier,zint);
         grapheFrequence.monde[mondeY].graduation := Tgraduation(zint);
         litBooleanWin;
         readln(fichier,grapheFrequence.monde[mondeY].MinidB);
         if imax>=5 then readln(fichier);
         if imax>=6 then begin
            readln(fichier,PrecisionFFT);
            if precisionFFT<3 then precisionFFT := 10;
         end;
         if imax>=7 then enveloppeSpectre := litBooleanWin;
         if imax>=8 then SonagrammeBtn.down := litBooleanWin;
         if imax>=9 then NbreHarmoniqueOptimise := litBooleanWin;
         if imax>=10 then begin
             readln(fichier,zint);
             OptionsFFTdlg.ordonneeFFT := TordonneeFFT(zint);
         end;
         if imax>=11 then continuInclus := litBooleanWin;
         if imax>=12 then TempsBtn.down := litBooleanWin;
         if imax>=13 then begin
            readln(fichier,zint);
            PaintBoxTemps.height := zint;
         end;
         if imax>=14 then begin
            readln(fichier,zint);
            DecadeDB := zint;
         end;
         if imax>=15 then begin
            readln(fichier,zint);
            NbreHarmoniqueAff := zint;
         end;
         if imax>=16 then readln(fichier,pasSonagrammeAff);
         if imax>=17 then begin
           readln(fichier,zreel);
           pasSonCalcul := round(zreel);
           if pasSonCalcul<1 then pasSonCalcul := 1;
           if pasSonCalcul>3 then pasSonCalcul := 3;
         end;
         if imax>=18 then readln(fichier,FreqMaxSonagramme);
         for i := 19 to imax do readln(fichier);
         lignesLues := true;
      end;
      if pos('FENETRE',ligneWin)<>0 then begin
              readln(fichier,zByte);
              windowState := TwindowState(zByte);
              readln(fichier,zint);
              top := zint;
              readln(fichier,zint);
              left := zint;
              readln(fichier,zint);
              width := zint;
              readln(fichier,zint);
              height := zint;
              position := poDesigned;
              lignesLues := true;
      end;
      if not(lignesLues) and (pos('F',ligneWin)<>0) then begin
             litLigneWin;
             grandeurs[cFrequence].nom := ligneWin;
             lignesLues := true;
      end;
      if not(lignesLues) and (pos('MEMO',ligneWin)<>0) then
         for i := 1 to imax do begin
             litLigneWin;
             lignesLues := true;
         end;
      if not(lignesLues) and (pos('DESSIN',ligneWin)<>0) then begin
            Adessin := Tdessin.create(grapheFrequence);
            Adessin.load;
            grapheFrequence.dessins.add(Adessin);
            lignesLues := true;
      end;
      if not lignesLues then for i := 1 to imax do readln(fichier);
      litLigneWin
   end;
end; // litConfig

Procedure TFgrapheFFT.ecritConfigXML(ANode : TDOMNode);
var i : integer;
begin
   listeY.delimiter := ' ';
   writeStringXML(ANode,'Y',listeY.DelimitedText);
   writeStringXML(ANode,'X',grandeurs[cFrequence].nom);
   writeBoolXML(ANode,'SuperPage',grapheFrequence.SuperposePage);
   writeIntegerXML(ANode,'Graduation',ord(grapheFrequence.monde[mondeY].graduation));
   writeFloatXML(ANode,'MinidB',grapheFrequence.monde[mondeY].MinidB);
   writeIntegerXML(ANode,'PrecisionFFT',PrecisionFFT);
   writeBoolXML(Anode,'EnveloppeSpectre',enveloppeSpectre);
   writeBoolXML(ANode,'SonagrammeBtn',SonagrammeBtn.down);
   writeBoolXML(ANode,'NbreHarmOpt',NbreHarmoniqueOptimise);
   writeIntegerXML(ANode,'TypeOrdonnee',ord(OptionsFFTdlg.ordonneeFFT));
   writeBoolXML(ANode,'ContinuInclus',continuInclus);
   writeBoolXML(ANode,'TempsBtn',TempsBtn.down);
   writeIntegerXML(ANode,'DecadedB',DecadeDB);
   writeIntegerXML(ANode,'NbreHarmAff',NbreHarmoniqueAff);
   writeFloatXML(ANode,'PasSonagrammeAff',pasSonagrammeAff);
   writeIntegerXML(ANode,'PasSonCalcul',PasSonCalcul);
   writeFloatXML(ANode,'FreqMaxSonagramme',FreqMaxSonagramme);{18}
   for i := 0 to pred(GrapheFrequence.Dessins.count) do begin
       //    GrapheFrequence.dessins.ecritXML(ANode);
   end;
end; // ecritConfigXML

procedure TFGrapheFFT.Enregistrer1Click(Sender: TObject);
begin
    saveDialog.options := saveDialog.options-[ofOverwritePrompt];
    if saveDialog.Execute then begin
       GrapheTemps.withDebutFin := true;
       grapheUtilise := true;
       GrapheTemps.VersFichier(saveDialog.fileName);
       GrapheTemps.withDebutFin := false;
       grapheUtilise := false;

    end;
end;

procedure TFGrapheFFT.Enregistrergraphe1Click(Sender: TObject);
begin
   saveDialog.options := saveDialog.options-[ofOverwritePrompt];
   if saveDialog.Execute then begin
      grapheUtilise := true;
      GrapheFrequence.VersFichier(saveDialog.FileName);
      grapheUtilise := false;
   end;
end;

Procedure TFgrapheFFT.litConfigXML(ANode: TDOMNode);

procedure LoadXMLInReg(ANode: TDOMNode);

procedure Suite;
var Node : TDOMNODE;
begin
    Node := ANode.FirstChild;
    while Node <> Nil do begin
      LoadXMLInReg(Node);
      Node := Node.NextSibling;
    end;
end;


var zint : integer;
    Adessin : Tdessin;
begin
      if ANode.NodeName='Y' then begin
         listeY.Delimiter := ' ';
         listeY.DelimitedText := ANode.NodeValue;
         exit;
      end;
      if ANode.NodeName='SuperPage' then begin
         grapheFrequence.SuperposePage := GetBoolXML(ANode);
         grapheTemps.SuperposePage := grapheFrequence.SuperposePage;
         exit;
      end;
      if ANode.NodeName='Graduation' then begin
         zint := GetIntegerXML(ANode);
         grapheFrequence.monde[mondeY].graduation := Tgraduation(zInt);
         exit;
      end;
      if ANode.NodeName='MinidB' then begin
         grapheFrequence.monde[mondeY].MinidB := getFloatXML(ANode);
         exit
      end;
      if ANode.NodeName='PrecisionFFT' then begin
          PrecisionFFT := getIntegerXML(ANode);
          exit
      end;
      if ANode.NodeName='EnveloppeSpectre' then begin
          enveloppeSpectre := getBoolXML(ANode);
          exit
      end;
      if ANode.NodeName='SonagrammeBtn' then begin
          SonagrammeBtn.down := getBoolXML(ANode);
          exit
      end;
      if ANode.NodeName='NbreHarmOpt' then begin
            NbreHarmoniqueOptimise := getBoolXML(ANode);
            exit
      end;
      if ANode.NodeName='azerty' then begin
           zint := GetIntegerXML(ANode);
           OptionsFFTdlg.ordonneeFFT := TordonneeFFT(zint);
           exit
      end;
      if ANode.NodeName='ContinuInclus' then begin
          continuInclus := getBoolXML(ANode);
          exit;
      end;
      if ANode.NodeName='TempsBtn' then begin
            TempsBtn.down := getBoolXML(ANode);
            exit
      end;
     if ANode.NodeName='azerty' then begin
           PaintBoxTemps.height := getIntegerXML(ANode);
           exit
     end;
     if ANode.NodeName='DecadedB' then begin
            DecadeDB := getIntegerXML(ANode);
            exit
     end;
     if ANode.NodeName='NbreHarmAff' then begin
           NbreHarmoniqueAff := getIntegerXML(ANode);
           exit;
     end;
     if ANode.NodeName='PasSonagrammeAff' then begin
         pasSonagrammeAff := GetFloatXML(ANode);
         exit
     end;
     if ANode.NodeName='PasSonCalcul' then begin
           pasSonCalcul := getIntegerXML(ANode);
          if pasSonCalcul<1 then pasSonCalcul := 1;
          if pasSonCalcul>3 then pasSonCalcul := 3;
          exit
     end;
     if ANode.NodeName='FreqMaxSonagramme' then begin
           FreqMaxSonagramme := getFloatXML(ANode);
           exit;
     end;
     if ANode.NodeName='X' then begin
          grandeurs[cFrequence].nom := ANode.NodeValue;
          exit;
     end;
     if ANode.NodeName='DESSIN' then begin
           Adessin := Tdessin.create(grapheFrequence);
            //Adessin.loadXML;
           grapheFrequence.dessins.add(Adessin);
           exit;
     end;
end;

var Node: TDOMNode;
begin
    Node := ANode.FirstChild;
    while Node <> Nil do begin
      LoadXMLInReg(Node);
      Node := Node.NextSibling;
    end;
end; // litConfigXML

procedure TFGrapheFFT.OptionsFourierItemClick(Sender: TObject);

Procedure SetCoordonneeFFT;
var i : integer;
    OldListeY : TstringList;
    ModifListe : boolean;
begin with GrapheFrequence do begin
           oldListeY := TstringList.Create;
           oldListeY.assign(listeY);
           ListeY.Clear;
           with OptionsFFTDlg.ListeVariableBox do begin
                for i := 0 to pred(items.count) do
                    if selected[i] and (listeY.count<maxOrdonnee)
                       then ListeY.Add(items[i]);
                if listeY.count=0 then ListeY.add(Items[0]);
           end;   // droite
           ModifListe := oldListeY.count<>listeY.count;
           if not modifListe then
              for i := 0 to pred(listeY.count) do
                  if listeY[i]<>oldListeY[i] then modifListe := true;
           if modifListe then include(modif,gmXY);
           oldListeY.Free;
end end;

var i : integer;
begin
    with OptionsFFTdlg do begin
          if (sender=VariablesItem) then pageControl.ActivePage := OrdonneeTS;
          superPagesCB.checked := grapheFrequence.SuperposePage;
          ordonneeFFT := ordonneeFFTgr;
          OptionsFFTdlg.DecibelCB.Checked := not SonagrammeBtn.down and
                    (grapheFrequence.monde[mondeY].graduation=gdB);
          ContinuCB.checked := ContinuInclus;
          MiniDecibel := grapheFrequence.monde[mondeY].MinidB;
          with ListeVariableBox do begin
               Items.Clear;
               for i := 1 to pred(NbreGrandeurs) do with grandeurs[i] do
                  if (genreG=variable) and
                     (fonct.genreC<>g_texte) then Items.add(nom);
               for i := 0 to pred(items.count) do
                   selected[i] := listeY.indexOf(items[i])>=0;
          end;
          if OptionsFFTDlg.showModal=mrOK then begin
             FenetrageBtn.imageIndex := IconeW[fenetre];
             recalculFourierE;
             GrapheFrequence.modif := [gmEchelle];
             grapheFrequence.SuperposePage := superPagesCB.checked;
             grapheTemps.SuperposePage := grapheFrequence.SuperposePage;
             ContinuInclus := ContinuCB.checked;
             if OptionsFFTdlg.DecibelCB.checked and not SonagrammeBtn.down
                then grapheFrequence.monde[mondeY].graduation := gdB
                else grapheFrequence.monde[mondeY].graduation := gLin;
             grapheFrequence.monde[mondeY].MinidB := MiniDecibel;
             ordonneeFFTgr := ordonneeFFT;
             SetCoordonneeFFT;
             GrapheTemps.modif := GrapheFrequence.modif;
             HarmoniqueAffItem.checked := harmoniqueAff;
             debutFFTspin.Visible := avecReglagePeriode;
             PeriodeFFTEdit.Visible := avecReglagePeriode;
             finFFTspin.Visible := avecReglagePeriode;
             MajGrapheFFT;
         end;
     end; // OptionsFourierItemClick
end;

procedure TFGrapheFFT.CopierTempsWMFClick(Sender: TObject);
begin
     GrapheTemps.withDebutFin := true;
     grapheUtilise := true;
     GrapheTemps.VersPressePapier(grapheClip);
     GrapheTemps.withDebutFin := false;
     grapheUtilise := false;
end;

procedure TFGrapheFFT.CouleursItemClick(Sender: TObject);
begin
     OptionCouleurDlg := TOptionCouleurDlg.create(self);
     OptionCouleurDlg.DlgGraphique := GrapheFrequence;
     if optionCouleurDlg.ShowModal=mrOK then refresh;
     OptionCouleurDlg.free;
end;

procedure TFGrapheFFT.PopupMenuFFTPopup(Sender: TObject);
begin
     SaveFreqItem.visible := curseurF=curReticuleF;
end;

procedure TFGrapheFFT.TempsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if Button=mbRight then exit;
   curseurT := curNormal;
   if abs(x-XfinInt)<margeDebutFin then curseurT := curFin;
   if abs(x-XdebutInt)<margeDebutFin then curseurT := curDebut;
   if (curseurT=curNormal) and
      (x>xDebutInt) and
      (x<xfinInt) then curseurT := curDeplace;
   case curseurT of
       curNormal : PaintBoxTemps.Cursor := crDefault;
       curDeplace : PaintBoxTemps.Cursor := crHandPoint;
       else PaintBoxTemps.Cursor := crHsplit;
   end;
   if curseurT<>curNormal then ChercheReference;
   Xdeplace := X;   
end;

procedure TFGrapheFFT.TempsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var DeltaMini,dX : integer;
begin
     if curseurT=curNormal
        then if (abs(X-XfinInt)<margeDebutFin) or (abs(X-XdebutInt)<margeDebutFin)
             then PaintBoxTemps.cursor := crHsplit
             else if (x>xDebutInt) and (x<xFinInt)
                  then PaintBoxTemps.cursor := crHandPoint
                  else PaintBoxTemps.cursor := crDefault
        else begin
          deltaMini := Round(PaintBoxTemps.Width*16/pages[pageCourante].Nmes);
          traceReference; // efface
          traceDebutFin; // efface l'ancien 
          case curseurT of
               curDebut : if X<(XfinInt-DeltaMini) then XdebutInt := X;
               curFin : if X>(XdebutInt+DeltaMini) then XfinInt := X;
               curDeplace : begin
                  dX := X-Xdeplace;
                  if ((xdebutInt+dX)>grapheTemps.limiteCourbe.left) and
                     ((xfinInt+dX)<grapheTemps.limiteCourbe.right) then begin
                    xdebutInt := xdebutInt + dX;
                    xfinInt := xfinInt + dX;
                    Xdeplace := X;
                  end;
               end;
          end;
          traceDebutFin; // trace le nouveau
          chercheReference;
       end;
end; // TempsMouseMove

procedure TFGrapheFFT.TempsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var xf,xd,yy : double;
    zut : integer;
begin
      if Button=mbRight then exit;
      PaintBoxTemps.Cursor := crDefault;
      TraceReference; // efface
      with GrapheTemps do begin
          case curseurT of
            curDebut : begin
               XdebutInt := x;
               if (xdebutInt<grapheTemps.limiteCourbe.left) then
                   xdebutInt := grapheTemps.limiteCourbe.left;
            end;
            curFin : begin
               XfinInt := x;
               if (xfinInt>grapheTemps.limiteCourbe.right) then
                   xfinInt := grapheTemps.limiteCourbe.right;
            end;
            curDeplace : ;
          end;
          curseurT := curNormal;
          mondeXY(XdebutInt,0,mondeY,xd,yy);
          mondeXY(XfinInt,0,mondeY,xf,yy);
      end;
      with pages[pageCourante] do begin
          periodeFFT := xf-xd;
          DebutFFT := 0;
          while (debutFFT<pred(nmes)) and
                (valeurVar[indexTri,debutFFT]<xd) do inc(debutFFT);
// point(debutFFT) >= debut
          xf := valeurVar[indexTri,debutFFT] + periodeFFT;
          FinFFT := pred(nmes);
          while (finFFT>debutFFT) and
                (valeurVar[indexTri,finFFT]>=xf) do dec(finFFT);
// point(finFFT) < fin
          if finFFT<(debutFFT+minPeriodeFFT) then
             if debutFFT<nmes div 2
                then begin
                   finFFT := debutFFT+minPeriodeFFT;
                   grapheTemps.windowXY(valeurVar[indexTri,debutFFT],0,mondeY,XfinInt,zut);
                end
                else begin
                   debutFFT := finFFT-minPeriodeFFT;
                   grapheTemps.windowXY(valeurVar[indexTri,debutFFT],0,mondeY,XdebutInt,zut);
                end;
          periodeFFT := valeurVar[indexTri,finFFT]-valeurVar[indexTri,debutFFT]/ (finFFT-debutFFT) * (finFFT-debutFFT+1);
      end;
      AffecteDebutFin;
end;

procedure TFgrapheFFT.AffecteDebutFin;
begin with pages[pageCourante] do begin
    recalculFourierP;
    GrapheFrequence.modif := [gmEchelle];
    MajGrapheFFT;
end end;

procedure TFgrapheFFT.TraceDebutFin;
var yy,xFinIntMax,xDebutIntMin : integer;
begin with grapheTemps,PaintBoxTemps.Canvas,pages[pageCourante] do begin
      Pen.Color := clGray;
      Pen.Width := 3;
      Pen.mode := pmNotXor;
      windowXY(valeurVar[indexTri,pred(nmes)],0,mondeY,XfinIntMax,yy);
      windowXY(valeurVar[indexTri,0],0,mondeY,XdebutIntMin,yy);
      if XdebutInt<XdebutIntMin then XdebutInt := XdebutIntMin;
      if XfinInt>XfinIntMax then XfinInt := XfinIntMax;
      if (XfinInt-XdebutInt)<(3*LimiteCourbe.right div 4) then begin
         Brush.color := clCream;
         Brush.style := bsSolid;
      end;
      Rectangle(XdebutInt,LimiteFenetre.Bottom-marge,XfinInt,LimiteFenetre.Top+marge);
      Brush.style := bsClear;
      Pen.Width := 1;
      periodeFFTedit.text := grandeurs[indexTri].FormatValeurEtUnite(periodeFFT);
end end;

procedure TFGrapheFFT.PasSonAffEditExit(Sender: TObject);
begin
    if SonagrammeBtn.down then begin
        pasSonagrammeAff := GetFloat(pasSonAffEdit.text);
        calculSonagramme;
        PaintBoxFrequence.invalidate;
    end;
end;

procedure TFGrapheFFT.pasSonCalculEditExit(Sender: TObject);
begin
    if SonagrammeBtn.down then begin
       pasSonCalcul := pasSonCalculSE.value;
       calculSonagramme;
       PaintBoxFrequence.invalidate;
    end;
end;

procedure TFGrapheFFT.PeriodeBtnClick(Sender: TObject);
var zut,finLoc : Integer;
begin with pages[pageCourante] do begin
    if PaintBoxTemps.visible then traceDebutFin; // efface
    cherchePeriode(valeurVar[indexFonction],nmes,debutFFT,finFFT);
    with GrapheTemps do begin
        if finFFT<(nmes-2)
            then finLoc := finFFT+1
            else finLoc := pred(nmes);
// de manière  à ce que le graphe représente la durée
// alors que le dernier point est à (durée - deltat)
        windowXY(valeurVar[indexTri,finLoc],valeurVar[indexFonction,finLoc],mondeY,XfinInt,zut);
        windowXY(valeurVar[indexTri,debutFFT],valeurVar[indexFonction,debutFFT],mondeY,XdebutInt,zut);
    end;
    if PaintBoxTemps.visible then traceDebutFin;
    avecPeriode := true;
    MajRaiesAfaire := true;
    AffecteDebutFin;
end end;

procedure TFgrapheFFT.ChercheReference;
var x,y : double;
    ifin,idebut : integer;
    YdebutInt,YfinInt,z : Integer;
begin with grapheTemps,PaintBoxTemps.Canvas,pages[pageCourante] do begin
         mondeXY(XdebutInt,YdebutInt,mondeY,x,y);
         idebut := 0;
         while (idebut<pred(nmes)) and (valeurVar[indexTri,idebut]<x) do inc(idebut);
         windowXY(valeurVar[indexTri,idebut],valeurVar[IndexFonction,idebut],mondeY,z,YdebutInt);
         mondeXY(XfinInt,YfinInt,mondeY,x,y);
         ifin := pred(nmes);
         while (ifin>idebut) and (valeurVar[indexTri,ifin]>x) do dec(ifin);
         windowXY(valeurVar[indexTri,ifin],valeurVar[IndexFonction,ifin],mondeY,z,YfinInt);
         Reference := rect(XdebutInt,YdebutInt,XfinInt,YfinInt);
         TraceReference;
end end;

procedure TFGrapheFFT.PaintBoxFrequenceDblClick(Sender: TObject);
begin
    doubleClick := true
end;

procedure TFGrapheFFT.ToolButtonClick(Sender: TObject);
var i : integer;
    nom : string;
    bouton : TToolButton;
begin
  bouton := (sender as TToolButton);
  nom := bouton.caption;
  if bouton.Down
      then begin
           if listeY.Count>=maxOrdonnee then listeY.Delete(0);
           ListeY.add(nom);
      end
      else begin
           for i := 0 to pred(listeY.count) do
               if listeY[i]=nom
                  then begin
                       ListeY.delete(i);
                       break;
                  end;
           if listeY.count=0 then ListeY.add(Grandeurs[indexVariab[1]].nom);
      end;
   include(GrapheFrequence.modif,gmXY);
   MajGrapheFFT;
end;

procedure TFGrapheFFT.ToutBtnClick(Sender: TObject);
var zut : Integer;
begin with pages[pageCourante] do begin
    if PaintBoxTemps.visible then traceDebutFin; // efface
    debutFFT := 0;
    finFFT := pred(nmes);
    with GrapheTemps do begin
        windowXY(valeurVar[indexTri,finFFT],valeurVar[indexFonction,finFFT],mondeY,XfinInt,zut);
        windowXY(valeurVar[indexTri,0],valeurVar[indexFonction,0],mondeY,XdebutInt,zut);
    end;
    if PaintBoxTemps.visible then traceDebutFin;
    AffecteDebutFin;
    avecPeriode := false;
    MajRaiesAfaire := true;
end end;

procedure TFGrapheFFT.MajGrapheFFT;
begin
      if pageCourante=0 then exit;
      GrapheTemps.Modif := GrapheFrequence.Modif;
      MajRaiesAfaire := true;
      PaintBoxFrequence.invalidate;
      if paintBoxTemps.visible then PaintBoxTemps.invalidate;
      periodeFFTedit.text := grandeurs[indexTri].FormatValeurEtUnite(pages[pageCourante].periodeFFT);
end;

procedure TFGrapheFFT.metafile1Click(Sender: TObject);
begin
     GrapheTemps.withDebutFin := true;
     grapheUtilise := true;
     GrapheTemps.VersFichier('');
     GrapheTemps.withDebutFin := false;
     grapheUtilise := false;
end;

procedure TFGrapheFFT.SonagrammeBtnClick(Sender: TObject);
begin
     include(grapheFrequence.modif,gmXY);
     SonagrammeBtn.Down := not SonagrammeBtn.Down;
     setCoordonnee
end;

procedure TFGrapheFFT.FrequenceReduiteCBClick(Sender: TObject);
begin
     GrapheFrequence.Modif := [gmXY];
     MajGrapheFFT
end;

procedure TFGrapheFFT.harmoniqueAffItemClick(Sender: TObject);
begin
  inherited;
  PaintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.copierTableauItemClick(Sender: TObject);
begin
end;

procedure TFGrapheFFT.ImprimeBtnClick(Sender: TObject);
var bas : integer;
    PleinePage : boolean;
begin
    if OKreg(OkImprGr,0) then begin
         PleinePage := not(PaintBoxTemps.visible);
         if PleinePage
            then DebutImpressionGr(poLandScape,bas)
            else DebutImpressionGr(poPortrait,bas);
         if PaintBoxTemps.visible then begin
            grapheTemps.withDebutFin := true;
            grapheTemps.versImprimante(HautGrapheGr,bas);
            grapheTemps.withDebutFin := false;
         end;
         if PleinePage
            then grapheFrequence.versImprimante(1,bas)
            else grapheFrequence.versImprimante(HautGrapheGr,bas);
        finImpressionGr;
     end;
end;

procedure TFGrapheFFT.FenetreBtnClick(Sender: TObject);
begin
     Fenetre := Tfenetre((sender as Tcomponent).tag);
     FenetrageBtn.imageIndex := IconeW[fenetre];
     (sender as TmenuItem).checked := true;
     recalculFourierE;
     GrapheFrequence.modif := [gmEchelle];
     GrapheTemps.modif := [gmEchelle];
     MajRaiesAfaire := true;
     Application.MainForm.perform(WM_Reg_Maj,MajValeur,0);
end;

procedure TFGrapheFFT.ValeursGridDblClick(Sender: TObject);
begin
end;

procedure TFGrapheFFT.ValeursGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TFgrapheFFT.ImprimerGraphe(var bas : integer);
begin
    if printer.orientation=poLandscape then begin
       Printer.NewPage;
       bas := 0;
    end;
    if PaintBoxTemps.visible
       then begin
           grapheTemps.versImprimante(HautGrapheTxt,bas);
           grapheFrequence.versImprimante(HautGrapheTxt,bas);
       end
       else grapheFrequence.versImprimante(HautGrapheTxt,bas);
end;

procedure TFGrapheFFT.JPEG1Click(Sender: TObject);
begin
     GrapheTemps.withDebutFin := true;
     GrapheTemps.VersJPEG('');
     GrapheTemps.withDebutFin := false;
end;

procedure TFGrapheFFT.SplitterGridCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  TableauBtn.down := newSize>16;
  Accept := true;
end;

procedure TFGrapheFFT.FormActivate(Sender: TObject);
begin
   inherited;
   HarmoniqueAffItem.checked := harmoniqueAff;
   FenetrageBtn.imageIndex := IconeW[fenetre];
   debutFFTspin.Visible := avecReglagePeriode;
   PeriodeFFTEdit.Visible := avecReglagePeriode;
   finFFTspin.Visible := avecReglagePeriode;
   grapheUtilise := false;
   affecteToolBar;
end;

procedure TFGrapheFFT.CacherTempsClick(Sender: TObject);
begin
  TempsBtn.down := false;
  refresh;
end;

procedure TFGrapheFFT.bitmap1Click(Sender: TObject);
begin
     GrapheTemps.withDebutFin := true;
     GrapheTemps.VersBitmap('');
     GrapheTemps.withDebutFin := false;
end;

procedure TFGrapheFFT.CacherGridClick(Sender: TObject);
begin
  TableauBtn.down := false;
  refresh;
end;

procedure TFGrapheFFT.DebutFFTEditKeyPress(Sender: TObject; var Key: Char);
begin
     if key=crCR
        then begin
           if sender=periodeFFTedit then VerifPeriodeFFT(sender);
           if sender=pasSonAffEdit then PasSonAffEditexit(sender);
           if sender=freqMaxEdit then FreqMaxEditexit(sender);
           key := #0;
        end
        else if key<>crTab
             then VerifKeyGetFloat(key)
end;

procedure TFGrapheFFT.DebutFFTEditExit(Sender: TObject);
begin
     if not VerifPeriodeFFT(sender)
        then with Sender as Tedit do SetFocus
end;

function TFGrapheFFT.VerifPeriodeFFT(Sender: TObject) : boolean;
var valFin,deltat : double;
    posU : integer;
    tampon : String;
begin
     result := true;
     if pageCourante=0 then exit;
     try
     with pages[pageCourante] do begin
        tampon := periodeFFTedit.text;
        // enlever l'unité
        posU := pos(grandeurs[indexTri].nomUnite,tampon);
        if posU>0 then delete(tampon,posU,length(tampon)-posU+1);
        periodeFFT := GetFloat(tampon);
        deltat := valeurVar[indexTri,1]-valeurVar[indexTri,0];
        valFin := valeurVar[indexTri,debutFFT]+periodeFFT-deltat;
        if valFin>valeurVar[indexTri,pred(nmes)] then begin
           debutFFT := 0;
           valFin := valeurVar[indexTri,0]+periodeFFT-deltat;
        end;
        finFFT := pred(nmes);
        while (finFFT>0) and (valeurVar[indexTri,finFFT]>valFin) do dec(finFFT);
     end;
     AffecteDebutFin;
     except
         result := false
     end;
end;

procedure TFGrapheFFT.DebutFFTspinDownClick(Sender: TObject);
begin
     if pages[pageCourante].debutFFT>0 then
        dec(pages[pageCourante].debutFFT);
     affecteDebutFin;
end;

procedure TFGrapheFFT.DebutFFTspinUpClick(Sender: TObject);
begin
     if pages[pageCourante].debutFFT<(pages[pageCourante].finFFT-minPeriodeFFT) then
        inc(pages[pageCourante].debutFFT);
     affecteDebutFin;
end;

procedure TFGrapheFFT.DecadeSEChange(Sender: TObject);
begin
    DecadeDB := DecadeSE.value;
    PaintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.DecibelCBClick(Sender: TObject);
begin
    decadeSE.visible := decibelCB.checked;
    decadeLabel.visible := decadeSE.visible;
    if decibelCB.checked
       then begin
          DecadeDB := DecadeSE.value;
          decibelCB.Caption := 'en dB sur';
       end
       else begin
         DecadeDB := 0;
         decibelCB.Caption := 'en dB';
       end;
    paintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.FinFFTspinUpClick(Sender: TObject);
begin
     if pages[pageCourante].finFFT<pred(pages[pageCourante].nmes) then
        inc(pages[pageCourante].finFFT);
     affecteDebutFin;
end;

procedure TFGrapheFFT.FinFFTspinDownClick(Sender: TObject);
begin
     if pages[pageCourante].finFFT>(pages[pageCourante].debutFFT+minPeriodeFFT) then
        dec(pages[pageCourante].finFFT);
     affecteDebutFin;
end;

procedure TFGrapheFFT.CopierItemClick(Sender: TObject);
begin
     grapheUtilise := true;
     GrapheFrequence.VersPressePapier(grapheClip);
     grapheUtilise := false;
end;

procedure TFGrapheFFT.ZoomManuelBtnClick(Sender: TObject);
begin
end;

procedure TFGrapheFFT.FenetrageMenuPopup(Sender: TObject);
begin
    case Fenetre of
         Rectangulaire : RectBtn.checked := true;
         Hamming : HammingBtn.checked := true;
         Flattop : FlatTopBtn.checked := true;
         Blackman : BlackmanBtn.checked := true;
    end;
    FenetrageBtn.imageIndex := IconeW[fenetre];
end;

procedure TFGrapheFFT.DessinSupprimerItemClick(Sender: TObject);
var P : Tpoint;
begin
    if grapheFrequence.DessinCourant=nil then begin
         P := menuDessin.PopupPoint;
         P := grapheFrequence.paintBox.ScreenToClient(P);
         grapheFrequence.setDessinCourant(P.x,P.y);
    end;
    if grapheFrequence.DessinCourant=nil then exit;
    with grapheFrequence.dessins do Remove(grapheFrequence.DessinCourant);
    refresh;
end;

procedure TFGrapheFFT.ProprietesMenuClick(Sender: TObject);
var P : Tpoint;
begin
   if grapheFrequence.DessinCourant=nil then begin
         P := menuDessin.PopupPoint;
         P := grapheFrequence.paintBox.ScreenToClient(P);
         grapheFrequence.setDessinCourant(P.x,P.y);
   end;
   if grapheFrequence.DessinCourant=nil then exit;
   grapheFrequence.dessinCourant.litOption(grapheFrequence);
   refresh;
end;

procedure TFGrapheFFT.SelectBtnClick(Sender: TObject);
begin
    (sender as TtoolButton).CheckMenuDropdown
end;

procedure TFGrapheFFT.TextBtnClick(Sender: TObject);
begin
      curseurF := curTexteF;
      PaintBoxFrequence.Invalidate;
      Application.ProcessMessages;
      grapheFrequence.DessinCourant := Tdessin.create(grapheFrequence);
      grapheFrequence.DessinCourant.isTexte := true
end;

procedure TFGrapheFFT.ReticuleBtnClick(Sender: TObject);
begin
      setCurseurF(curReticuleF);
      PaintBoxFrequence.invalidate;
      pages[pageCourante].affecteConstParam;
end;

procedure TFGrapheFFT.FaussesCouleursCBClick(Sender: TObject);
begin
    UseFaussesCouleurs := FaussesCouleursCB.Checked;
    PaintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  action := caFree;
end;

procedure TFGrapheFFT.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
     case msg.charCode of
          vk_F10 : if curseurF=curReticuleF then begin
              SaveFreqItemClick(nil);
              handled := true;
          end;
     end;
end;

procedure TFGrapheFFT.SaveFreqItemClick(Sender: TObject);
var P : Tpoint;
begin with grapheFrequence.curseurOsc[1] do begin
        sauverFrequence(xr,yr);
        P := point(xc,yc);
        P := paintBoxFrequence.ClientToScreen(P);
        mouse.CursorPos := P;
end end;

procedure TFGrapheFFT.SauverFrequence(f,a : double);
begin with grapheFrequence do begin
       if saveHarmoniqueDlg=nil then
          Application.CreateForm(TsaveHarmoniqueDlg, saveHarmoniqueDlg);
       saveHarmoniqueDlg.valeur[gFrequence] := f;
       saveHarmoniqueDlg.valeur[gAmplitude] := a;
       saveHarmoniqueDlg.grandeur[gAmplitude] := grandeurs[indexNom(listeY[0])];
       saveHarmoniqueDlg.grandeur[gFrequence] := grandeurs[cFrequence];
       saveHarmoniqueDlg.showModal;
end end;

procedure TFGrapheFFT.ReticuleItemClick(Sender: TObject);
begin
     SelectBtn.imageIndex := 23;
     ReticuleItem.checked := true;
end;

procedure TFGrapheFFT.TexteItemClick(Sender: TObject);
begin
     CurseurF := curTexteF;
     PaintBoxFrequence.Invalidate;
     Application.ProcessMessages;
     grapheFrequence.DessinCourant := Tdessin.create(grapheFrequence);
     grapheFrequence.DessinCourant.isTexte := true
end;

procedure TFGrapheFFT.StandardItemClick(Sender: TObject);
begin
     CurseurF := curSelectF;
     PaintBoxFrequence.Invalidate;
end;

procedure TFGrapheFFT.FreqMaxEditExit(Sender: TObject);
begin
    freqMaxSonagramme := GetFloat(freqMaxEdit.text);
    freqMaxSonUD.position := round(freqMaxSonagramme);
    freqMaxSonUD.increment := round(freqMaxSonagramme/10);
    grapheFrequence.monde[mondeX].defini := false;
    grapheFrequence.monde[mondeY].defini := false;
    PaintBoxFrequence.invalidate;
end;


procedure TFGrapheFFT.FreqMaxSonUDChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Integer; Direction: TUpDownDirection);
begin
    freqMaxSonagramme := newValue;
    freqMaxSonUD.increment := round(freqMaxSonagramme/10);
    grapheFrequence.monde[mondeX].defini := false;
    grapheFrequence.monde[mondeY].defini := false;
    freqMaxEdit.text := formatReg(freqMaxSonagramme);
    PaintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.LigneItemClick(Sender: TObject);
begin
    CurseurF := curLigneF;
    PaintBoxFrequence.Invalidate;
    Application.ProcessMessages;
    grapheFrequence.DessinCourant := Tdessin.create(grapheFrequence);
    grapheFrequence.DessinCourant.isTexte := false
end;

procedure TFGrapheFFT.ZoomDebutFinItemClick(Sender: TObject);
begin
      refresh
end;

procedure TFGrapheFFT.NbreHarmAffSpinDownClick(Sender: TObject);
begin
  if NbreHarmoniqueAff>0 then dec(NbreHarmoniqueAff);
  HarmoniqueAff := NbreHarmoniqueAff>0;
  MajRaiesAfaire := true;
  paintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.NbreHarmAffSpinUpClick(Sender: TObject);
begin
  if NbreHarmoniqueAff<NbreHarmoniqueAffMax then begin
     inc(NbreHarmoniqueAff);
     HarmoniqueAff := true;
     HarmoniqueAffItem.Checked := true;
     MajRaiesAfaire := true;
     paintBoxFrequence.invalidate;
  end;
end;

procedure TFGrapheFFT.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var P : Tpoint;
begin
     if ssAlt in Shift then begin
        case key of
             ord('C') : OptionsFourierItemClick(nil);
             ord('I') : ImprimeBtnClick(nil);
        end;
        exit;
     end;
     GetCursorPos(P);
     P := PaintBoxFrequence.ScreenToClient(P);
     if (P.X<0) or (P.X>paintBoxFrequence.width) or
        (P.Y<0) or (P.Y>paintBoxFrequence.height) then exit;
     case key of
          vk_left : if ssShift in Shift then dec(P.X,4) else dec(P.X);
          vk_right :  if ssShift in Shift then inc(P.X,4) else inc(P.X);
          vk_down : inc(P.Y);
          vk_up : dec(P.Y);
          vk_prior : dec(P.X,16);
          vk_next : inc(P.X,16);
          vk_escape : begin
               case curseurF of
                    curZoomF : zoomEnCours := false;
                    curFmaxi : ;
                    curFmaxiSon : ;
                    curTexteF,curLigneF : begin
                       grapheFrequence.dessinCourant.free;
                       grapheFrequence.dessinCourant := nil;
                    end;
                    curReticuleF : exit;
                    curMoveF : ;
                    curSelectF : ;
                end;
            setCurseurF(curSelectF);
            PaintBoxFrequence.invalidate;
            exit;
          end // escape
          else exit;
     end;
     key := 0;
     P := PaintBoxFrequence.ClientToScreen(P);
     setCursorPos(P);
end; //  FormKeyDown

procedure TFGrapheFFT.FormKeyPress(Sender: TObject; var Key: Char);
var posSouris : Tpoint;
    i : integer;
    xr,yr,dy : double;
    Dessin : Tdessin;
    indexTemps,IndexFreq : integer;
label fin,finS;
begin   // à vérifier dans le cas sonagramme
   if (key=' ') and
      (curseurF in [curReticuleF,curSelectF]) and
      not SonagrammeBtn.down then begin
        GetCursorPos(PosSouris);
        PosSouris := PaintBoxFrequence.ScreenToClient(PosSouris);
        if (PosSouris.X<0) or (PosSouris.X>paintBoxFrequence.width) or
           (PosSouris.Y<0) or (PosSouris.Y>paintBoxFrequence.height) then exit;
        iCourant := -1;
        with grapheFrequence.courbes[0] do
          if (((trace=[trLigne]) and (motif=mLosange)) or
              ((trace=[trPoint]) and (motif=mSpectre))) and
             (page=pageCourante) then with grapheFrequence do begin
                  mondeRT(posSouris.x,posSouris.y,mondeY,xr,yr);
                  iCourant := listePic[1].Proche(xr,monde[mondeX].maxi);
                  xr := listePic[1].pics[iCourant].valeurF;
                  yr := listePic[1].pics[iCourant].valeurH;
          end;
      if iCourant<0 then exit;
      with grapheFrequence do
      for i := 0 to pred(dessins.count) do
         if (dessins[i].identification=identRaie) and
            (abs(dessins[i].x1i-posSouris.x)<16) and
            (abs(dessins[i].y1i-posSouris.y)<32)
          then begin
             Dessins.remove(dessins[i]);
             goto fin
          end;
      dy := grapheFrequence.monde[mondeY].maxi/64;
      Dessin := Tdessin.create(grapheFrequence);
      with Dessin do begin
            isTexte := true;
            avecLigneRappel := false;
            MotifTexte := mtNone;
            hauteur := 2;
            NumPage := 0;
            vertical := false;
            pen.color := clBlue;
            identification := identRaie;
            x1 := xr;
            y1 := yr+dy;
            x2 := xr;
            y2 := yr+dy;
            Agraphe := grapheFrequence;
            grapheFrequence.dessins.add(Dessin);
            texte.add(grandeurs[cFrequence].formatValeurEtUnite(xr));
      end;
      fin:
      MajRaiesAfaire := false;
      PaintBoxFrequence.invalidate;
   end; // if barre et select
   if (key=' ') and  SonagrammeBtn.down then begin
       GetCursorPos(PosSouris);
       PosSouris := PaintBoxFrequence.ScreenToClient(PosSouris);
       if (PosSouris.X<0) or (PosSouris.X>paintBoxFrequence.width) or
          (PosSouris.Y<0) or (PosSouris.Y>paintBoxFrequence.height) then exit;
       grapheFrequence.mondeRT(posSouris.X,posSouris.Y,mondeY,Xr,Yr);
       with grapheFrequence do
       for i := 0 to pred(dessins.count) do
         if (abs(dessins[i].x1i-posSouris.x)<16) and
            (abs(dessins[i].y1i-posSouris.y)<32)
          then begin
             Dessins.remove(dessins[i]);
             goto finS
          end;
      dy := grapheFrequence.monde[mondeY].maxi/64;
      Dessin := Tdessin.create(grapheFrequence);
      grapheFrequence.getIndexSonagramme(xr,yr,indexTemps,indexFreq,yr);
      with Dessin do begin
            isTexte := true;
            avecLigneRappel := false;
            MotifTexte := mtNone;
            hauteur := 2;
            NumPage := 0;
            vertical := false;
            pen.color := clBlack;
            isOpaque := true;
            x1 := xr;
            y1 := yr+dy;
            x2 := xr;
            y2 := yr+dy;
            Agraphe := grapheFrequence;
            grapheFrequence.dessins.add(Dessin);
            texte.add(grandeurs[cFrequence].formatValeurEtUnite(yr));
        end;
        finS :
        PaintBoxFrequence.invalidate;
   end;
end;

procedure TFGrapheFFT.changeEchelleSon(xnew, xold: integer);
begin
  with grapheFrequence do begin
    xnew := limiteCourbe.bottom - xnew;
    if xnew < 16 then exit;
    xold := limiteCourbe.bottom - xold;
    with monde[mondeY] do begin
       A := A * xnew / xold;
       Maxi := Mini + (limiteCourbe.top - limiteCourbe.bottom) / A;
       FreqMaxSonagramme := Maxi;
       if FreqMaxSonagramme>pasFreqSonagramme*NbrePointsSonagramme
          then freqMaxSonagramme := pasFreqSonagramme*NbrePointsSonagramme;
       freqMaxEdit.text := formatReg(freqMaxSonagramme);
       freqmaxsonUD.Position := round(freqMaxSonagramme);
       freqMaxSonUD.increment := round(freqMaxSonagramme/10);
    end;
  end;
  PaintBoxFrequence.invalidate;
end;

procedure TFGrapheFFT.TracePosition;
begin
    with paintBoxTemps.canvas do begin
         pen.mode := pmNotXor;
         pen.color := clRed;
         pen.Width := 1;
         moveTo(positionPB,0);
         lineTo(positionPB,paintBoxTemps.height);
    end;
    if sonagrammeBtn.Down then with paintBoxFrequence.canvas do begin
         pen.mode := pmNotXor;
         pen.color := clRed;
         pen.Width := 1;
         moveTo(positionPB,0);
         lineTo(positionPB,paintBoxFrequence.height);
    end;
end;

procedure TFGrapheFFT.CalculSonagramme;
var freqEch : double;
begin with pages[pageCourante] do begin
     FreqEch := pred(Nmes)/(valeurVar[indexTri][pred(Nmes)]-valeurVar[indexTri][0]);
     Sonagramme(Nmes,valeurVar[indexSonagramme],FreqEch);
     freqMaxSonUD.max := round(freqEch/2);
     PeriodeFFT := valeurVar[indexTri,pred(nmes)];
     nbreFFT := NbrePointsSonagramme;
end end;

procedure TFGrapheFFT.AffecteToolBar;
var i,j,iVar : integer;
begin
     j := 0;
     for i := 0 to pred(NbreVariab) do begin
         iVar := indexVariab[i];
         if iVar<>indexTri then
            with ToolBarGrandeurs.Buttons[j] do begin
                 visible := true;
                 caption := grandeurs[iVar].nom;
                 tag := iVar;
                 inc(j);
            end;
            if j>=ToolBarGrandeurs.ButtonCount then break;
      end;
      for i := j to pred(ToolBarGrandeurs.ButtonCount) do
               ToolBarGrandeurs.Buttons[i].visible := false;
end;

end.



