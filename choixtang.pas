{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit choixtang;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Spin, sysUtils, colorBox,
  regutil, maths, compile, graphker, ExtCtrls, ComCtrls;

type
  TChoixTangenteDlg = class(TForm)
    CancelBtn: TBitBtn;
    OptionsBtn: TSpeedButton;
    TangenteCB: TComboBox;
    OKBtn: TBitBtn;
    SupprCB: TCheckBox;
    OptionsPC: TPageControl;
    TabSheet1: TTabSheet;
    StoechTS: TTabSheet;
    TabSheet3: TTabSheet;
    TraitCB: TComboBox;
    TangenteColor: TColorBox;
    Label4: TLabel;
    NbreEdit: TEdit;
    SpinButton1: TSpinEdit;
    Label3: TLabel;
    Burette: TSpinEdit;
    Label5: TLabel;
    Becher: TSpinEdit;
    Label1: TLabel;
    LargeurCB: TComboBox;
    LabelLargeur: TLabel;
    GridCB: TCheckBox;
    procedure OptionsBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure TangenteCBChange(Sender: TObject);
    procedure NbreSpinButtonDownClick(Sender: TObject);
    procedure NbreSpinButtonUpClick(Sender: TObject);
  private
  public
  end;

var
  ChoixTangenteDlg: TChoixTangenteDlg;

implementation

  {$R *.lfm}

procedure TChoixTangenteDlg.OptionsBtnClick(Sender: TObject);
begin
   TangenteCBChange(sender);
end;

procedure TChoixTangenteDlg.FormActivate(Sender: TObject);
begin
  case LigneRappelTangente of
     lrTangente : TangenteCB.itemIndex := 1;
     else TangenteCB.itemIndex := 0
  end;
  if (stoechBecherGlb>1) or (stoechBuretteGlb>1)
     then optionsBtn.Down := true;
  largeurCB.itemIndex := round(1/LongueurTangente)-1;
  NbreEdit.text := IntToStr(NbrePointDerivee);
  gridCB.Checked := avecTableau;
  TangenteColor.selected := pColorTangente;
  TraitCB.itemIndex := ord(PstyleTangente);
  TangenteCBChange(sender);
end;

procedure TChoixTangenteDlg.NbreSpinButtonDownClick(Sender: TObject);
begin
     if NbrePointDerivee>MinPointsDerivee then begin
        dec(NbrePointDerivee,2);
        NbreEdit.text := IntToStr(NbrePointDerivee);
     end;
end;

procedure TChoixTangenteDlg.NbreSpinButtonUpClick(Sender: TObject);
begin
     if NbrePointDerivee<MaxPointsDerivee then begin
        inc(NbrePointDerivee,2);
        NbreEdit.text := IntToStr(NbrePointDerivee);
     end;
end;

procedure TChoixTangenteDlg.OKBtnClick(Sender: TObject);
begin
    case TangenteCB.itemIndex of
         1 : LigneRappelTangente := lrTangente;
         else LigneRappelTangente := lrEquivalence;
    end;
    avecTableau := gridCB.Checked;
    LigneRappelCourante := LigneRappelTangente;
    PstyleTangente := TpenStyle(TraitCB.itemIndex);
    if stoechTS.visible
       then begin
          StoechBecherGlb := Becher.value;
          StoechBuretteGlb := Burette.value;
          pages[pageCourante].StoechBecher :=  StoechBecherGlb;
          pages[pageCourante].StoechBurette :=  StoechBuretteGlb;
       end
       else begin
          StoechBecherGlb := 1;
          StoechBuretteGlb := 1;
       end;
    LongueurTangente := 1/(largeurCB.itemIndex+1);
    pColorTangente := TangenteColor.selected;
end;

procedure TChoixTangenteDlg.TangenteCBChange(Sender: TObject);
begin
   optionsPC.visible := optionsBtn.down;
   stoechTS.Visible := TangenteCB.itemIndex<>2;
   largeurCB.visible := TangenteCB.itemIndex=2;
   labelLargeur.Visible := largeurCB.visible;
   if optionsBtn.down
      then height := 248
      else height := 248-optionsPC.height;
end;

end.


