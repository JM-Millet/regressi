{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit zoomman;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, Grids, ExtCtrls, sysUtils, math, 
  constreg, regutil, compile, graphker;

type
  TZoomManuelDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    MinMaxGrid: TStringGrid;
    AutoTickCB: TCheckBox;
    MemeZeroCB: TCheckBox;
    ZoomAutoBtn: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure MinMaxGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MinMaxGridKeyPress(Sender: TObject; var Key: Char);
    procedure MinMaxGridGetEditText(Sender: TObject; ACol, ARow: Longint;
      var Value: String);
    procedure MinMaxGridExit(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure MinMaxGridSelectCell(Sender: TObject; Col, Row: Longint;
      var CanSelect: Boolean);
    procedure AutoTickCBClick(Sender: TObject);
    procedure ZoomAutoBtnClick(Sender: TObject);
  private
    X : array[1..4,1..6] of double;
    Ligne : array[indiceMonde] of integer;
    LigneTemps : integer;
  public
    Echelle : TgrapheReg;
  end;

var
  ZoomManuelDlg: TZoomManuelDlg;

implementation

  {$R *.lfm}

procedure TZoomManuelDlg.FormActivate(Sender: TObject);
var m : indiceMonde;
    Acol,Arow : integer;
    AjouterTemps : boolean;
begin
inherited;
with echelle,minMaxGrid do begin
    visible := false;
    Arow := 0;
    Cells[1,0] := stMinimum;
    Cells[2,0] := stMaximum;
    Cells[3,0] := stGrad;
    Cells[4,0] := stSubdiv;
    AutoTickCB.checked  := autoTick;
    AjouterTemps := true;
    LigneTemps := -1;
    ZoomAutoBtn.visible := useDefaut;
    for m := mondeX to high(indiceMonde) do with monde[m] do if defini then begin
       inc(Arow);
       ligne[m] := Arow;
       rowCount := Arow+1;
       if axe<>nil
          then begin
            Cells[0,Arow] := axe.nom;
            AjouterTemps := AjouterTemps and (indexNom(axe.nom)<>0);
          end
          else Cells[0,Arow] := '';
       X[1,Arow] := Mini;
       X[2,Arow] := Maxi;
       X[3,Arow] := deltaAxe*Nticks;
       X[4,Arow] := Nticks;
       case Graduation of
            gLog : begin
                 X[1,Arow] := power(10,X[1,Arow]);
                 X[2,Arow] := power(10,X[2,Arow]);
            end;
            gInv : begin
                 X[1,Arow] := 1/X[1,Arow];
                 X[2,Arow] := 1/X[2,Arow];
            end;
       end; { case }
       for Acol := 1 to 2 do
           Cells[Acol,Arow] := formatReg(X[Acol,Arow]);
       if AutoTickCB.checked
           then begin
              cells[3,Arow] := 'Auto';
              cells[4,Arow] := 'Auto'
           end
           else begin
              cells[3,Arow] := formatReg(X[3,Arow]);
              cells[4,Arow] := intToStr(NTicks);
           end;
    end; { monde }
    if OgPolaire in OptionGraphe then begin
       Cells[0,1] := 'abscisse';
       Cells[0,2] := 'ordonnée';
    end;
    col := 1;
    row := 1;
    visible := true;
    MemeZeroCB.visible :=
      not(OgAnalyseurLogique in OptionGraphe) and
      not(OgPolaire in OptionGraphe);
    MemeZeroCB.checked := OgMemeZero in OptionGraphe;
    if ajouterTemps then begin
       inc(Arow);
       ligneTemps := Arow;
       rowCount := Arow+1;
       Cells[0,Arow] := grandeurs[0].nom+' (Mod)';
       X[1,Arow] := MiniTemps;
       X[2,Arow] := MaxiTemps;
       X[3,Arow] := 0;
       X[4,Arow] := 0;
       for Acol := 1 to 2 do
           Cells[Acol,Arow] := formatReg(X[Acol,Arow]);
       Cells[3,Arow] := '';
       Cells[4,Arow] := '';
    end;
end end;

procedure TZoomManuelDlg.OKBtnClick(Sender: TObject);

Procedure verif(m : indiceMonde);
var Arow : integer;
begin with echelle.monde[m] do begin
   Arow := ligne[m];
   if abs(x[1,Arow]-x[2,Arow])<(abs(x[1,Arow])+abs(x[2,Arow]))/100 then exit;
   if Graduation=gdB
      then SetMinMaxDefaut(power(10,x[1,Arow]/20),power(10,x[2,Arow]/20))
      else SetMinMaxDefaut(x[1,Arow],x[2,Arow]);
   try
   NTicks := strToInt(MinMaxGrid.cells[4,ARow]);
   except
       NTicks := 2;
   end;
   if NTicks<1 then NTicks := 1;
   if NTicks>5 then NTicks := 5;
   deltaAxe := x[3,Arow]/NTicks;
end end; // verif

var m : indiceMonde;
begin with echelle do begin
    MinMaxGridExit(nil);
    autoTick := AutoTickCB.checked;
    useDefaut := true;
    for m := mondeX to high(indiceMonde) do begin
        if Monde[m].defini then verif(m);
         Monde[m].zeroInclus := false;
    end;
    if ogOrthonorme in OptionGraphe
       then for m := mondeY to high(indiceMonde) do with monde[m] do
            if defini then deltaAxe := monde[mondeX].deltaAxe;
    if ligneTemps>0 then begin
       miniTemps := x[1,ligneTemps];
       maxiTemps := x[2,ligneTemps];
    end;
    UseDefaut := true;
    if MemeZeroCB.visible then if MemeZeroCB.checked
        then include(echelle.OptionGraphe,OgMemeZero)
        else exclude(echelle.OptionGraphe,OgMemeZero)
end end; // OKBtnClick

procedure TZoomManuelDlg.MinMaxGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if ToucheValidation(key) then with MinMaxGrid do begin
       if (col>0) and (row>0) then begin
           try
           x[col,row] := GetFloat(cells[col,row]);
           except
              cells[col,row] := FloatToStr(x[col,row]);
           end;
     end;
end end;

procedure TZoomManuelDlg.MinMaxGridKeyPress(Sender: TObject;var Key: Char);
begin with MinMaxGrid do
    if (col>0) and (row>0)
        then VerifKeyGetFloat(key)
        else key := #0
end;

procedure TZoomManuelDlg.MinMaxGridGetEditText(Sender: TObject; ACol,
  ARow: Longint; var Value: String);
begin
     if (Acol>0) and (Arow>0) then
         case Acol of
            3 : if AutoTickCB.checked
                  then value := 'Auto'
                  else value := FormatReg(x[Acol,Arow]);
            4 : if AutoTickCB.checked
                  then value := 'Auto'
                  else value := intToStr(round(x[Acol,Arow]));
            else value := FormatReg(x[Acol,Arow])
         end;
end;

procedure TZoomManuelDlg.MinMaxGridExit(Sender: TObject);
begin with MinMaxGrid do
       if (col>0) and (row>0) then
           try
           x[col,row] := GetFloat(cells[col,row]);
           except
              if col<4
                 then cells[col,row] := FloatToStr(x[col,row])
                 else cells[col,row] := intToStr(round(x[col,row]))
           end;
end;

procedure TZoomManuelDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(812)  { TODO : aide zoom manuel }
end;

procedure TZoomManuelDlg.MinMaxGridSelectCell(Sender: TObject;
  Col,Row: Longint; var CanSelect: Boolean);
begin
     MinMaxGridExit(Sender);
     if AutoTickCB.checked
        then CanSelect := col<3
        else CanSelect := true
end;

procedure TZoomManuelDlg.AutoTickCBClick(Sender: TObject);
var Arow : integer;
    S : String;
begin
     for Arow := 1 to pred(MinMaxGrid.rowCount) do begin
         MinMaxGridGetEditText(sender,3,Arow,S);
         MinMaxGrid.cells[3,Arow] := S;
         MinMaxGridGetEditText(sender,4,Arow,S);
         MinMaxGrid.cells[4,Arow] := S;
     end;
     MinMaxGrid.refresh
end;

procedure TZoomManuelDlg.ZoomAutoBtnClick(Sender: TObject);
begin
     echelle.useDefaut := false;
     echelle.autoTick := true;
end;

end.
