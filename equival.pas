{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

// equival.pas = include graphker

function TriEquivalence(Item1, Item2: Pointer): Integer;
var z : double;
begin
    z := Tequivalence(item1).ve-Tequivalence(item2).ve;
    if z<0
       then result := -1
       else if z>0
          then result := 1
          else result := 0
end;

Procedure TGrapheReg.GetEquivalence(x,y : integer;
     var Aequivalence : Tequivalence;var selectEq : TselectEquivalence);
const pourCent = 0.1;
      minPoint = 10;
      maxpente = 10;
var penteTest : double;
    i : integer;
  //  penteTgSI : double;
begin
      selectEq := seNone;
      Aequivalence := nil;
      if (equivalences[pageCourante].count=0) then exit;
      for i := 0 to pred(equivalences[pageCourante].count) do begin
          with equivalences[pageCourante].items[i] do begin
          (*
              if uniteSIGlb and not modelisee
                 then penteTgSI := pente*coeffSIpente
                 else penteTgSI := pente;
                 *)
              if (abs(x-x1i)+abs(y-y1i))<minPoint then selectEq := sePoint1;
              if (abs(x-x2i)+abs(y-y2i))<minPoint then selectEq := sePoint2;
              if (abs(x-vei)+abs(y-phei))<minPoint then selectEq := sePointEq;
              if abs(x-x2i)>maxPente then begin
                  penteTest := monde[mondeX].a*(y-y2i)/(x-x2i)/monde[MondepH].a;
                  if abs(pente-penteTest)<pourCent*abs(Pente) then selectEq := sePente2;
              end;
              if abs(x-x1i)>maxPente then begin
                    penteTest := monde[mondeX].a*(y-y1i)/(x-x1i)/monde[MondepH].a;
                    if abs(pente-penteTest)<pourCent*abs(pente) then selectEq := sePente1;
              end;
              if abs(x-vei)>maxPente then begin
                  penteTest := monde[mondeX].a*(y-phei)/(x-vei)/monde[MondepH].a;
                  if abs(pente-penteTest)<pourCent*abs(Pente) then selectEq := sePenteEq;
              end;
         end;
         if selectEq<>seNone then begin
            Aequivalence := equivalences[pageCourante].items[i];
            break;
         end;
      end;
end;

Function Tequivalence.Modif(x,y : integer;selectEq : TselectEquivalence) : boolean;

Procedure ModifpH;
begin
     Agraphe.MondeRT(x2i,y2i,mondepH,x2,y2);
     Agraphe.MondeRT(x1i,y1i,mondepH,x1,y1);
     ve := (StoechBecherGlb*x1+StoechBuretteGlb*x2)/(StoechBecherGlb+StoechBuretteGlb);
     pHe := (StoechBecherGlb*y1+StoechBuretteGlb*y2)/(StoechBecherGlb+StoechBuretteGlb);
     Agraphe.windowXY(ve,pHe,mondepH,vei,phei);
end;

var xx,yy : double;
    dx,dy : integer;
begin with Agraphe do begin
      case selectEq of
           sePoint2 : begin
               result  := (abs(x-x1i)+abs(y-y1i))>64;
               if result then begin
                  x2i := x;
                  y2i := y;
                  modifph;
               end;
           end;
           sePointEq : begin
               result := true;
               dx := x-vei;dy := y-phei;
               x1i := x1i+dx;x2i := x2i+dx;
               y1i := y1i+dy;y2i := y2i+dy;               
               modifph;
           end;
           sePoint1 : begin
               result := (abs(x-x2i)+abs(y-y2i))>64;
               if result then begin
                  x1i := x;
                  y1i := y;
                  modifph;
               end;
           end;
           sePente1 : begin
               result := (abs(x-x1i)>16) and (abs(y-y1i)>16);
               if result then begin
                  MondeRT(x,y,MondepH,xx,yy);
                  pente := (yy-y1)/(xx-x1);
               end;
           end;
           sePente2 : begin
               result := (abs(x-x2i)>16) and (abs(y-y2i)>16);
               if result then begin
                  MondeRT(x,y,MondepH,xx,yy);
                  pente := (yy-y2)/(xx-x2);
               end;
           end;
           sePenteEq : begin
               result := (abs(x-vei)>16) and (abs(y-phei)>16);
               if result then begin
                  MondeRT(x,y,MondepH,xx,yy);
                  pente := (yy-phe)/(xx-ve);
               end;
           end;
           else result := false;
     end;
end end;

Procedure TgrapheReg.TraceDroite(x,y,pente : double;
   xMin,yMin,xMax,Ymax : double);
const largeur = 8;
var
   x1,x2,K : double;
   x1i,y1i,x2i,y2i,dx,dy : Integer;
begin
    if pente=0
       then begin
          x1 := xMin;
          x2 := xMax;
       end
       else begin
          try
          if pente>0
             then begin
                x1 := x+(yMin-y)/pente;
                x2 := x+(yMax-y)/pente;
             end
             else begin
                x1 := x+(yMax-y)/pente;
                x2 := x+(yMin-y)/pente;
             end;
         if x1<xMin then x1 := xMin;
         if x2>xMax then x2 := xMax;
         except
             x1 := xMin;
             x2 := xMax;
         end;
    end;
    windowXY(x1,y+pente*(x1-x),MondeDerivee,x1i,y1i);
    windowXY(x2,y+pente*(x2-x),MondeDerivee,x2i,y2i);
    canvas.MoveTo(x1i,y1i);
    canvas.LineTo(x2i,y2i);
    try
    if abs(x2i-x1i)>abs(y2i-y1i)
      then begin
           K := (y2i-y1i)/(x2i-x1i);
           dy := round(largeur/sqrt(1+K*K));
           dx := -round(K*dy);
      end
      else begin
           K := (x2i-x1i)/(y2i-y1i);
           dx := round(largeur/sqrt(1+K*K));
           dy := -round(K*dx);
      end;
    windowXY(x,y,MondeDerivee,x2i,y2i);
    canvas.MoveTo(x2i+dx,y2i+dy);
    canvas.LineTo(x2i-dx,y2i-dy);
    except
    end;
end;

Procedure Tequivalence.Draw;
const dimEq = 3;
var ecart,x3,x4,y3,y4 : double;
    posTrait : integer;
    signe : integer;
    couleur : Tcolor;
begin with Agraphe do begin
     if LigneRappel in [lrXdeY]
        then pen.mode := pmNotXor
        else pen.mode := pmCopy;
     couleur := pColorTangente;
     if ligneRappel in [lrXdeY,lrX,lrY,lrReticule] then couleur := pColorReticule;
     createPen(PstyleTangente,1,colorToRGB(couleur));
     case LigneRappel of
        lrEquivalence : begin
             x3 := x1-(x2-x1)/2;
             x4 := x2+(x2-x1)/2;
             y3 := (y1+y2)/2-abs(y2-y1);
             y4 := (y2+y1)/2+abs(y2-y1);
             WindowRT(ve,pHe,MondepH,vei,phei);
             agraphe.canvas.MoveTo(vei,basCourbe);
             agraphe.canvas.LineTo(vei,phei);
             agraphe.canvas.MoveTo(limiteCourbe.left,phei);
             agraphe.canvas.LineTo(vei,phei);
             traceDroite(x1,y1,pente,x3,y3,x4,y4);
             windowXY(x1,y1,MondepH,x1i,y1i);
             traceDroite(x2,y2,pente,x3,y3,x4,y4);
             windowXY(x2,y2,MondepH,x2i,y2i);
             traceDroite(ve,pHe,pente,x3,y3,x4,y4);

        end;
        lrTangente : begin
             WindowRT(ve,pHe,MondepH,vei,phei);
             ecart := (monde[mondeX].maxi - monde[mondeX].mini)*LongueurTangente;
             x3 := ve+ecart;
             if x3>monde[mondeX].maxi then x3 := monde[mondeX].maxi;
             x4 := ve-ecart;
             if x4<monde[mondeX].mini then x4 := monde[mondeX].mini;
             ecart := (monde[MondepH].maxi - monde[MondepH].mini)*LongueurTangente;
             y3 := pHe+ecart;
             if y3>monde[mondepH].maxi then y3 := monde[mondepH].maxi;
             y4 := pHe-ecart;
             if y4<monde[mondepH].mini then y4 := monde[mondepH].mini;
             traceDroite(ve,pHe,pente,x4,y4,x3,y3);
        end;
        lrXdeY,lrReticule : begin
            WindowRT(ve,pHe,mondepH,vei,phei);
            canvas.moveTo(vei,phei);
            canvas.lineTo(vei,basCourbe);
            case mondepH of
                 mondeY : begin
                     canvas.moveTo(limiteCourbe.left,phei);
                     canvas.lineTo(vei,phei);
                 end;
                 mondeDroit : begin
                     canvas.moveTo(limiteCourbe.right,phei);
                     canvas.lineTo(vei,phei);
                 end;
            end;
        end;
        lrX : begin
            WindowRT(x1,y1,mondepH,x1i,y1i);
            WindowRT(x2,y2,mondepH,x2i,y2i);
            CreatePen(psSolid,penWidthReticule,clHighLight);
            posTrait := limiteCourbe.top + hautCarac div 2;
            Segment(x1i,posTrait,x2i,posTrait);
            // fl�ches
            if x1i < x2i
               then signe := 1
               else signe := -1;
            Segment(x1i + signe * marge * 3, posTrait + marge, x1i, posTrait);
            Segment(x1i + signe * marge * 3, posTrait - marge, x1i, posTrait);
            signe := -signe;
            Segment(x2i + signe * marge * 3, posTrait + marge, x2i, posTrait);
            Segment(x2i + signe * marge * 3, posTrait - marge, x2i, posTrait);
            CreatePen(psDash,1,clHighLight);
            Segment(x1i,limiteCourbe.bottom,x1i,posTrait);
            Segment(x2i,limiteCourbe.bottom,x2i,posTrait);
        end;
        lrY : begin
            WindowRT(x1,y1,mondepH,x1i,y1i);
            WindowRT(x2,y2,mondepH,x2i,y2i);
            CreatePen(psSolid,penWidthReticule,clHighLight);
            posTrait := LimiteCourbe.right - 4*largCarac;
            Segment(posTrait,y1i,posTrait,y2i);
            // fl�ches
            if y1i < y2i
               then signe := 1
               else signe := -1;
            Segment(posTrait - marge, y1i + 3 * signe * marge, posTrait, y1i);
            Segment(posTrait + marge, y1i + 3 * signe * marge, posTrait, y1i);
            signe := -signe;
            Segment(posTrait - marge, y2i + 3 * signe * marge, posTrait, y2i);
            Segment(posTrait + marge, y2i + 3 * signe * marge, posTrait, y2i);
            CreatePen(psDash,1,clHighLight);
            Segment(limiteCourbe.left,y1i,posTrait,y1i);
            Segment(limiteCourbe.left,y2i,posTrait,y2i);
        end;
        lrPente : ;
     end;
     pen.mode := pmCopy;
end end;

Procedure Tequivalence.DrawLatex(var latexStr : TstringList);

Procedure TraceDroiteLatex(x,y,pente : double;
   xMin,yMin,xMax,Ymax : double);
var x1,x2 : double;
begin
    if pente=0
       then begin
          x1 := xMin;
          x2 := xMax;
       end
       else begin
          try
          if pente>0
             then begin
                x1 := x+(yMin-y)/pente;
                x2 := x+(yMax-y)/pente;
             end
             else begin
                x1 := x+(yMax-y)/pente;
                x2 := x+(yMin-y)/pente;
             end;
         if x1<xMin then x1 := xMin;
         if x2>xMax then x2 := xMax;
         except
             x1 := xMin;
             x2 := xMax;
         end;
    end;
    latexStr.add('\draw[->] '+
     '(axis cs:'+floatToStrPoint(x1)+','+floatToStrPoint(y1)+')'+
     ' -- '+
     '(axis cs:'+floatToStrPoint(x2)+','+floatToStrPoint(y2)+');');
end;

var ecart,x3,x4,y3,y4,xAxe : double;
    strX,strY : string;
begin with Agraphe do begin
// PstyleTangente, pen.color
     if monde[mondepH].axe = nil
        then strY := formatReg(pHe / monde[mondepH].exposant) + ' '
        else strY := monde[mondepH].axe.formatNombre(pHe / monde[mondepH].exposant) + ' ';
     if monde[mondeX].axe = nil
        then strX := formatReg(ve / monde[mondeX].exposant) + ' '
        else strX := monde[mondeX].axe.formatNombre(ve / monde[mondeX].exposant) + ' ';
     case mondepH of
          mondeY : xAxe := monde[mondeX].mini;
          mondeDroit : xAxe := monde[mondeX].maxi;
          else xAxe := monde[mondeX].mini;
     end;
      case LigneRappel of
        lrEquivalence : begin
             x3 := x1-(x2-x1)/2;
             x4 := x2+(x2-x1)/2;
             y3 := (y1+y2)/2-abs(y2-y1);
             y4 := (y2+y1)/2+abs(y2-y1);
       latexStr.add('\path '+
          '(axis cs:'+floatToStrPoint(xAxe)+','+floatToStrPoint(phe)+') node(p1) {'+strX+'} '+
          '(axis cs:'+floatToStrPoint(ve)+','+floatToStrPoint(monde[mondepH].mini)+') node(p2) {'+strY+'} ');
             latexStr.add('\draw[->] (p1) -- (p1 |- p2);');
             latexStr.add('\draw[->] (p2) -- (p1 |- p2);');
             traceDroite(x1,y1,pente,x3,y3,x4,y4);
             traceDroite(x2,y2,pente,x3,y3,x4,y4);
             traceDroite(ve,pHe,pente,x3,y3,x4,y4);
        end;
        lrTangente : begin
             ecart := (monde[mondeX].maxi - monde[mondeX].mini)*LongueurTangente;
             x3 := ve+ecart;
             if x3>monde[mondeX].maxi then x3 := monde[mondeX].maxi;
             x4 := ve-ecart;
             if x4<monde[mondeX].mini then x4 := monde[mondeX].mini;
             ecart := (monde[MondepH].maxi - monde[MondepH].mini)*LongueurTangente;
             y3 := pHe+ecart;
             if y3>monde[mondepH].maxi then y3 := monde[mondepH].maxi;
             y4 := pHe-ecart;
             if y4<monde[mondepH].mini then y4 := monde[mondepH].mini;
             traceDroite(ve,pHe,pente,x4,y4,x3,y3);
             setUnitePente(mondepH);
             strX := unitePente.formatNomPenteUnite(pente);
             latexStr.add('\path '+
                    '(axis cs:'+floatToStrPoint(ve)+','+floatToStrPoint(phe)+') node(p1) {'+strX+'}');
        end;
        lrXdeY,lrReticule : begin
              latexStr.add('\path '+
                '(axis cs:'+floatToStrPoint(xAxe)+','+floatToStrPoint(phe)+') node(p1) {'+strX+'} '+
                '(axis cs:'+floatToStrPoint(ve)+','+floatToStrPoint(monde[mondepH].mini)+') node(p2) {'+strY+'} ');
              latexStr.add('\draw[->] (p1) -- (p1 |- p2);');
              latexStr.add('\draw[->] (p2) -- (p1 |- p2);');
        end;
     end;
end end;

Procedure Tequivalence.DrawFugitif;
var ecartX,ecartY : double;
begin with Agraphe do begin
     createPen(PstyleTangente,1,PcolorTangente);
     ecartX := (monde[mondeX].maxi - monde[mondeX].mini)*LongueurTangente;
     ecartY := (monde[MondepH].maxi - monde[MondepH].mini)*LongueurTangente;
     traceDroite(ve,pHe,pente,ve-ecartX,pHe-ecartY,
                                 ve+ecartX,pHe+ecartY);
     pen.mode := pmCOPY;
end end;

Procedure Tequivalence.Assign(Aequivalence : Tequivalence);
begin
     ve := Aequivalence.ve;
     phe := Aequivalence.pHe;
     pente := Aequivalence.pente;
     x2 := Aequivalence.x2;
     y2 := Aequivalence.y2;
     x1 := Aequivalence.x1;
     y1 := Aequivalence.y1;
     mondepH := Aequivalence.mondeph;
     varY := Aequivalence.varY;
     pen.color := Aequivalence.pen.color;
     modelisee := Aequivalence.modelisee;
end;

constructor Tequivalence.Create(v1,ph1,v2,ph2,v,ph,p : double;gr : TgrapheReg);
begin
     Inherited create(gr);
     ve := v;
     phe := pH;
     pente := p;
     x2 := v2;
     y2 := ph2;
     x1 := v1;
     y1 := ph1;
     mondepH := mondeY;
     varY := nil;
     modelisee := false;
end;

constructor Tequivalence.CreateVide(gr : TgrapheReg);
begin
     Inherited create(gr);
     ve := 10;
     phe := 10;
     pente := 1;
     x2 := 1;
     y2 := 1;
     x1 := 1;
     y1 := 1;
     mondepH := mondeY;
     varY := nil;
     pen.color := clBlack;
end;

Procedure TgrapheReg.AjouteEquivalence(i : integer;effaceDouble : boolean);
var penteTg,v11,vee,phee,ph11,v22,ph22 : double;
    codeDerivee : integer;
    IsModelisee : boolean;
    penteTgSI : double;

Function ChercheVpHModele : boolean;
var veAp,pHeAp : double;

procedure ChercheEqui(i : integer);
var v3,pH3,penteCourbe : double;
// intersection droite (veeAp pHeAp pentetg) et (v3 ph3 pentecourbe)
begin with courbes[indexCourbeEquivalence] do begin
     penteCourbe :=
        (valY[succ(i)]-valY[i])/
        (valX[succ(i)]-valX[i]);
     v3 := valX[i];
     ph3 := valY[i];
     vee := (pH3-penteCourbe*v3-pHeAp+penteTg*veAp)/(penteTg-penteCourbe);
     pHee := pH3+penteCourbe*(vee-v3);
end end; // ChercheEqui

var i2,i3,i3ap,iprec,isuiv : integer;
    derRef : double;
    k,kmax : integer;
    VcroissantSelonI,Croissant : boolean;
begin with courbes[indexCourbeEquivalence] do begin
     result := false;
     vee := monde[mondeX].Maxi;
     penteTg := valYder[i];
     if uniteSIGlb
        then penteTgSI := penteTg*coeffSIpente
        else penteTgSI := penteTg;
     derRef := abs(penteTg);
     i2 := i;
     croissant := true;
     iprec := i2-2;
     if iprec<0 then iprec := 0;
     isuiv := i2+2;
     if iprec>finc then iprec := finc;
     VcroissantSelonI := valX[isuiv]>valX[iprec];
     repeat inc(i2)
     until (i2>=finC) or
           (abs(valYder[i2])>(derRef*1.25)); // un peu d'hyst�r�sis
     if i2>=finC then begin
        croissant := false;
        i2 := i;
        repeat dec(i2)
        until (i2<0) or
              (abs(valYder[i2])>derRef);
        if i2<0 then exit;
        repeat dec(i2);
        until (i2<0) or
               (abs(valYder[i2])<derRef);
        if i2<0 then exit;
     end
     else begin
         repeat inc(i2);
         until (i2>finC) or
          (abs(valYder[i2])<derRef);
         if i2>=finC then exit;
     end;
     if not croissant then swap(i,i2);
// i2 : point apr�s l'�quivalence de m�me pente
     v11 := valX[i];
     ph11 := valY[i];
     v22 := valX[i2];
     ph22 := valY[i2];
// valeur approch�e de ve pHe
     veAp := (StoechBecherGlb*v11+StoechBuretteGlb*v22)/(StoechBecherGlb+StoechBuretteGlb);
     pHeAp := (StoechBecherGlb*pH11+StoechBuretteGlb*pH22)/(StoechBecherGlb+StoechBuretteGlb);
// cherche intersection avec courbe
     i3 := i;
     repeat
       inc(i3)
     until (valX[i3]>veAp) or (i3>i2); // hypoth�se VcroissantSelonI
     i3ap := i3;
     k := 0;
     kmax := (i2-i) div 2;
     if kmax>32 then kmax := 32;
     repeat
        inc(k);
        chercheEqui(i3);
        result := (vee-valX[succ(i3)])*
                  (vee-valX[i3])<=0;
// result si vee entre les deux points
        if not Result then begin
           if vee>valX[i3]
              then if VcroissantSelonI then inc(i3) else dec(i3)
              else if VcroissantSelonI then dec(i3) else inc(i3);
        end;
     until result or (k>kmax) or (i3>i2) or (i3<i);
     if not result then chercheEqui(i3Ap);
// on remet � la valeur approch�e la plus raisonnable
     result := (vee<monde[mondeX].maxi) and (vee>monde[mondeX].mini);
end end; // ChercheVpHmodele

Function ChercheVpH : boolean;
var veAp,pHeAp : double;

procedure ChercheEqui(i : integer);
var v3,pH3,penteCourbe : double;
// intersection droite (veeAp pHeAp pentetg) et (v3 ph3 pentecourbe)
begin
     penteCourbe :=
        (monde[MondeY].axe.valeur[succ(i)]-monde[mondeY].axe.valeur[i])/
        (monde[mondeX].axe.valeur[succ(i)]-monde[mondeX].axe.valeur[i]);
     v3 := monde[mondeX].axe.valeur[i];
     ph3 := monde[mondeY].axe.valeur[i];
     vee := (pH3-penteCourbe*v3-pHeAp+penteTg*veAp)/(penteTg-penteCourbe);
     pHee := pH3+penteCourbe*(vee-v3);
end; // ChercheEqui

var i2,i3,iprec,isuiv : integer;
    derRef : double;
    k,kmax : integer;
    VcroissantSelonI,Croissant : boolean;
begin
     result := false;
     vee := monde[mondeX].Maxi;
     penteTg := grandeurs[codeDerivee].valeur[i];
    if uniteSIGlb
        then penteTgSI := penteTg*coeffSIpente
        else penteTgSI := penteTg;
     derRef := abs(penteTg);
     i2 := i;
     croissant := true;
     iprec := i2-2;
     if iprec<0 then iprec := 0;
     isuiv := i2+2;
     if iprec>=pages[pageCourante].nmes
        then iprec := pred(pages[pageCourante].nmes);
     with monde[mondeX].axe do VcroissantSelonI :=
          valeur[isuiv]>valeur[iprec];
     repeat inc(i2)
     until (i2>=pages[pageCourante].nmes) or
           (abs(grandeurs[codeDerivee].valeur[i2])>derRef);
     if i2>=pages[pageCourante].nmes then begin
        croissant := false;
        i2 := i;
        repeat dec(i2)
        until (i2<0) or
              (abs(grandeurs[codeDerivee].valeur[i2])>derRef);
        if i2<0 then exit;
        repeat dec(i2);
        until (i2<0) or
               (abs(grandeurs[codeDerivee].valeur[i2])<derRef);
        if i2<0 then exit;
     end
     else begin
         repeat inc(i2);
         until (i2>=pages[pageCourante].nmes) or
          (abs(grandeurs[codeDerivee].valeur[i2])<derRef);
         if i2>=pages[pageCourante].nmes then exit;
     end;
     if not croissant then swap(i,i2);
// i2 : point apr�s l'�quivalence de m�me pente 
     v11 := monde[mondeX].axe.valeur[i];
     ph11 := monde[mondeY].axe.valeur[i];
     v22 := monde[mondeX].axe.valeur[i2]+(penteTg-grandeurs[codeDerivee].valeur[i2])*
        (monde[mondeX].axe.valeur[i2]-monde[mondeX].axe.valeur[pred(i2)])/
        (grandeurs[codeDerivee].valeur[i2]-grandeurs[codeDerivee].valeur[pred(i2)]);
     ph22 := monde[mondeY].axe.valeur[i2]+(v22-monde[mondeX].axe.valeur[i2])*
        (monde[mondeY].axe.valeur[i2]-monde[mondeY].axe.valeur[pred(i2)])/
        (monde[mondeX].axe.valeur[i2]-monde[mondeX].axe.valeur[pred(i2)]);
// valeur approch�e de ve pHe
     veAp := (StoechBecherGlb*v11+StoechBuretteGlb*v22)/(StoechBecherGlb+StoechBuretteGlb);
     pHeAp := (StoechBecherGlb*pH11+StoechBuretteGlb*pH22)/(StoechBecherGlb+StoechBuretteGlb);
// cherche intersection avec courbe
     i3 := (i+i2) div 2;
     k := 0;
     kmax := abs(i3-i2);
     if kmax>16 then kmax := 16;
     repeat
        inc(k);
        chercheEqui(i3);
        result := (vee-monde[mondeX].axe.valeur[succ(i3)])*
                  (vee-monde[mondeX].axe.valeur[i3])<=0;
// result si vee entre les deux points
        if not Result then begin
           if vee>monde[mondeX].axe.valeur[i3]
               then if VcroissantSelonI then inc(i3) else dec(i3)
               else if VcroissantSelonI then dec(i3) else inc(i3);
        end;
     until result or (k>kmax) or (i3>i2) or (i3<i);
     if not result then chercheEqui((i+i2) div 2);
// on remet � la valeur approch�e la plus raisonnable
     result := (vee<monde[mondeX].maxi) and (vee>monde[mondeX].mini);
end; // ChercheVpH

Procedure CherchePente;
begin
    if IsModelisee
        then begin
           vee := courbes[indexCourbeEquivalence].valX[i];
           pHee := courbes[indexCourbeEquivalence].valY[i];
           penteTg := courbes[indexCourbeEquivalence].valYder[i];
        end
        else begin
           vee := monde[mondeX].axe.valeur[i];
           pHee := monde[mondeDerivee].axe.valeur[i];
           penteTg := Grandeurs[codeDerivee].valeur[i];
        end;
     v11 := vee;
     v22 := vee;
     pH11 := pHee;
     pH22 := pHee;
end;// CherchePente

var t : integer;
    NewEquivalence : Tequivalence;
    dx,dy : double;
begin // AjouteEquivalence
     codeDerivee := indexDerivee(monde[mondeDerivee].axe,monde[mondeX].axe,true,true);
     if codeDerivee=grandeurInconnue then exit;
     IsModelisee := not(courbes[indexCourbeEquivalence].courbeExp) and
                    ((courbes[indexCourbeEquivalence].indexModele=1) or
                     (courbes[indexCourbeEquivalence].indexModele=indexSpline)) and
                    (courbes[indexCourbeEquivalence].valYder<>nil);
     case LigneRappelCourante of
          lrEquivalence : if IsModelisee
            then begin if not ChercheVpHModele then exit end
            else begin if not ChercheVpH then exit end;
          lrTangente : CherchePente;
     end;
     with monde[mondeX] do dx := (maxi-mini)/128; // +- 8 pixels sur 1024
     with monde[mondeDerivee] do dy := (maxi-mini)/128;
     for t := 0 to Pred(equivalences[pageCourante].count) do
         with equivalences[pageCourante].items[t] do begin
             if varY=nil then varY := monde[mondeY].axe;
             if (abs(x1-monde[mondeX].axe.valeur[i])<dx) and
                (abs(y1-varY.valeur[i])<dy) then begin
                  if effaceDouble then begin
                     Draw;
                     equivalences[pageCourante].Delete(t);
                  end;
                  exit;
            end;
        end;
      NewEquivalence := Tequivalence.Create(v11,ph11,v22,ph22,vee,phee,penteTgSI,self);
      NewEquivalence.modelisee := IsModelisee;
      NewEquivalence.mondepH := mondeDerivee;
      NewEquivalence.ligneRappel := ligneRappelCourante;
      equivalences[pageCourante].Add(NewEquivalence);
      NewEquivalence.Draw;
      StatusSegment[0].clear;
      StatusSegment[1].clear;
      StatusSegment[1].Add(monde[mondeX].Axe.formatNomEtUnite(vee));
      StatusSegment[1].Add(monde[mondeDerivee].Axe.formatNomEtUnite(pHee));
      StatusSegment[1].Add(grandeurs[codeDerivee].formatNomEtUnite(penteTg));
      StatusSegment[0].Add(monde[mondeX].Axe.Nom);
      StatusSegment[0].Add(monde[mondeDerivee].Axe.Nom);
      StatusSegment[0].Add(grandeurs[codeDerivee].Nom);

end;// AjouteEquivalence

Procedure Tequivalence.SetValeurEquivalence(i : integer);
var codeDerivee : integer;
begin with Agraphe do begin
     if modelisee
        then begin
           ve := courbes[indexCourbeEquivalence].valX[i];
           pHe := courbes[indexCourbeEquivalence].valY[i];
           try
           pente := courbes[indexCourbeEquivalence].valYder[i];
           except
           pente := 0;
           modelisee := false;
           end;
        end
        else begin
           if varY=nil then varY := monde[mondeY].axe;
           codeDerivee := indexDerivee(varY,monde[mondeX].axe,true,true);
           ve := monde[mondeX].axe.valeur[i];
           pHe := varY.valeur[i];
           if codeDerivee=grandeurInconnue
             then pente := 0
             else pente := Grandeurs[codeDerivee].valeur[i];
        end;   
end end;// SetValeurEquivalence

procedure TgrapheReg.RemplitTableauEquivalence;
var ligne,i : integer;
    codeDerivee : integer;
begin
     if LigneRappelCourante in [lrEquivalence,lrTangente]
        then codeDerivee := indexDerivee(monde[mondeDerivee].axe,monde[mondeX].axe,true,true)
        else codeDerivee := 0;
     if codeDerivee=grandeurInconnue then exit;
     if curseurModeleDlg=nil then
        Application.CreateForm(TcurseurModeleDlg,curseurModeleDlg);
// pr�voir le cas o� le curseur pointe sur deux courbes diff�rentes
// premier point gain, deuxi�me point phase par exemple        
     with curseurModeleDlg.tableau do begin
          rowCount := equivalences[pageCourante].count+3;
          colCount := 3;
          cells[0,0] := monde[mondeX].axe.nom;
          Cells[0,1] := monde[mondeX].axe.NomUnite;
          cells[1,0] := monde[mondeDerivee].axe.nom;
          Cells[1,1] := monde[mondeDerivee].axe.NomUnite;
          if LigneRappelCourante in [lrXdeY,lrReticule]
             then cells[2,0] := stCommentaire
             else begin
                 cells[2,0] := grandeurs[codeDerivee].nom;
                 Cells[2,1] := grandeurs[codeDerivee].NomUnite;
             end;
          ligne := 1;
          for i := 0 to pred(equivalences[pageCourante].count) do
          with equivalences[pageCourante].items[i] do begin
              inc(ligne);
              cells[0,ligne] := formatReg(ve);
              cells[1,ligne] := formatReg(phe);
              if LigneRappel in [lrTangente,lrEquivalence]
                 then cells[2,ligne] := formatReg(pente)
                 else cells[2,ligne] := commentaire;
          end;
          inc(ligne);            // ??
          for i := 0 to 2 do cells[i,ligne] := ''
     end;
end;

Procedure Tequivalence.Store;
begin
  writeln(fichier,pente); {1}
  writeln(fichier,ve);{2}
  writeln(fichier,pHe);{3}
  writeln(fichier,vei);{4}
  writeln(fichier,phei);{5}
  writeln(fichier,mondepH);{6}
  writeln(fichier,ord(LigneRappel));{7}
  writeln(fichier,ord(modelisee));{8}
  ecritChaineRW3(commentaire);{9}
  writeln(fichier,pen.color);{10}
end;

(*
Procedure Tequivalence.StoreXML;
begin
  writeFloat('Pente',pente);
  writeFloat('Ve',ve);{2}
  writeFloat('Phe',pHe);{3}
  writeInteger('monde',mondepH);
  writeInteger('Rappel',ord(LigneRappel));
  writeBool(modelisee);
  writeString(commentaire);
  writeInteger(pen.color);
end;
*)

Procedure Tequivalence.Load;
var i,imax : integer;
    z : integer;
    zz : longWord;
begin
  imax := NbreLigneWin(ligneWin);
  readln(fichier,pente); {1}
  readln(fichier,ve); {2}
  readln(fichier,Phe); {3}
  readln(fichier,vei); {4}
  readln(fichier,phei);{5}
  readln(fichier,mondepH);{6}
  readln(fichier,z);{7}
  ligneRappel := TligneRappel(z);
  modelisee := litBooleanWin;{8}
  litLigneWin;
  commentaire := ligneWin;{9}
  litligneWin;
  try
  zz := strToInt(ligneWin);
  pen.color := zz;
  except
     pen.color := clBlack;
  end;
  for i := 11 to imax do readln(fichier);
end;

(*
Procedure Tequivalence.LoadXML;
begin
  readln(fichier,pente := readFloat;
  readln(fichier,ve := readFloat;
  readln(fichier,Phe := readFloat;
  readln(fichier,mondepH := readInteger;
  ligneRappel := TligneRappel(z);
  modelisee := litBool;
  commentaire := litString;
  pen.color := litInteger;
end;
*)

