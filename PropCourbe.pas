{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit PropCourbe;

  {$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids,
  uniteker,graphker, StdCtrls, Spin, ExtCtrls,
  regutil, math, maths, compile, Buttons;

type
  TPropCourbeForm = class(TForm)
    Grid: TStringGrid;
    LissageGB: TGroupBox;
    OrdreLissageSE: TSpinEdit;
    DeriveGB: TGroupBox;
    Label1: TLabel;
    NbreEdit: TEdit;
    DegreRG: TRadioGroup;
    NbreSpinButton: TSpinEdit;
    Label2: TLabel;
    SplineGB: TGroupBox;
    Label3: TLabel;
    OrdreSplineSE: TSpinEdit;
    ExitBtn: TBitBtn;
    TexteGB: TGroupBox;
    LabelTaille: TLabel;
    SpinEditHauteur: TSpinEdit;
    NbreLabel: TLabel;
    NbreSE: TSpinEdit;
    ExtrapoleDerCB: TCheckBox;
    CourbeEdit: TEdit;
    Panel1: TPanel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OrdreSplineSEChange(Sender: TObject);
    procedure NbreSpinButtonDownClick(Sender: TObject);
    procedure NbreSpinButtonUpClick(Sender: TObject);
    procedure DegreRGClick(Sender: TObject);
    procedure OrdreLissageSEChange(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure SpinEditHauteurChange(Sender: TObject);
    procedure NbreSEChange(Sender: TObject);
    procedure ExtrapoleDerCBClick(Sender: TObject);
  private
    varF : Tunite;
    procedure ModifDerivee;
  public
    Acourbe : Tcourbe;
    Agraphe : TgrapheReg;
    procedure MaJ;
  end;

var
  PropCourbeForm: TPropCourbeForm;

implementation

  {$R *.lfm}

procedure TPropCourbeForm.FormDestroy(Sender: TObject);
begin
     varF.free;
     PropCourbeForm := nil;
     inherited 
end;

procedure TPropCourbeForm.FormCreate(Sender: TObject);
begin with Grid do begin
       cells[0,0] := 'Période';
       cells[0,1] := 'Fréquence';
       cells[0,2] := 'Crête à crête';
       cells[0,3] := 'Efficace';
       cells[0,4] := 'Moyenne';
       varF := Tunite.create;
end end;

procedure TPropCourbeForm.MaJ;
begin with aCourbe do begin
   courbeEdit.text := 'Caractéristiques de '+varY.nom+'('+varX.nom+')';
   courbeEdit.font.Color := acourbe.couleur;
   calcul;
   varF.RecopieUnite(varX);
   varF.uniteDonnee := false;
   varF.uniteImposee := false;
   varF.InverseUnite;
   if isNan(periode)
      then begin
           Grid.cells[1,0] := '?';
           Grid.cells[1,1] := '?';
           Grid.cells[1,3] := '';
      end
      else begin
          Grid.cells[1,0] := varX.formatValeurEtUnite(periode);
          Grid.cells[1,1] := varF.formatValeurEtUnite(1/periode);
          Grid.cells[1,3] := varY.formatValeurEtUnite(ValeurEff);
      end;
   Grid.cells[1,2] := varY.formatValeurEtUnite(ValeurMax-ValeurMin);
   Grid.cells[1,4] := varY.formatValeurEtUnite(ValeurMoy);
end end;

procedure TPropCourbeForm.FormActivate(Sender: TObject);
var Gder : Tsetgrandeur;
    i : integer;
    codeX,codeY : integer;
begin
    inherited;
    spinEditHauteur.Value := texteGrapheSize;
    ExtrapoleDerCB.checked := ExtrapoleDerivee;
    NbreSE.value := NbreTexteMax;
    splineGB.Visible := (trLigne in Acourbe.trace) and
                        (Acourbe.IndexModele=indexSpline);
    if Acourbe.IndexModele=indexSpline
    then begin
       splineGB.caption := 'Lissage par une B-spline';
       ordreSplineSE.MaxValue := ordreMaxSpline;
    end
    else splineGB.caption := 'Lissage par un sinc';
    lissageGB.Visible := (Acourbe.varY.fonct.genreC in [g_lissageGlissant,g_lissageCentre]) and
                         (Acourbe.varY.fonct.calcul.OperandDebut=nil);
    OrdreLissageSE.Value := OrdreFiltrage;
    OrdreSplineSE.Value := OrdreLissage;
    TexteGB.visible := (Acourbe.varY.fonct.genreC=g_texte) or
                       (trTexte in Acourbe.trace);
    NbreEdit.text := IntToStr(NbrePointDerivee);
    DegreRG.itemIndex := DegreDerivee-1;
    Gder := [];
    for i := 1 to pred(NbreGrandeurs) do begin
        if grandeurs[i].fonct.genreC=g_derivee then include(GDer,i);
        if grandeurs[i].fonct.genreC=g_enveloppe then include(GDer,i);
    end;
    codeX := Acourbe.varX.indexG;
    codeY := Acourbe.varY.indexG;
    deriveGB.visible := (codeX in Gder) or
                        (codeY in Gder) or
                        ((Acourbe.varX.fonct.depend * Gder)<>[]) or
                        ((Acourbe.varY.fonct.depend * Gder)<>[]) or
                        (trVitesse in Acourbe.trace) or
                        (trAcceleration in Acourbe.trace);
end;

procedure TPropCourbeForm.OrdreSplineSEChange(Sender: TObject);
begin
    OrdreLissage := OrdreSplineSE.Value;
    Agraphe.paintBox.refresh;
end;

procedure TPropCourbeForm.ModifDerivee;
begin
    OrdreFiltrage := OrdreLissageSE.Value;
    NbreEdit.text := IntToStr(NbrePointDerivee);
    recalculE;
    Application.MainForm.Perform(WM_Reg_Maj,MajValeur,0)
end;

procedure TPropCourbeForm.NbreSpinButtonDownClick(Sender: TObject);
begin
    if NbrePointDerivee>MinPointsDerivee then begin
          dec(NbrePointDerivee,2);
          ModifDerivee;
    end;
end;

procedure TPropCourbeForm.NbreSpinButtonUpClick(Sender: TObject);
begin
     if NbrePointDerivee<MaxPointsDerivee
             then begin
                inc(NbrePointDerivee,2);
                ModifDerivee;
             end;
end;

procedure TPropCourbeForm.DegreRGClick(Sender: TObject);
begin
    DegreDerivee := 1+DegreRG.itemIndex;
    ModifDerivee
end;

procedure TPropCourbeForm.OrdreLissageSEChange(Sender: TObject);
begin
    ModifDerivee;
end;

procedure TPropCourbeForm.ExitBtnClick(Sender: TObject);
begin
  close
end;

procedure TPropCourbeForm.SpinEditHauteurChange(Sender: TObject);
begin
     TexteGrapheSize := spinEditHauteur.value;
     Application.MainForm.Perform(WM_Reg_Maj,MajValeur,0)
end;

procedure TPropCourbeForm.NbreSEChange(Sender: TObject);
begin
      NbreTexteMax := NbreSE.value;
      Application.MainForm.Perform(WM_Reg_Maj,MajValeur,0)
end;

procedure TPropCourbeForm.ExtrapoleDerCBClick(Sender: TObject);
begin
    ExtrapoleDerivee := ExtrapoleDerCB.checked;
    ModifDerivee
end;

end.

