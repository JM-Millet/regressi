{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit selpage;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, sysutils, ExtCtrls, dialogs,
  compile, CheckLst, ComCtrls;

type
  TSelectPageDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    AllBtn: TBitBtn;
    OneBtn: TBitBtn;
    PageListBox: TCheckListBox;
    StatusBar: TStatusBar;
    AllOKBtn: TBitBtn;
    OneOKBtn: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure AllBtnClick(Sender: TObject);
    procedure OneBtnClick(Sender: TObject);
    procedure AllOKBtnClick(Sender: TObject);
    procedure OneOKBtnClick(Sender: TObject);
  private
  public
    appelPrint : boolean;
  end;

var
  SelectPageDlg: TSelectPageDlg;

implementation

uses regutil;

  {$R *.lfm}

procedure TSelectPageDlg.FormActivate(Sender: TObject);
var p : integer;
    hauteur,index,i : integer;
    LabelConst : string;
begin
     inherited;
     PageListBox.clear;
     for p := 1 to NbrePages do with pages[p] do begin
          labelConst := '';
          for i := 0 to pred(ListeConstAff.count) do begin
              index := indexNom(ListeConstAff[i]);
              if index<>grandeurInconnue then
                 labelConst := labelConst+grandeurs[index].FormatNomEtUnite(valeurConst[index])+';'
         end;
         PageListBox.Items.Add(IntToStr(p)+' : '+labelConst+commentaireP);
         PageListBox.checked[p-1] := active;
     end;
     hauteur := NbrePages*22+60;
     if statusBar.Visible
        then begin
           hauteur := hauteur + 30;
           if hauteur<260 then hauteur := 260;
        end
        else if hauteur<240 then hauteur := 240;
     if hauteur>420
         then height := 420
         else height := hauteur;
end;

procedure TSelectPageDlg.OKBtnClick(Sender: TObject);
var p : codePage;
    N : integer;
begin
     N := 0;
     for p := 1 to NbrePages do begin
         pages[p].active := PageListBox.checked[p-1];
         if pages[p].active then inc(N);
     end;
     if N=0 then begin
        pages[pageCourante].active := true;
        N := 1;
     end;
     if not pages[pageCourante].active then begin
        p := 1;
        while (p<NbrePages) and not pages[p].active do inc(p);
        pageCourante := p;
     end;
     if appelPrint then N := 0;
     Application.MainForm.perform(WM_Reg_Maj,MajSelectPage,N);
end;

procedure TSelectPageDlg.AllBtnClick(Sender: TObject);
var p : codePage;
begin
     for p := 1 to NbrePages do
         PageListBox.checked[p-1] := true;
end;

procedure TSelectPageDlg.OneBtnClick(Sender: TObject);
var p : codePage;
begin
     for p := 1 to NbrePages do
         PageListBox.checked[p-1] := false;
     PageListBox.checked[pageCourante-1] := true;
end;

procedure TSelectPageDlg.AllOKBtnClick(Sender: TObject);
var p : codePage;
begin
     for p := 1 to NbrePages do pages[p].active := true;
     Application.MainForm.perform(WM_Reg_Maj,MajSelectPage,NbrePages);
end;

procedure TSelectPageDlg.OneOKBtnClick(Sender: TObject);
var p : codePage;
begin
     for p := 1 to NbrePages do pages[p].active := false;
     pages[pageCourante].active := true;
     Application.MainForm.perform(WM_Reg_Maj,MajSelectPage,1);
end;

end.
