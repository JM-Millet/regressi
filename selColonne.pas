{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit selColonne;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, sysutils, ExtCtrls,  dialogs,
  compile, CheckLst;

type
  TSelectColonneDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    GrandeursListBox: TCheckListBox;
    CSVCB: TCheckBox;
    ValeursSeulesCB: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  public
  //   SelVariab: TList<Integer>;
  end;

var
  SelectColonneDlg: TSelectColonneDlg;

implementation

uses regutil;

  {$R *.lfm}

procedure TSelectColonneDlg.FormActivate(Sender: TObject);
var p : integer;
    hauteur : integer;
    iCalc : codeGrandeur;
begin
     inherited;
  //   SelVariab := TList<Integer>.Create();
     GrandeursListBox.clear;
     for p := 0 to pred(NbreVariab) do begin
         iCalc := indexVariab[p];
         with grandeurs[iCalc] do begin
            GrandeursListBox.Items.Add(nom+' : '+fonct.expression);
            GrandeursListBox.checked[p] := active;
         end;
     end;
     hauteur := NbrePages*22+60;
     if hauteur<240 then hauteur := 240;
     if hauteur>420
         then height := 420
         else height := hauteur;
end;

procedure TSelectColonneDlg.FormDestroy(Sender: TObject);
begin
     //SelVariab.Free;
end;

procedure TSelectColonneDlg.OKBtnClick(Sender: TObject);
var p : codeGrandeur;
begin
     for p := 0 to pred(NbreVariab) do
         if GrandeursListBox.checked[p] then
         //   selVariab.Add(indexVariab[p]);
end;

end.
