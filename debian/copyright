Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded:
 sdDebug.pas
 sdStreams.pas
 simdesign.inc
 Regressi.compiled
 Regressi.res
Source: https://regressi.fr/source/RegressiLazarus.zip, https://svn.code.sf.net/p/lazarus-ccr/svn/components/tdi/opm/TDINoteBook.zip
Upstream-Name: regressi
Upstream-Contact: Jean Michel Millet <jm.millet@orange.fr>

Files:
 *
Copyright:
 2000-2021 Jean Michel Millet <jm.millet@orange.fr>
License: GPL-3+

Files:
 FitsJMM/*
Copyright: 2013-2016, Evgeniy Dikov
License: Expat

Files:
 tdi/*
Copyright: 2012, Daniel Simões de Almeida
License: LGPL-2.1+

Files:
 resample.pas
Copyright:
 1997, 98 by Anders Melander
 2024, Jean-Michel Millet <jm.millet@orange.fr>
License: GPL-3+ OR public-domain

Files:
 fmath.pas
 fspec.pas
Copyright:
 2003 Jean Debord <debord.jean@orange.fr,jean.debord@unilim.fr>
License: LGPL-2.1+

Files:
 debian/*
Copyright:
 2024 Georges Khaznadar <georgesk@debian.org>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 You can also get a copy of the license accessing the address:
 http://www.opensource.org/licenses/lgpl-license.php
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: public-domain
 The public domain (PD) consists of all the creative work to which no
 exclusive intellectual property rights apply. Those rights may have
 expired, been forfeited, expressly waived, or may be
 inapplicable. Because no one holds the exclusive rights, anyone can
 legally use or reference those works without permission.
