Document: regressi
Title: Debian regressi Manual
Author: <insert document author here>
Abstract: This manual describes what regressi is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/regressi/regressi.sgml.gz

Format: postscript
Files: /usr/share/doc/regressi/regressi.ps.gz

Format: text
Files: /usr/share/doc/regressi/regressi.text.gz

Format: HTML
Index: /usr/share/doc/regressi/html/index.html
Files: /usr/share/doc/regressi/html/*.html
