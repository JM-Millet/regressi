{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit regdde;

  {$MODE Delphi}

// Regressi serveur pour recevoir les ordres de l'acquisition

interface

uses
  SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, ClipBrd, Grids,
   constreg, regutil, compile, math, maths, filerrr, valeurs, graphker;

type
  TformatSepare = (sTab,sBlanc,sCSV);

  { TFormDDE }

  TFormDDE = class(TForm)
    Editor: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    avecTab : TformatSepare;
    CoeffTempsVellman,CoeffCH1Vellman,CoeffCH2Vellman : double;
    ZeroVellman : integer;
    donnees : TstringList;
    DecodeVariab,DecodeConst : array[codeGrandeur] of integer;
    isPrecVariab,isPrecConst : array[codeGrandeur] of boolean;
    NbrePrecVariab,NbrePrecConst : integer;    
    NbreVariabAlire : integer;
    separateurCSV : char;
    function GetData : boolean;
    function AjouteData : integer;
    Procedure AjouteDataPageCourante;
    Procedure AjouteColPageCourante;
    Function ExtraitValeur(Agenre : TgenreGrandeur;const s : string;imax : integer) : boolean;
    Function ExtraitCoord(var ligne : integer) : boolean;
    Function ExtraitIncert(var ligne : integer) : boolean;
    Function ExtraitVariableTexte(var ligne : integer) : boolean;
    Function ExtraitNomFichier(var ligne : integer) : boolean;
    Function ExtraitOrigineTemps(var ligne : integer) : boolean;
    Procedure extraitNom(const s : String;var ListeN,ListeU : TstringList;creation : boolean);
    procedure ChercheTab;
    procedure MemoAddComm(Astr : string);
    function ChercheEnTeteVellman : boolean;
    Procedure ExtraitVellman;
  public
     Procedure RazRTF;
     procedure AjouteMemo(Amemo : TcustomMemo);
     procedure AjouteGrid(Agrid : TstringGrid);
     procedure AjouteStringList(AstringList : TstringList);
     procedure EnvoieRTF;
     Function  ImportePressePapier : boolean;
     Function  ImporteFichierTableur(const NomFichier : string) : boolean;
     Function  AjouteFichierTableur(const NomFichier : string) : boolean;
     Procedure AjoutePressePapier;
     Procedure ExporteFichierTableur(const NomFichier : string);
     Procedure FocusAcquisition;
  end;

var
  FormDDE: TFormDDE;

implementation

uses RegMain, Graphvar, AffecteNom, addPageExp, options, selColonne;

const NbreTab = 8;

var indexTime,indexDate,indexDateTime,indexTexte : integer;

  {$R *.lfm}

Function LigneVide(Astr : String) : boolean;
begin
      trimComplet(Astr);
      result := Astr=''
end;

Procedure TformDDE.extraitNom(const s : String;
                        var ListeN,ListeU : TstringList;creation : boolean);

Procedure extraitNomLoc;
var j,debut,long : integer;
    nomU : String;
    posParentheseO,posParentheseF : integer;
begin
    j := 1;
    Repeat
        while (j<length(s)) and
              not isLettre(s[j]) do inc(j);
        debut := j;
        while (j<=length(s)) and
              isCaracGrandeur(s[j]) do inc(j);
        if j<>debut then begin
            long := j-debut;
            nomU := copy(s,debut+long,length(s));
            if long>longNom then long := longNom;
            listeN.Add(copy(s,debut,long));
            posParentheseO := pos('(',nomU); // grandeur sous forme : nom (unite)
            posParentheseF := pos(')',nomU);
            if (posParentheseO>0) and (posParentheseF>posParentheseO)
               then begin
                   nomU := copy(nomU,posParentheseO+1,posParentheseF-posParentheseO-1);
                   repeat inc(j) until s[j]=')';
               end
               else nomU := '';
            listeU.Add(nomU);
        end;
     Until (j>=length(s)) or (listeN.count=32);
end;

Procedure extraitNomTab;
var j,debut,posIn : integer;
    nomLoc,nomU : String;
    posParentheseO,posParentheseF : integer;
begin
    j := 1;
    Repeat
        debut := j;
        while (j<=length(s)) and (s[j]<>crTab) do inc(j);
        nomLoc := copy(s,debut,j-debut);
        posIn := pos(' in ',nomLoc);
        if posIn>0 then begin
           nomLoc[posIn] := '(';
           delete(nomLoc,posIn+1,3);
           nomLoc := nomLoc + ')';
        end;
        trimComplet(nomLoc);
        if nomLoc='Magnum' then nomLoc := 'x';
        posParentheseO := pos('(',nomLoc);
        posParentheseF := pos(')',nomLoc);
        if (posParentheseO>0) and (posParentheseF>(posParentheseO+1))
           then begin
              nomU := copy(nomLoc,posParentheseO+1,posParentheseF-posParentheseO-1);
              nomLoc := copy(nomLoc,1,pred(posParentheseO));
           end
           else nomU := '';
        if (nomLoc='') and Creation then nomLoc := 'x'+intToStr(listeN.count);
        listeN.Add(nomLoc);
        listeU.Add(nomU);
        inc(j);
     Until (j>length(s)) or (listeN.count=32);
     if listeN[listeN.count-1]='' then begin
            // cas de dataDirect #9#9 pour trois variables ?
        if creation then listeN.Add('Z') else listeN.Add('');
        listeU.Add('');
     end;
end;

Procedure extraitNomCSV;
var j,debut : integer;
    nomLoc,nomU : String;
    posParentheseO,posParentheseF : integer;
begin
    j := 1;
    Repeat
        debut := j;
        while (j<=length(s)) and (s[j]<>separateurCSV) do inc(j);
        nomLoc := copy(s,debut,j-debut);
        trimComplet(nomLoc);
        posParentheseO := pos('(',nomLoc);
        posParentheseF := pos(')',nomLoc);
        if (posParentheseO>0) and (posParentheseF>(posParentheseO+1))
           then begin
              nomU := copy(nomLoc,posParentheseO+1,posParentheseF-posParentheseO-1);
              nomLoc := copy(nomLoc,1,pred(posParentheseO));
           end
           else nomU := '';
        if (nomLoc='') and Creation then nomLoc := 'x'+intToStr(listeN.count);   
        listeN.Add(nomLoc);
        listeU.Add(nomU);
        inc(j);
     Until (j>length(s)) or (listeN.count=32);
end;

begin
   listeN.clear;
   listeU.clear;
   case avecTab of
        sTab :  extraitNomTab;
        sBlanc : extraitNomLoc;
        sCSV : extraitNomCSV;
   end;
end;

Function TFormDDE.ExtraitValeur(Agenre : TgenreGrandeur;
                             const s : string;imax : integer) : boolean;

Function ExtraitValeurLoc : boolean;
var jfin,jdebut : integer;
    i : integer;
    index : integer;
    valeur : array[codeGrandeur] of double;
    strValeur,texte : string;
    posDate,posTime : integer;
begin with pages[pageCourante] do begin
    result := (length(s)>0) and (s[1]<>SymbReg2);
    if not result then exit;
    i := 0;jfin := 1;
    try
    Repeat
        jdebut := jfin;
        while (jdebut<length(s)) and
              not charinset(s[jdebut],chiffreWin) do inc(jdebut);
        jfin := jdebut;
        while (jfin<=length(s)) and
              charinset(s[jfin],caracNombre) do inc(jfin);
        strValeur := copy(s,jdebut,jfin-jdebut);
        posTime := pos(':',strValeur);
        posDate := pos('/',strValeur);
        if (posTime>0) or (posDate>0)
            then begin
                try
                if Agenre=variable
                    then index := decodeVariab[i]
                    else index := decodeConst[i];
                if posTime>0
                   then if posDate>0
                        then begin
                            valeur[i] := StrToDateTime(strValeur);
                            Grandeurs[index].formatU := fDateTime;
                            indexDateTime := index;
                        end
                        else begin
                           valeur[i] := StrToTime(strValeur);
                           indexTime := index;
                           Grandeurs[index].formatU := fTime;
                        end
                   else begin
                        valeur[i] := StrToDate(strValeur);
                        indexDate := index;
                        Grandeurs[index].formatU := fDate;                        
                   end;
                 except
                   valeur[i] := Nan;
                 end;
             end
             else if i=indexTexte
               then texte := strValeur
               else valeur[i] := strToFloatWin(strValeur);
        inc(i);
    Until (jfin>=length(s)) or (i>=imax);
    imax := pred(i);
    if Agenre=variable
       then begin
           nmes := nmes+1;
           for i := 0 to imax do begin
               index := decodeVariab[i];
               if i=indexTexte
                  then texteVar[index,pred(Nmes)] := texte //??
                     else if isPrecVariab[i]
                          then incertVar[index,pred(Nmes)] := valeur[i]
                          else valeurVar[index,pred(Nmes)] := valeur[i];
           end;
       end
       else for i := 0 to imax do begin
            index := decodeConst[i];
            if isPrecConst[i]
               then incertConst[index] := valeur[i]
               else valeurConst[index] := valeur[i];
       end
     except
       result := false
     end;
end end; // extraitValeurLoc

Function ExtraitValeurTab : boolean;
var j,jDebut : integer;
    i : integer;
    k,kk : integer;
    index : integer;
    valeur : array[codeGrandeur] of double;
    strValeur : String;
    posDate,posTime : integer;
    valeurNonDef : boolean;

Procedure TrimStrValeur;
var longueur,posCourant : integer;
begin
     longueur := Length(strValeur);
     posCourant := 1;
     while (posCourant<=longueur) do
         if charinset(strValeur[posCourant],chiffreWin+[':','/'])
            then inc(posCourant)
            else if (posCourant=longueur) and
                    (pos(strValeur[posCourant],caracPrefixe)>0)
                 then convertitPrefixe(strValeur)
                 else begin
                      Delete(strValeur,posCourant,1);
                      dec(longueur);
                 end;
end;

begin with pages[pageCourante] do begin
    result := (length(s)>0) and (s[1]<>SymbReg2);
    if not result then exit;
    i := 0;j := 1;
    valeurNonDef := false;
    result := false;
    Repeat
        jDebut := j;
        while (j<=length(s)) and (s[j]<>crTab) do inc(j);
        { s[j]=crTab }
        strValeur := copy(s,jdebut,j-jdebut);
        trimStrValeur;
        posTime := pos(':',strValeur);
        posDate := pos('/',strValeur);
        try
        if (posTime>0) or (posDate>0)
            then begin
                if Agenre=variable
                    then index := decodeVariab[i]
                    else index := decodeConst[i];
                if posTime>0
                   then if posDate>0
                        then begin
                            valeur[i] := StrToDateTime(strValeur);
                            Grandeurs[index].formatU := fDateTime;
                        end
                        else begin
                           valeur[i] := StrToTime(strValeur);
                           indexTime := index;
                           Grandeurs[index].formatU := fTime;
                        end
                   else begin
                        valeur[i] := StrToDate(strValeur);
                        indexDate := index;
                        Grandeurs[index].formatU := fDate;
                   end
             end
             else valeur[i] := strToFloatWin(strValeur);
        result := true;
        except
              valeur[i] := Nan;
              traceDefaut := tdPoint;
              valeurNonDef := true;
        end;
        inc(i);
        inc(j);
        if (i=imax) and valeurNonDef and (j<length(s)) then begin
            k := -1;
            repeat inc(k)
            until (k=imax) or isNan(valeur[k]);
            if k<imax then begin
               for kk := k to (imax-2) do valeur[kk] := valeur[succ(kk)];
               dec(i);
            end;
        end;
    until (j>length(s)) or (i=imax);
    imax := pred(i);
    if (imax<0) or isNan(valeur[0]) then exit;
    if Agenre=variable
       then begin
           nmes := nmes+1;
           for i := 0 to imax do begin
               index := decodeVariab[i];
               if isPrecVariab[i]
                  then incertVar[index,pred(Nmes)] := valeur[i]
                  else valeurVar[index,pred(Nmes)] := valeur[i];
               // texteVar[index,pred(Nmes)] := texte;
           end
       end
       else for i := 0 to imax do begin
               index := decodeConst[i];
               if isPrecConst[i]
                   then incertConst[index] := valeur[i]
                   else valeurConst[index] := valeur[i];
       end;
end end; // extraitValeurTab

Function ExtraitValeurCSV : boolean;
var j,jDebut : integer;
    i : integer;
    k,kk : integer;
    index : integer;
    valeur : array[codeGrandeur] of double;
    strValeur : string;
    posDate,posTime : integer;
    valeurNonDef : boolean;

Procedure TrimStrValeur;
var longueur,posCourant : integer;
begin
     longueur := Length(strValeur);
     posCourant := 1;
     while (posCourant<=longueur) do
         if charinset(strValeur[posCourant],chiffreWin+[':','/'])
            then inc(posCourant)
            else begin
                 Delete(strValeur,posCourant,1);
                 dec(longueur);
            end;
end;

begin with pages[pageCourante] do begin
    result := length(s)>0;
    if not result then exit;
    i := 0;j := 1;
    valeurNonDef := false;
    result := false;
    Repeat
        jDebut := j;
        while (j<=length(s)) and (s[j]<>separateurCSV) do inc(j);
// s[j]=separateur 
        strValeur := copy(s,jdebut,j-jdebut);
        trimStrValeur;
        posTime := pos(':',strValeur);
        posDate := pos('/',strValeur);
        try
        if (posTime>0) or (posDate>0)
            then begin
                if Agenre=variable
                    then index := decodeVariab[i]
                    else index := decodeConst[i];
                if posTime>0
                   then if posDate>0
                        then begin
                            valeur[i] := StrToDateTime(strValeur);
                            Grandeurs[index].formatU := fDateTime;
                        end
                        else begin
                           valeur[i] := strToTime(strValeur);
                           indexTime := index;
                           Grandeurs[index].formatU := fTime;
                        end
                   else begin
                        valeur[i] := StrToDate(strValeur);
                        indexDate := index;
                        Grandeurs[index].formatU := fDate;
                   end
             end
             else valeur[i] := strToFloatWin(strValeur);
        result := true;
        except
              valeur[i] := Nan;
              traceDefaut := tdPoint;
              valeurNonDef := true;
        end;
        inc(i);
        inc(j);
        if (i=imax) and valeurNonDef and (j<length(s)) then begin
            k := -1;
            repeat inc(k)
            until (k=imax) or isNan(valeur[k]);
            if k<imax then begin
               for kk := k to (imax-2) do valeur[kk] := valeur[succ(kk)];
               dec(i);
            end;
        end;
    until (j>length(s)) or (i=imax);
    imax := pred(i);
    if Agenre=variable
       then begin
           nmes := nmes+1;
           for i := 0 to imax do begin
               index := decodeVariab[i];
               valeurVar[index,pred(Nmes)] := valeur[i];
           end
       end
       else for i := 0 to imax do
            valeurConst[decodeConst[i]] := valeur[i];
end end; // extraitValeurCSV

begin
   case avecTab of
      sTab : result := extraitValeurTab;
      sBlanc :  result := extraitValeurLoc;
      sCSV :  result := extraitValeurCSV;
      else result := false;
   end;
end;

Function TFormDDE.ExtraitCoord(var ligne : integer) : boolean;
var NbreLigne,j : integer;
    s : string;
    zz : word;
    m : integer;
    index : integer;
    UnitePrec : string;
begin
    if ligne>=Donnees.count then begin
       result := false;
       exit;
    end;
    s := Donnees[ligne];
    result := (length(s)>3) and (s[1]=symbReg2);
    if not result then exit;
    NbreLigne := NbreLigneWin(s);
    if pageCourante>1 then begin // pas de mise à jour du graphe
       inc(ligne,succ(NbreLigne));
       exit;
    end;
    result := pos('NOMX',s)>0;
    if result then with FgrapheVariab.graphes[1] do begin
          for j := 1 to NbreLigne do begin
             inc(ligne);
             Coordonnee[j].nomX := Donnees[ligne];
          end;
          for j := succ(NbreLigne) to maxOrdonnee do
             Coordonnee[j].nomX := Coordonnee[1].nomX;
          Monde[mondeX].Graduation := gLin;
          FgrapheVariab.configCharge := true;
          FgrapheVariab.majFichierEnCours := true;
          inc(ligne);
          exit;
    end;
    result := pos('NOMY',s)>0;
    if result then with FgrapheVariab.graphes[1] do begin
         // filDeFer := filDeFer and (NbreLigne>2);
          m := mondeY;
          UnitePrec := '';
          for j := 1 to NbreLigne do begin
             inc(ligne);
             Coordonnee[j].nomY := Donnees[ligne];
             index := IndexNom(Coordonnee[j].nomY);
             if index<>grandeurInconnue then begin
                if (j>1) and
                   (UnitePrec<>grandeurs[index].nomUnite)
                   then inc(m);
                UnitePrec := grandeurs[index].nomUnite;
             end;
             Coordonnee[j].iMondeC := m;
             Monde[m].Graduation := gLin;
          end;
          for j := succ(NbreLigne) to maxOrdonnee do
              Coordonnee[j].nomY := '';
          inc(ligne);
          FgrapheVariab.configCharge := true;
          FgrapheVariab.majFichierEnCours := true;          
          exit;
    end;
    result := pos('LOGY',s)>0;
    if result then begin
       for j := 1 to NbreLigne do begin
          inc(ligne);
          if Donnees[ligne]<>'0'
             then FgrapheVariab.graphes[1].monde[j].graduation := gLog
       end;
       inc(ligne);
       exit;
    end;
    result := pos('AnalLogique',s)>0;
    if result then begin
       for j := 1 to NbreLigne do begin
          inc(ligne);
          if Donnees[ligne]<>'0'
             then include(FgrapheVariab.graphes[1].OptionGraphe,OgAnalyseurLogique);
       end;
       inc(ligne);
       exit;
    end;
    result := pos('DIMPOINT',s)>0;
    if result then begin
       inc(ligne);
       dimPointVGA := StrToInt(Donnees[ligne]);
       inc(ligne);
       exit;
    end;
    result := pos('DIMLIGNE',s)>0;
    if result then begin
       inc(ligne);
      // penWidth := StrToInt(Donnees[ligne]);
       inc(ligne);
       exit;
    end;
    result := pos('GRILLE',s)>0;
    if result then begin
       inc(ligne);
       if Donnees[ligne]='0'
          then exclude(FgrapheVariab.Graphes[1].OptionGraphe,OgQuadrillage)
          else include(FgrapheVariab.Graphes[1].OptionGraphe,OgQuadrillage);
       inc(ligne);
       exit;
    end;
    result := pos('ORTHO',s)>0;
    if result then begin
       inc(ligne);
       if Donnees[ligne]='0'
          then exclude(FgrapheVariab.Graphes[1].OptionGraphe,OgOrthonorme)
          else include(FgrapheVariab.Graphes[1].OptionGraphe,OgOrthonorme);
       inc(ligne);
       exit;
    end;
    result := pos('POLAIRE',s)>0;
    if result then begin
       inc(ligne);
       if Donnees[ligne]='0'
          then exclude(FgrapheVariab.Graphes[1].OptionGraphe,OgPolaire)
          else include(FgrapheVariab.Graphes[1].OptionGraphe,OgPolaire);
       inc(ligne);
       exit;
    end;
    result := pos('COULEUR',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       for j := 1 to NbreLigne do begin
          inc(ligne);
          if j<NbreCouleur then begin
             couleurPages[j] := Tcolor(StrToInt(Donnees[ligne]));
             couleurInit[j] := Tcolor(StrToInt(Donnees[ligne]));
          end;   
       end;
       inc(ligne);
       exit;
    end;
    result := pos('MOTIF',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       for j := 1 to NbreLigne do begin
          inc(ligne);
          motifPages[j] := Tmotif(StrToInt(Donnees[ligne]));
          motifInit[j] := Tmotif(StrToInt(Donnees[ligne]));
       end;
       inc(ligne);
       exit;
    end;
    result := pos('LIGNE',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       for j := 1 to NbreLigne do begin
          inc(ligne);
          zz := StrToInt(Donnees[ligne]);
          if zz=1 then FgrapheVariab.graphes[1].Coordonnee[j].trace := [trPoint];
          if zz=2 then FgrapheVariab.graphes[1].Coordonnee[j].trace := [trLigne];
          if zz=3 then FgrapheVariab.graphes[1].Coordonnee[j].trace := [trLigne,trPoint];
          FgrapheVariab.graphes[1].Coordonnee[j].ligne := LiDroite;
       end;
       inc(ligne);
       exit;
    end;
    result := pos('LOGX',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       inc(ligne);
       if Donnees[ligne]<>'0'
          then FgrapheVariab.graphes[1].monde[mondeX].graduation := gLog;
       inc(ligne,NbreLigne);
       exit;
    end;
    result := pos('Decibel',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       for j := 1 to NbreLigne do begin
          inc(ligne);
          if Donnees[ligne]<>'0' then
             FgrapheVariab.graphes[1].monde[j].graduation := gdB;
       end;
       inc(ligne);
       exit;
    end;
    result := pos('ANALOG',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       inc(ligne);
       if Donnees[ligne]='0'
          then exclude(FgrapheVariab.graphes[1].optionGraphe,ogAnalyseurLogique)
          else include(FgrapheVariab.graphes[1].optionGraphe,ogAnalyseurLogique);
       inc(ligne,NbreLigne);
       exit;
    end;
    result := pos('ZEROX',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       inc(ligne);
       FgrapheVariab.graphes[1].monde[mondeX].zeroInclus := Donnees[ligne]<>'0';
       inc(ligne,NbreLigne);
       exit;
    end;
    result := pos('ZEROY',s)>0;
    if result then begin
       NbreLigne := NbreLigneWin(s);
       m := mondeY;
       for j := 1 to NbreLigne do begin
          inc(ligne);
          FgrapheVariab.graphes[1].monde[m].zeroInclus := Donnees[ligne]<>'0';
          inc(m);
       end;
       inc(ligne);       
       exit;
    end;
    inc(ligne,succ(NbreLigne)); // code non reconnu
end; // extraitCoord

Function TFormDDE.ExtraitNomFichier(var ligne : integer) : boolean;
var j : indiceOrdonnee;
begin
    result := (ligne<Donnees.count) and
              (pos(symbReg2+'1 FICHIER',Donnees[ligne])>0);
    if not result then exit;
    with pages[pageCourante] do begin
       inc(ligne);
       with FgrapheVariab.graphes[1] do begin
          for j := 1 to maxOrdonnee do Coordonnee[j].iMondeC := mondeY;
       end;
       inc(ligne);
   end;
end; // extraitNomFichier

Function TFormDDE.ExtraitOrigineTemps(var ligne : integer) : boolean;
var s : string;
begin with pages[pageCourante] do begin
    if ligne>=Donnees.count then begin
       result := false;
       exit;
    end;
    s := Donnees[ligne];
    result := pos(symbReg2+'1 ORIGINE TEMPS',s)>0;
    if not result then exit;
    inc(ligne);
    inc(ligne);
end end; // extraitOrigineTemps

Function TFormDDE.ExtraitIncert(var ligne : integer) : boolean;

Function ExtraitValeurIncert(const s : string;N : integer) : boolean;
var jfin,jdebut : integer;
    i : integer;
    valeur : double;
begin with pages[pageCourante] do begin
    result := true;
    i := 0;jfin := 1;
    try
    Repeat
        jdebut := jfin;
        while (jdebut<length(s)) and
              not charinset(s[jdebut],chiffreWin) do inc(jdebut);
        jfin := jdebut;
        while (jfin<=length(s)) and
              charinset(s[jfin],chiffreWin) do inc(jfin);
        valeur := strToFloatWin(copy(s,jdebut,jfin-jdebut));
        incertVar[decodeVariab[i],N] := valeur;
        inc(i);
    Until (jfin>=length(s)) or (i>=NbreVariab);
    except
          result := false;
    end;
end end; // extraitValeurIncert

var NbreLigne,j : integer;
    s : string;
    posErreur,LongErreur : integer;
begin with pages[pageCourante] do begin
    if ligne>=Donnees.count then begin
       result := false;
       exit;
    end;
    s := Donnees[ligne];
    result := (pos('INCERT',s)>0) and (s[1]=symbReg2);
    if not result then exit;
    inc(ligne);
    NbreLigne := NbreLigneWin(s);
    result := pos('FONCT',s)>0;
    if result
          then for j := 0 to pred(NbreLigne) do begin
(*              grandeurs[j].IncertitudeF[0].expression := Donnees[ligne];*)
              grandeurs[j].IncertCalc.expression := Donnees[ligne];
              grandeurs[j].compileIncertitude(j,posErreur,LongErreur);
              inc(ligne);
          end
          else for j := 0 to pred(NbreLigne) do begin
             extraitValeurIncert(Donnees[ligne],j);
             inc(ligne);
          end;
     result := true;
end end; // extraitIncert

Function TFormDDE.ExtraitVariableTexte(var ligne : integer) : boolean;
var NbreLigne,j,index : integer;
    s : string;
begin with pages[pageCourante] do begin
    if ligne>=Donnees.count then begin
       result := false;
       exit;
    end;
    s := Donnees[ligne];
    result := (pos('VARIAB_TEXTE',s)>0) and (s[1]=symbReg2);
    if not result then exit;
    NbreLigne := NbreLigneWin(s);
    result := NbreLigne=(nmes+1);
    if not result then exit;
    inc(ligne); // nom de la variable texte
    index := indexNom(Donnees[ligne]);
    result := index>0;
    if not result then begin
       inc(ligne,NbreLigne);
       exit;
    end;
    inc(ligne); // les valeurs
    grandeurs[index].fonct.genreC := g_texte;
    for j := 0 to pred(nmes) do begin
         s := Donnees[ligne];
         texteVar[index,j] := s;
         inc(ligne);
    end;
    result := true;
end end; // extraitVariableTexte

Function TFormDDE.ImportePressePapier : boolean;
begin
     result := false;
     if Clipboard.HasFormat(CF_TEXT) then begin
          Donnees.clear;
          Donnees.text := ClipBoard.asText;
// tout de suite avant que le presse papiers change             
          if not FregressiMain.VerifSauve then exit;
          if GetData then begin
                result := true;
                NomFichierData := '';
                ModeAcquisition := AcqClipBoard;
                FregressiMain.FinOuvertureFichier(true);
                if FregressiMain.WindowState=wsMinimized then
                   FregressiMain.WindowState := wsNormal;
                FregressiMain.sauveEtatCourant;
                ModifFichier := true;
          end;
          donnees.clear;
     end;
end;

Procedure TFormDDE.AjoutePressePapier;
var sauveModeAcq : TmodeAcquisition;
begin
     if pageCourante=0 then begin
        ImportePressePapier;
        exit;
     end;
     Donnees.clear;
     Donnees.text := ClipBoard.asText;
     LecturePage := true;
     case AjouteData of
        mrOK : begin
           if FregressiMain.WindowState=wsMinimized then
              FregressiMain.WindowState := wsNormal;
              if FgrapheVariab.WindowState=wsMinimized then
                 FgrapheVariab.WindowState:=wsMaximized;
              FgrapheVariab.show;
              FgrapheVariab.setFocus;
        end;
        mrYes : begin
           sauveModeAcq := modeAcquisition;
           importePressePapier;
           modeAcquisition := sauveModeAcq;
           FgrapheVariab.setFocus;
        end;
        mrCancel : ;
     end;
     donnees.Clear;
     LecturePage := false;     
end; // AjoutePressePapier

Function TFormDDE.AjouteData : integer;
var i : integer;
    listeNom,listeUnite : TstringList;
    isGTS : boolean;
    NbreVariabLu : integer;

Function ChercheVariab : boolean;
var i,j,k,kc : integer;
    libre : boolean;
 { TODO : Vérifier les doublons de nom }
begin
  NbreVariabAlire := NbreVariabExp+NbreVariabTexte;
  NbreVariabLu := 0;
  for i := 0 to MaxGrandeurs do DecodeVariab[i] := grandeurInconnue;
  for i := 0 to pred(listeNom.count) do begin
        k := indexNom(listeNom[i]);
        if k<>grandeurInconnue
          then begin
             inc(NbreVariabLu);
             DecodeVariab[i] := k;
          end
          else if (pos('sigma_',listeNom[i])<>1)
               then inc(NbreVariabLu);
  end;
  for i := 0 to pred(listeNom.count) do begin
        k := indexNom(listeNom[i]);
        if k=grandeurInconnue then begin  // else decodeVariab déjà affecté
             if pos('sigma_',listeNom[i])=1
                then begin
                    k := indexNom(copy(listeNom[i],7,length(listeNom[i])-6));
                    isPrecVariab[i] := k<>grandeurInconnue;
                    if isPrecVariab[i]
                       then begin
                           decodeVariab[i] := k;
                           inc(NbrePrecVariab); // decodeVariab non nécessaire
                       end;
                       // else à faire
                end
                else begin
                     kc := 0;
                     repeat
                        libre := true;
                        for j := 0 to pred(NbreVariabLu) do
                            if decodeVariab[j]=kc then begin // déjà pris
                               repeat
                                  inc(kc)
                               until ((grandeurs[kc].fonct.genreC=g_experimentale) and
                                      (grandeurs[kc].genreG=variable)) or
                                     (kc=pred(NbreGrandeurs));
                               libre := false;
                               break;
                            end;
                      until libre or (kc=pred(NbreGrandeurs));
                      if libre and
                        (grandeurs[kc].fonct.genreC=g_experimentale) and
                        (grandeurs[kc].genreG=variable) then DecodeVariab[i] := kc;
               end;
        end;
  end;
  if (NbreVariabAlire<NbreVariabLu) then begin
     for i := pred(NbreVariabAlire) downto NbreVariabLu do
         decodeVariab[i] := indexVariabExp[i]; // à améliorer
  end;
  result := true;
end; // ChercheVariab

procedure AjouteConstante;
var NbreConstCB : integer;

Procedure extraitUniteConst(const contenu : string);
var j,debut,long : integer;
    k,c : integer;
begin
    j := 1;
    c := 0;
    Repeat
        debut := j;
        k := decodeConst[c];
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then begin
           Grandeurs[k].NomUnite := copy(contenu,debut,long);
           if pos('°',Grandeurs[k].nomUnite)>0 then AngleEnDegre := true;
           if pos('rad',Grandeurs[k].nomUnite)>0 then AngleEnDegre := false;
        end;
        inc(c);
        inc(j); { saute Tab }
     Until (j>length(contenu)) or (k>=NbreConstCB);
end;  // extraitUniteConst

Procedure affecteNomConst;
var j,jmax : integer;
begin
    extraitNom(Donnees[i],listeNom,listeUnite,true);
    if NbreConstExp>listeNom.count
       then jmax := listeNom.count
       else jmax := NbreConstExp;
    for j := 0 to pred(jmax) do
        decodeConst[j] := IndexNom(listeNom[j]);
    for j := jmax to pred(listeNom.count) do begin
        decodeConst[j] := AjouteExperimentale(listeNom[j],constante);
        grandeurs[decodeConst[j]].nomUnite := listeUnite[j];
    end;
    NbreConstCB := listeNom.count;
end;

Procedure extraitCommConst(const contenu : string);
var j,debut,long : integer;
    k,c : integer;
begin
    j := 1;
    c := 0;
    Repeat
        k := decodeConst[c];
        debut := j;
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then begin
           Fvaleurs.Memo.Lines.Add(''''+grandeurs[k].nom+'='+
                        copy(contenu,debut,long));
           grandeurs[k].fonct.expression := copy(contenu,debut,long);
        end;
        inc(c);
        inc(j);
     Until (j>=length(contenu)) or (k>=NbreConstCB);
end;

Function IsSymbReg2 : boolean;
begin
   result := (length(donnees[i])>0) and (donnees[i][1]=symbReg2)
end;

begin // AjouteConstante
     while (i<Donnees.count) and
           LigneVide(Donnees[i]) do inc(i);
     if (i>=Donnees.count) or isSymbReg2 then exit;
     affecteNomConst;
     inc(i);
     if (i>=Donnees.count) or isSymbReg2 then exit;
     extraitUniteConst(Donnees[i]);
     inc(i);
     if (i>=Donnees.count) or isSymbReg2 then exit;
     if not LigneDeChiffres(Donnees[i]) then begin
        extraitCommConst(Donnees[i]);
        inc(i);
     end;
     if (i>=Donnees.count) or isSymbReg2 then exit;
     ExtraitValeur(constante,Donnees[i],NbreConstExp);
     inc(i);
end; // AjouteConstante

Procedure AjouteValeur;
var s : string;
begin
     while (i<Donnees.count) and
           ExtraitValeur(variable,Donnees[i],NbreVariabAlire)
        do inc(i);
     AjouteConstante;
     while i<Donnees.count do begin
         s := Donnees[i];
         if (length(s)>1) and (s[1]=symbReg2)
               then if not ExtraitIncert(i) and
                       not ExtraitVariableTexte(i) and
                       not ExtraitOrigineTemps(i) and
                       not ExtraitNomFichier(i) and
                       not ExtraitCoord(i)
                            then inc(i)
                            else
               else inc(i);
     end;
     Pages[pageCourante].modifiedP := true;
end; // AjouteValeur

var Contenu,Comm : string;
    LigneNom,Fini : boolean;
    j,k : integer;
    DataSuppl,dataOK : boolean;
label fin;
begin // AjouteData
     screen.cursor := crHourGlass;
     for i := 0 to MaxGrandeurs do begin
         DecodeVariab[i] := indexVariabExp[i];
         isPrecVariab[i] := false;
         NbrePrecVariab := 0;
         DecodeConst[i] := indexConstExp[i];
         isPrecConst[i] := false;
         NbrePrecConst := 0;
     end;
     i := 0;
     isGTS := donnees[0]='Données acquises par ORPHY GTS';     
     result := mrCancel;
     chercheTab;
     listeNom := TstringList.Create;
     listeUnite := TstringList.Create;
     if ChercheEnTeteVellman then begin
        if not ajoutePage then goto fin;
        extraitVellman;
        result := mrOK;
        goto fin;
     end;
     LigneNom := true;
     Fini := false;
     Comm := '';
     if avecTab=sTab then while (i<Donnees.count) and not fini do begin
        Contenu := Donnees[i];
        fini := pos(crTab,Contenu)<>0;
        if not fini then begin
              comm := comm+Contenu;
              inc(i);
        end;
     end; // Tabulation trouvée
     fini := false;
     NbreVariabAlire := NbreVariabExp+NbreVariabTexte;
     while (i<Donnees.count) and not fini do begin
        Contenu := Donnees[i];
        Fini := LigneDeChiffres(Contenu);
        if not Fini and LigneNom and (length(Contenu)>2) then begin
               extraitNom(Contenu,listeNom,listeUnite,false);
            { TODO : vérifier listeNom.count<=nbrevariab }
               dataOK := true;
               if isGTS and (listeNom[0]='') then begin
                  for i := 0 to listeNom.count-2 do
                      listeNom[i] := listeNom[i+1];
                  listeNom[listeNom.Count-1] := 'numero';
               end;
               for j := 0 to pred(listeNom.count) do begin
                   k := indexVariabExp[j];
                   if k<NbreGrandeurs then dataOK := dataOK and (listeNom[j]=grandeurs[k].nom);
                               { TODO : sigma_ }
               end;
               if dataOK then begin
                  result := mrOK;
                  NbreVariabLu := listeNom.count;
               end;
               if result<>mrOK then if chercheVariab
                   then result := mrOK
                   else begin
                      result := mrYes;
                      goto fin;
                   end;
               DataSuppl := NbreVariabLu>NbreVariabAlire;
               if NbreVariabLu<>NbreVariabAlire then begin
                  AddPageExpDlg := TaddPageExpDlg.create(self);
                  if NbreVariabLu>NbreVariabAlire
                     then AddPageExpDlg.grid.colCount := NbreVariabLu
                     else AddPageExpDlg.grid.colCount := NbreVariabAlire;
                  for j := 0 to pred(NbreVariabExp) do
                      AddPageExpDlg.grid.cells[j,0] := grandeurs[indexVariabExp[j]].nom;
                  for j := 0 to pred(NbreVariabTexte) do
                      AddPageExpDlg.grid.cells[j,0] := grandeurs[indexVariabTexte[j]].nom;
                  k := 0;
                  for j := 0 to pred(listeNom.count) do if not isPrecVariab[j] then begin
                      AddPageExpDlg.grid.cells[k,1] := listeNom[j];
                      inc(k);
                  end;
                  result := AddPageExpDlg.showModal;
                  AddPageExpDlg.free;
               end;
               if result<>mrOK then goto fin;
               if DataSuppl then begin
                  for i := 0 to pred(listeNom.count) do
                      if DecodeVariab[i]=grandeurInconnue then begin
                         isPrecVariab[i] := pos('sigma_',listeNom[i])=1;
                         if isPrecVariab[i] then begin
                            k := indexNom(copy(listeNom[i],7,length(listeNom[i])-6));
                            isPrecVariab[i] := k<>grandeurInconnue;
                            DecodeVariab[i] := k;
                            inc(NbrePrecVariab);
                         end;
                         if not isPrecVariab[i]
                            then DecodeVariab[i] := AjouteExperimentale(listeNom[i],variable);
                      end;
                  NbreVariabAlire := NbreVariabExp+NbreVariabTexte;
               end;
               for k := 0 to pred(NbreVariabExp) do
                   if DecodeVariab[k]=grandeurInconnue then DecodeVariab[k] := indexVariabExp[k];
               LigneNom := false;
        end;
        if Fini
           then begin
                NbreVariabLu := 0;
                repeat inc(NbreVariabLu)
                until DecodeVariab[NbreVariabLu]=grandeurInconnue;
                dec(NbreVariabLu);
           end
           else inc(i);
     end;
     if i=Donnees.count then begin // pas d'en tête ?
        i := 0;
        while (i<Donnees.count) and
              not ligneDeChiffres(donnees[i]) do inc(i);
        if i=Donnees.count then goto fin;
     end;
     if not ajoutePage then goto fin;
     Pages[pageCourante].commentaireP := Comm;
     ajouteValeur;
     if Pages[PageCourante].nmes=0
        then supprimePage(pageCourante,false)
        else begin
             pages[pageCourante].recalculP;
             lecturePage := false;
             Application.MainForm.perform(WM_Reg_Maj,MajAjoutPage,0);
             ModifFichier := true;
             result := mrOK;
        end;
     fin :
     screen.cursor := crDefault;
     listeNom.free;
     listeUnite.free;
end; // AjouteData

Procedure TFormDDE.AjouteDataPageCourante;
var i : integer;
label fin;
begin
     for i := 0 to MaxGrandeurs do begin
         DecodeVariab[i] := indexVariabExp[i];
         DecodeConst[i] := indexConstExp[i];
         isPrecVariab[i] := false;
         isPrecConst[i] := false;
         NbrePrecVariab := 0;
         NbrePrecConst := 0;         
     end;
     Donnees.clear;
     Donnees.text := ClipBoard.asText;
     screen.cursor := crHourGlass;
     i := 0;
     while (i<Donnees.count) and
           ((pos(crTab,Donnees[i])=0) or
            not LigneDeChiffres(Donnees[i])) do inc(i);
     if i=Donnees.count then goto fin;
     while (i<Donnees.count) and
           ExtraitValeur(variable,Donnees[i],NbreVariabExp)
       do inc(i);
     Pages[pageCourante].modifiedP := true;
     pages[pageCourante].recalculP;
     Application.MainForm.perform(WM_Reg_Maj,MajValeur,0);
     ModifFichier := true;
     Fvaleurs.MajGridVariab := true;
     Fvaleurs.TraceGridVariab;
     fin : screen.cursor := crDefault;
     donnees.clear;
end; // AjouteDataPageCourante

Function TFormDDE.AjouteFichierTableur(const NomFichier : string) : boolean;
var sauveModeAcq : TmodeAcquisition;
    NomFichierCourt : string;
begin
     Donnees.clear;
     try
     Donnees.LoadFromFile(NomFichier);
     lecturePage := true;
     case AjouteData of
         mrYes : begin
            sauveModeAcq := modeAcquisition;
            nomFichierCourt := extractFileName(NomFichier)+' : ';
            result := importeFichierTableur(NomFichier);
            if result then pages[pageCourante].commentaireP :=
               nomFichierCourt+' : '+pages[pageCourante].commentaireP;
            modeAcquisition := sauveModeAcq;
         end;
         mrOK : result := true;
         mrCancel : result := false;
         else result := false;
     end;
     except
         result := false;
     end;
     lecturePage := false;
     donnees.clear;
end; // AjouteFichierTableur

Function TFormDDE.GetData : boolean;
var isGTS : boolean;
    ligneCourante : integer;
    Comm : string;
    contenu : String;
    listeNom,listeU : TstringList;

Procedure affecteNom(Agenre : TgenreGrandeur);
var i,z : integer;
    isIncert : boolean;
begin
    extraitNom(Contenu,listeNom,listeU,true);
    for i := 0 to pred(listeNom.count) do begin
        isIncert := pos('sigma_',listeNom[i])=1;
        z := grandeurInconnue; // pour le compilateur
        if isIncert then begin // incertitude de qui ?
           z := indexNom(copy(listeNom[i],7,length(listeNom[i])-6));
           isIncert := (z<>grandeurInconnue) and
                       (grandeurs[z].genreG=Agenre);
        end;
        if not isIncert then begin
            z := indexNom(listeNom[i]);
            if z<>grandeurInconnue then
               listeNom[i] := listeNom[i]+intToStr(i);
            z := AjouteExperimentale(listeNom[i],Agenre);
        end;
        if agenre=variable
           then begin
              decodeVariab[i] := z;
              isPrecVariab[i] := isIncert;
              if isIncert then inc(NbrePrecVariab);
           end
           else begin
              decodeConst[i] := z;
              isPrecConst[i] := isIncert;
              if isIncert then inc(NbrePrecConst);
           end;
        grandeurs[z].nomUnite := listeU[i];
    end;
    if (Agenre=variable) and isGTS and (grandeurs[0].nom='') then begin
       for i := 0 to NbreVariab-2 do
           grandeurs[i].nom := grandeurs[i+1].nom;
       grandeurs[NbreVariab-1].nom := 'numero';
    end;
{ isGTS pour régler le pb de GTS en mode XY qui renvoie
"rien" x y
"rien" x1 y1 t1 }
// cas de dataDirect #9#9 pour trois variables ?
end;

Procedure completeNomVar;

Procedure TrimNomVar(var s : String);
var longueur,posCourant : integer;
    sMaj : String;
begin
     longueur := Length(s);
     posCourant := 1;
     while (posCourant<=longueur) do
         if isCaracGrandeur(s[posCourant])
            then inc(posCourant)
            else begin
                 Delete(s,posCourant,1);
                 dec(longueur);
            end;
     sMaj := AnsiUpperCase(s);
     if sMaj='TEMPERATURE' then s := 'T'
     else if sMaj='HAUT' then s := 'max'
     else if sMaj='BAS' then s := 'min'
     else if sMaj='VITESSE' then s := 'V'
     else if sMaj='BAROMETRE' then s := 'P'
     else if sMaj='HUMIDITE' then s := 'Hum'
     else if sMaj='EXTERIEURE' then s := 'ext'
     else if sMaj='INTERIEURE' then s := 'int'
     else if sMaj='REFROIDISSEMENT' then s := 'Refr'
end;

var liste : TstringList;
    i,imax : integer;
    z1,z2 : string;
begin
    liste := TstringList.Create;
    extraitNom(Contenu,liste,listeU,false);
    imax := pred(liste.count);
    if imax>=NbreVariab then imax := pred(NbreVariab);
    for i := 0 to imax do begin
        Z1 := listeNom[i];
        Z2 := liste[i];
        grandeurs[i].fonct.expression := z1+' '+z2;
        TrimNomVar(Z1);
        TrimNomVar(Z2);
        if ((z1='V') or (z2='V')) and
           (grandeurs[i].nomUnite='') then grandeurs[i].nomUnite := 'm/s';
        if ((z1='T') or (z2='T')) and
           (grandeurs[i].nomUnite='') then grandeurs[i].nomUnite := '°C';
        if ((z1='P') or (z2='P')) and
           (grandeurs[i].nomUnite='') then grandeurs[i].nomUnite := 'hPa';
        if ((z1='Hum') or (z2='Hum')) and
           (grandeurs[i].nomUnite='') then grandeurs[i].nomUnite := '%';
        if (length(Z1)+length(Z2))>longNom
           then grandeurs[i].nom := copy(Z1,1,longNom div 2)+
                                    copy(Z2,1,longNom div 2)
           else grandeurs[i].nom := Z1+Z2;
    end;
    liste.free;
    AffecteNomDlg := TAffecteNomDlg.create(self);
    AffecteNomDlg.showModal;
    AffecteNomDlg.free;
end;

Procedure extraitUniteLigne(Agenre : TgenreGrandeur);
var j,debut,long : integer;
    k : integer;
begin
    if pos('dt=0',contenu)=1 then exit; { fichier Synchronie }
    j := 1;
    if Agenre=variable then k := 0 else k := NbreVariab;
    Repeat
        debut := j;
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then begin
           Grandeurs[k].NomUnite := copy(contenu,debut,long);
           if Grandeurs[k].nomUnite='°' then AngleEnDegre := true;
           if Grandeurs[k].nomUnite='rad' then AngleEnDegre := false;
        end;
        inc(k);
        inc(j); // saute Tab
     Until (j>length(contenu)) or (k>=NbreGrandeurs);
     if (Agenre=variable) and isGTS and (grandeurs[NbreVariab-1].nom='numero') then begin
         for j := 0 to NbreVariab-2 do
             grandeurs[j].nomUnite := grandeurs[j+1].nomUnite;
         grandeurs[NbreVariab-1].nomUnite := '';
     end;
end;

Procedure extraitCommLigne(Agenre : TgenreGrandeur);
var j,debut,long : integer;
    k : integer;
begin
    j := 1;
    if Agenre=variable then k := 0 else k := NbreVariab;
    Repeat
        debut := j;
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then begin
           Fvaleurs.Memo.Lines.Add(''''+grandeurs[k].nom+'='+
                        copy(contenu,debut,long));
           grandeurs[k].fonct.expression := copy(contenu,debut,long);
        end;
        inc(k);
        inc(j);
     Until (j>=length(contenu)) or (k>=NbreGrandeurs);
end;

function ChercheEnTeteVariabFichier : boolean;
var FinEnTete : boolean;
    Code : integer;
    FinContenu : string;

Procedure AffecteExe;
begin
end;

var k : integer;
begin // ChercheEnTeteVariabFichier
     FinEnTete := false;
     Code := 0;
     Comm := '';
     isGTS := donnees[0]='Données acquises par ORPHY GTS';
     while (ligneCourante<Donnees.count) and not FinEnTete do begin
        Contenu := Donnees[LigneCourante];
        FinEnTete := LigneDeChiffres(Contenu);
        if FinEnTete then break;
        if Length(contenu)>4
           then FinContenu := AnsiUppercase(copy(contenu,length(contenu)-3,4))
           else FinContenu := '';
        if (pos(crTab,Contenu)=0) and (avecTab=sTab)
           then if FinContenu='.EXE'
                then affecteExe
                else begin
                   MemoAddComm(Contenu);
                   Comm := Contenu;
                 end
           else if (pos(crTab,Contenu)<>0) or
                    (contenu<>'') then if FinContenu='.EXE'
                then affecteExe
                else begin
                  inc(code);
                  case code of
                      1 : if NbreVariab=0
                         then begin
                            affecteNom(variable);
                            for k := 0 to pred(NbreVariab) do
                                if grandeurs[k].nom='' then begin
                                   code := 0;
                                   break
                                end;
                          end
                          else completeNomVar;
                      2 : extraitUniteLigne(variable);
                      3 : extraitCommLigne(variable);
                  end; {case}
            end; {else}
            inc(LigneCourante);
     end;
     result := NbreGrandeurs>0;
     isGTS := isGTS and (grandeurs[NbreVariab-1].nom='numero');
end; // ChercheEnTeteVariabFichier

function ChercheEnTeteVide : boolean;
var i,j,N : integer;
    NombreEnCours : boolean;
begin
    ligneCourante := 0;
    while (ligneCourante<Donnees.count) and
          (length(Donnees[LigneCourante])=0) do inc(LigneCourante);
    contenu := Donnees[LigneCourante];
    if pos(crTab,Contenu)>0 then avecTab := sTab else avecTab := sBlanc;
    Result := false;
    if length(contenu)=0 then exit;
    if not LigneDeChiffres(Contenu) then exit;
    NombreEnCours := charinset(contenu[1],caracNombre);
    if NombreEnCours then N := 1 else N := 0;
    for j := 2 to length(contenu) do
        if charinset(contenu[j],caracNombre) or
           ((pos(contenu[j],caracPrefixe)>0) and
            charinset(contenu[j-1],chiffre))
           then if NombreEnCours
                then
                else begin
                   NombreEnCours := true;
                   inc(N);
                end
           else if charinset(contenu[j],separateur)
                then NombreEnCours := false
                else exit;
    result := N>=1;
    for i := 1 to N do
        AjouteExperimentale('var'+intToStr(i),variable);
    if N>=1 then begin
       AffecteNomDlg := TAffecteNomDlg.create(self);
       AffecteNomDlg.showModal;
       AffecteNomDlg.free;
    end;
end; // ChercheEnTeteVide

function ChercheEnTeteVernier : boolean;
begin
     Comm := '';
     result := pos('Vernier Format 2',Donnees[0])>0;
     if not result then exit;
     MemoAddComm(Donnees[1]); // untitled.txt 9/12/110 8:37:51
     MemoAddComm(Donnees[2]); // Run 1
// 3 : Temps	Tension
// 4 : T	T
// 5 : ms	V
// 6 :
// 7 : première ligne de données
     Contenu := Donnees[3];
     affecteNom(variable);
     Contenu := Donnees[5];
     extraitUniteLigne(variable);
     contenu := Donnees[4];
     extraitCommLigne(variable);
     LigneCourante := 7;
     result := true;
end; // ChercheEnTeteVernier

function ChercheEnTeteConst : boolean;
var oldNbre : integer;
begin
     oldNbre := NbreGrandeurs;
     while (ligneCourante<Donnees.count) and
           LigneVide(Donnees[LigneCourante]) do inc(ligneCourante);
     result := ligneCourante<Donnees.count;
     if not result then exit;
     Contenu := Donnees[LigneCourante];
     result := (length(contenu)>0) and (contenu[1]<>SymbReg2);
     if not result then exit;
     affecteNom(constante);
     result := NbreGrandeurs>OldNbre;
     if not result then exit;
     inc(ligneCourante);
     Contenu := Donnees[LigneCourante];
     extraitUniteLigne(constante);
     inc(LigneCourante);
     Contenu := Donnees[LigneCourante];
     if LigneDeChiffres(Contenu) then exit;
     extraitCommLigne(constante);
     inc(LigneCourante);
end; // ChercheEnTeteConst

label fin;
var i,j,k : integer;
begin // GetData
     FichierTrie := DataTrieGlb;
     optionsDlg.resetConfig;
     for i := 0 to MaxGrandeurs do begin
         DecodeVariab[i] := i;
         DecodeConst[i] := indexConstExp[i];
         isPrecConst[i] := false;
         isPrecVariab[i] := false;
         NbrePrecConst := 0;
         NbrePrecVariab := 0;
     end;
     listeNom := TstringList.Create;
     listeU := TstringList.Create;
     screen.cursor := crHourGlass;
     GetData := false;
     ChercheTab;
     indexTime := grandeurInconnue;
     indexDate := grandeurInconnue;
     indexDateTime := grandeurInconnue;
     indexTexte := grandeurInconnue;
     LigneCourante := 0;
     if ChercheEnTeteVellman then begin
        if not ajoutePage then goto fin;
        AjouteExperimentale('t',variable);
        grandeurs[0].nomUnite := 's';
        AjouteExperimentale('V1',variable);
        grandeurs[1].nomUnite := 'V';
        AjouteExperimentale('V2',variable);
        grandeurs[2].nomUnite := 'V';
        pageCourante := 1;
        extraitVellman;
        getData := true;
        goto fin;
     end;
     if ChercheEnTeteVernier then begin
        if not ajoutePage then goto fin;
        while (ligneCourante<Donnees.count) and
              ExtraitValeur(variable,Donnees[LigneCourante],NbreVariab)
           do inc(LigneCourante);
        getData := true;
        goto fin;
     end;
     if not ChercheEnTeteVariabFichier then begin
        if ChercheEnTeteVide and ajoutePage then begin
           pageCourante := 1;
           while (ligneCourante<Donnees.count) and
               ExtraitValeur(variable,Donnees[LigneCourante],NbreVariab+NbrePrecVariab)
               do inc(LigneCourante);
           getData := true;
        end;
        goto fin;
     end;
     if not AjoutePage then goto fin;
     pages[1].CommentaireP := Comm;
     pageCourante := 1;
     if indexTexte<>grandeurInconnue then ;
     while (ligneCourante<Donnees.count) and
            ExtraitValeur(variable,Donnees[LigneCourante],NbreVariab+NbrePrecVariab)
        do inc(LigneCourante);
     if (indexTime<>grandeurInconnue) and
        (indexDate<>grandeurInconnue) and
        (indexDateTime=grandeurInconnue) then begin
           with Fvaleurs do begin
                memo.Lines.Add('t='+grandeurs[indexDate].nom+'+'+grandeurs[indexTime].nom);
                memo.Lines.Add('''t=date et heure');
           end;
           with FgrapheVariab,graphes[1] do begin
               k := 0;
               for j := 1 to MaxOrdonnee do begin
                   Coordonnee[j].nomX := 't';
                   while (k=indexDate) or (k=indexTime) do inc(k);
                   if k<NbreGrandeurs then
                      Coordonnee[j].nomY := grandeurs[k].nom;
                   inc(k)
               end;
               Monde[mondeX].Graduation := gLin;
               Monde[mondeX].zeroInclus := false;
               configCharge := true;
           end;
     end;
     if (ligneCourante<(Donnees.count-2)) and
        ChercheEnTeteConst and
        ligneDeChiffres(Donnees[LigneCourante]) then begin
             ExtraitValeur(constante,Donnees[LigneCourante],NbreConst+NbrePrecConst);
             inc(ligneCourante);
     end;
     while ligneCourante<Donnees.count do
           if not ExtraitIncert(ligneCourante)
              and not ExtraitVariableTexte(ligneCourante)
              and not ExtraitNomFichier(ligneCourante)
              and not ExtraitOrigineTemps(ligneCourante)
              and not ExtraitCoord(ligneCourante)
                   then inc(ligneCourante);
     GetData := true;
     fin : screen.cursor := crDefault;
     ListeNom.free;
     ListeU.free;
end; // GetData

Procedure TFormDDE.AjouteColPageCourante;
var ligneCourante : integer;
    contenu : string;
    listeNom,listeU : TstringList;
    indexNew : array[0..16] of integer;
    NmesLoc : integer;
    NewCol : boolean;

Function ExtraitValeurCol(const s : string) : boolean;
var j,jDebut : integer;
    i : integer;
    strValeur : string;

Procedure TrimStrValeur;
var longueur,posCourant : integer;
begin
     longueur := Length(strValeur);
     posCourant := 1;
     while (posCourant<=longueur) do
         if charinset(strValeur[posCourant],chiffreWin)
         then inc(posCourant)
         else begin
            Delete(strValeur,posCourant,1);
            dec(longueur);
         end;
end;

begin with pages[pageCourante] do begin
    result := (length(s)>0) and (s[1]<>SymbReg2);
    if not result then exit;
    i := 0;j := 1;
    Repeat
        jDebut := j;
        while (j<=length(s)) and (s[j]<>crTab) do inc(j);
        strValeur := copy(s,jdebut,j-jdebut);
        trimStrValeur;
        try
        valeurVar[indexNew[i],NmesLoc] := strToFloatWin(strValeur);
        except
              valeurVar[indexNew[i],NmesLoc] := Nan;
              result := false;
        end;
        inc(i);
        inc(j);
    Until (j>length(s)) or (indexNew[i]=0);
    inc(NmesLoc);
end end; // extraitValeurCol

Procedure affecteNom;
var i : integer;
begin
    extraitNom(Contenu,listeNom,listeU,false);
    for i := 0 to pred(listeNom.count) do begin
        indexNew[i] := indexNom(listeNom[i]);
        if indexNew[i]=grandeurInconnue then begin
           indexNew[i] := AjouteExperimentale(listeNom[i],variable);
           grandeurs[indexNew[i]].nomUnite := listeU[i];
           NewCol := true;
        end;
    end;
    indexNew[listeNom.count] := 0;
end;

Procedure extraitUniteLigne;
var j,debut,long : integer;
    k : integer;
begin
    j := 1;
    k := 0;
    Repeat
        debut := j;
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then
           Grandeurs[indexNew[k]].NomUnite := copy(contenu,debut,long);
        inc(k);
        inc(j); { saute Tab }
     Until (j>length(contenu)) or (indexNew[k]=0);
end;

Procedure extraitCommLigne;
var j,debut,long : integer;
    k : integer;
begin
    j := 1;
    k := 0;
    Repeat
        debut := j;
        while (j<=length(contenu)) and
              (contenu[j]<>crTab) do inc(j);
        long := j-debut;
        if long>0 then with grandeurs[indexNew[k]] do begin
           fonct.expression := copy(contenu,debut,long);
           Fvaleurs.Memo.Lines.Add(''''+nom+'='+fonct.expression);
        end;
        inc(k);
        inc(j);
     Until (j>=length(contenu)) or (indexNew[k]=0);
end;

function ChercheEnTeteVariabPage : boolean;
var FinEnTete : boolean;
    Code : integer;
begin // ChercheEnTeteVariabPage 
     FinEnTete := false;
     Code := 0;
     NewCol := false;
     while (ligneCourante<Donnees.count) and not FinEnTete do begin
        Contenu := Donnees[LigneCourante];
        FinEnTete := LigneDeChiffres(Contenu);
        if FinEnTete then break;
        if pos(crTab,Contenu)=0
           then begin
                MemoAddComm(Contenu);
           end
           else if (pos(crTab,Contenu)<>0) or
                    (contenu<>'') then begin
                  inc(code);
                  case code of
                      1 : affecteNom;
                      2 : extraitUniteLigne;
                      3 : extraitCommLigne;
                  end; {case}
            end; {else}
            inc(LigneCourante);
     end;
     result := NewCol;
end; // ChercheEnTeteVariabPage

begin // AjouteColPageCourante
     screen.cursor := crHourGlass;
     Donnees.clear;
     Donnees.text := ClipBoard.asText;
     LigneCourante := 0;
     NmesLoc := 0;
     listeNom := TstringList.Create;
     listeU := TstringList.Create;
     if ChercheEnTeteVariabPage then begin
        while (ligneCourante<Donnees.count) and
              ExtraitValeurCol(Donnees[LigneCourante])
            do inc(LigneCourante);
        Fvaleurs.MajGridVariab := true;
     end;
     screen.cursor := crDefault;
     donnees.clear;
     ListeNom.free;
     ListeU.free;
end; // AjouteColPageCourante

Function TformDDE.ImporteFichierTableur(const NomFichier : string) : boolean;

Function IsFichierVspec : boolean;
var fichier,newFile : textFile;
    carac,oldCarac : char;
    compteur,count : integer;
begin
   result := false;
   AssignFile(fichier,NomFichier);
   Reset(fichier);
   compteur := 0;count := 0;
   try
   repeat
         read(fichier,carac);
         case carac of
              #0 : result := true;
              '.' : if compteur = 0 then compteur := 1;
              's' : if compteur = 1 then compteur := 2;
              'p' : if compteur = 2 then compteur := 3;
              'c' : if compteur = 3 then compteur := 4;
              crCR : if compteur = 4 then compteur := 5;
              else begin
                 compteur := 0;
                 inc(count);
              end;
         end;
   until eof(fichier) or (compteur=5) or (count>128);
   result := result and (compteur=5);
   except
       result := false;
   end;
   if result then begin // ne pas tenir compte du début
      AssignFile(newFile,'vspecregressi.txt');
      Rewrite(newFile);
      readln(fichier);// CR + LF
      oldCarac := #0;
      repeat
         read(fichier,carac);
         if (carac<>crCR) or (oldCarac<>crCR) then write(newFile,carac);
         oldCarac := carac;
      until eof(fichier);
      closeFile(newFile);
      Donnees.LoadFromFile('vspecregressi.txt');
   end;
   CloseFile(fichier);
end;

begin
     Donnees.clear;
     try
     if not IsFichierVspec then Donnees.LoadFromFile(NomFichier);
     except
        on E:exception do begin
           codeErreurF := E.message;
           ImporteFichierTableur := false;
           exit;
        end;
     end;
     try
     if GetData
        then result := true
        else begin
           codeErreurF := erFormat;
           result := false;
        end;
     ModeAcquisition := AcqFichier;
     except
         codeErreurF := erFormat;
         result := false;
     end;
     donnees.Clear;
end; // ImporteFichierTableur

Procedure TformDDE.ExporteFichierTableur(const NomFichier : string);
var CSV,TXT,Maple : boolean;
    FichierAsc : textFile;

Procedure sautDeLigne;
begin
    if CSV then write(FichierAsc,crLF) else writeln(fichierAsc)
end;

var
    i,j : integer;
    separeValue : char;
    extension : string;
    text : string;
    label fin;
begin
     extension := AnsiUpperCase(ExtractFileExt(NomFichier));
     CSV := extension='.CSV';
     TXT := extension='.TXT';
     Maple := extension='.DAT';
     SelectColonneDlg := TSelectColonneDlg.create(self);
     selectColonneDlg.CSVCB.visible := CSV;
//     if (SelectColonneDlg.showModal=mrCancel) or
//        (selectColonneDlg.selVariab.count<1) then goto fin;
     if CSV
        then if (FormatSettings.decimalSeparator='.') or selectColonneDlg.CSVCB.checked
            then separeValue := ','
            else separeValue := ';'
        else if Maple
           then separeValue := ' '
           else separeValue := crTab;
     try
     AssignFile(fichierAsc,NomFichier);
     Rewrite(fichierAsc);
     if not CSV and not TXT and not Maple then
        writeln(fichierAsc,pages[pageCourante].TitrePage);
     if not Maple and not selectColonneDlg.valeursSeulesCB.checked then begin
        text := '';
        (*
        for i := 0 to pred(selectColonneDlg.selVariab.count) do begin
            iVar := selectColonneDlg.selVariab[i];
            if text<>'' then text := text+separeValue;
            text := text+grandeurs[iVar].nom;
        end;
        *)
        write(fichierAsc,text);
        sautDeLigne;
        text := '';
        (*
        for i := 0 to pred(selectColonneDlg.selVariab.count) do  begin
             if text<>'' then text := text+separeValue;
             iVar := selectColonneDlg.selVariab[i];
             text := text+grandeurs[iVar].nomUnite;
        end;    -)
        write(fichierAsc,text);
        sautDeLigne;
     end;
     if not CSV and not TXT and not Maple then begin
        text := '';
        (*
        for i := 0 to pred(selectColonneDlg.selVariab.count) do begin
               if text<>'' then text := text+separeValue;
               iVar := selectColonneDlg.selVariab[i];
               text := text+grandeurs[iVar].fonct.expression;
            end;  *)
        write(fichierAsc,text);
        sautDeLigne;
     end;
     with pages[pageCourante] do
          for j := 0 to pred(Nmes) do begin
                text := '';
                (*
                for i := 0 to pred(selectColonneDlg.selVariab.count) do begin
                       if text<>'' then text := text+separeValue;
                       iVar := selectColonneDlg.selVariab[i];
                       if CSV and (separeValue=';')
                         then text := text+FloatToStr(valeurVar[iVar,j])
                         else text := text+FloatToStrPoint(valeurVar[iVar,j]);
                    end; *)
            write(fichierAsc,text);
            sautDeLigne;
     end; // for j
     if (NbreConst>0) and not CSV and not TXT and not Maple then begin
        writeln(fichierAsc);
        for i := 0 to pred(NbreGrandeurs) do
            if grandeurs[i].genreG=constante then
               write(fichierAsc,grandeurs[i].nom,separeValue);
        writeln(fichierAsc);
        for i := 0 to pred(NbreGrandeurs) do
            if grandeurs[i].genreG=constante then
               write(fichierAsc,grandeurs[i].nomUnite,separeValue);
        writeln(fichierAsc);
        for i := 0 to pred(NbreGrandeurs) do
            if grandeurs[i].genreG=constante then
               write(fichierAsc,grandeurs[i].fonct.expression,separeValue);
        writeln(fichierAsc);
        for i := 0 to pred(NbreGrandeurs) do
            if grandeurs[i].genreG=constante then
               write(fichierAsc,FloatToStrPoint(pages[pageCourante].valeurConst[i]),separeValue);
        writeln(fichierAsc);
     end;
     closeFile(FichierAsc);
     except
     end;
     fin:
     selectColonneDlg.free;
end; // ExporteFichierTableur 

procedure TFormDDE.FocusAcquisition;
begin
end;


procedure TFormDDE.AjouteMemo(Amemo : TcustomMemo);
var i : integer;
begin
  Editor.lines.Add('');
  Editor.font.size := 10;
  for i := 0 to pred(Amemo.lines.count) do
      Editor.lines.Add(Amemo.lines[i]);
  Editor.lines.Add('');
end;

procedure TFormDDE.AjouteStringList(AstringList : TstringList);
var i : integer;
begin
    Editor.font.size := 10;
    Editor.lines.Add('');
    for i := 0 to pred(AstringList.count) do
        Editor.lines.Add(AstringList[i]);
end;

procedure TFormDDE.EnvoieRTF;
begin
    Editor.SelStart := 0;
    Editor.SelLength := length(editor.text);
    Editor.copyToClipBoard;
end;

procedure TFormDDE.RazRTF;
begin
    Editor.Clear;
    Editor.font.size := 10;
end;

procedure TFormDDE.AjouteGrid(Agrid : TstringGrid);
var r,c,col1,row1,col2 : integer;
    Texte : String;
begin with aGrid do begin
      Editor.font.size := 8;
  //    Editor.lines.Add('');
      if (cells[0,0]='') or
         ((cells[0,0]='i') and (cells[0,1]=''))
        then col1 := 1
        else col1 := 0;
      if permuteColRow
         then begin
            row1 := col1;
            col1 := 0;
            while col1<rowcount do begin
               col2 := col1+nbreTab; { NbreTab colonnes de 10 = 80 carac }
               if col2>=rowCount then col2 := pred(rowCount);
               for r := row1 to pred(colCount) do begin
                   Texte := '';
                   for c := col1 to col2 do
                       if c<2
                          then Texte := Texte+Agrid.Cells[r,c]+crTab
                          else Texte := Texte+Agrid.cells[r,c]+crTab;
                   delete(Texte,length(texte),1);
     //              Editor.lines.Add(Texte);
               end; {for r}
               col1 := col2+1;
            end;
         end {then}
         else begin
            for r := 0 to 1 do begin // texte
                texte := '';
                for c := col1 to pred(colCount) do
                    Texte := Texte+Agrid.Cells[c,r]+crTab;
//Editor.lines.Add(Texte);
            end;
            for r := 2 to pred(rowCount) do begin // nombre
                texte := '';
                for c := col1 to pred(colCount) do
                    Texte := Texte+Agrid.cells[c,r]+crTab;
          //      Editor.lines.Add(Texte);
            end;
        end;
   //   Editor.lines.Add('');
end end;

procedure TFormDDE.MemoAddComm(Astr : string);
begin
       if Astr<>'' then Fvaleurs.Memo.Lines.Add(''''+Astr)
end;

procedure TFormDDE.ChercheTab;

Function LigneDeChiffres(const s : String) : boolean;
var j : integer;
    AvecChiffres : boolean;
begin
    Result := false;
    AvecChiffres := false;
    for j := 1 to length(s) do
        if charinset(s[j],caracNombre)
           then avecChiffres := true
           else if not charinset(s[j],separateur) then exit;
    result := avecChiffres;
    if result then begin
       if pos(';',s)>0 then begin
          separateurCSV := ';';
          if pos(',',s)>0 then avecTab := sCSV; // 1,23;1,58
       end;
       if pos('.',s)>0 then begin
          separateurCSV := ',';
          if pos(',',s)>0 then avecTab := sCSV; // 1.23,1.59
       end;
    end;
end;

var i : integer;
begin
     AvecTab := sBlanc;
     if (donnees.count<3) then exit;
     i := 0;
     while (AvecTab=sBlanc) and
           (i<Donnees.count) and
           (i<5) do begin
              if pos(crTab,Donnees[i])<>0 then avecTab := sTab;
              inc(i);
     end;
     if (AvecTab=sBlanc) then begin
          i := 0;
          repeat inc(i)
          until (i=Donnees.count) or
                (i=4) or
                ligneDeChiffres(donnees[i]);
     end;
end;

function TformDDE.ChercheEnTeteVellman : boolean;
var posTime,posVoltage : integer;

Function GetCoeff(ligne : integer) : double;
var N : integer;
    division : double;
    i,j : integer;
    Nstr : String;
begin
    result := 1;
    Nstr := Donnees[ligne];
    trimComplet(Nstr);
    i := pos(':',Nstr);
    if i>0 then delete(Nstr,1,i);
    i := pos('=',Nstr);
    if (i=0) or (i=length(Nstr)) then exit;
    N := strToInt(copy(Nstr,1,i-1));
    if ligne=posVoltage then ZeroVellman := N*4;
    j := i+1;
    while (j<=length(Nstr)) and
          charinset(Nstr[j],chiffreEtc) do inc(j);
    division := StrToFloatWin(copy(Nstr,i+1,j-i-1));
    if Nstr[j]='m' then division := division/1000;
    if Nstr[j]='k' then division := division*1000;
    if Nstr[j]='µ' then division := division/1e6;
    result := division/N;
end;

var i : integer;
begin
     posTime := 0;
     posVoltage := 0;
     i := 0;
     zeroVellman := 0;
     repeat
         if pos('TIME STEP:',Donnees[i])>0 then posTime := i+1;
         if pos('VOLTAGE STEP:',Donnees[i])>0 then posVoltage := i+1;
         inc(i);
         result := (posTime>0) and (posVoltage>0);
     until result or (i>=donnees.count) or (i=8);
     if result then begin
        CoeffTempsVellman := GetCoeff(posTime); { 160 = 10ms }
        CoeffCH1Vellman := GetCoeff(posVoltage); { CH1:  32 = 5V }
        CoeffCH2Vellman := GetCoeff(posVoltage+1); { CH2:  32 = 5V }
     end;
end; // ChercheEnTeteVellman

Procedure TformDDE.extraitVellman;
var ligneCourante,i : integer;
begin
     LigneCourante := 0;
     for i := 0 to MaxGrandeurs do begin
         DecodeVariab[i] := indexVariabExp[i];
         DecodeConst[i] := indexConstExp[i];
         isPrecConst[i] := false;
         isPrecVariab[i] := false;
         NbrePrecConst := 0;
         NbrePrecVariab := 0;
     end;
     repeat inc(LigneCourante)
     until (LigneCourante=Donnees.count) or
            LigneDeChiffres(Donnees[ligneCourante]);
     modifFichier := true;
     while (ligneCourante<Donnees.count) and
            ExtraitValeur(variable,Donnees[LigneCourante],NbreVariab)
         do inc(LigneCourante);
     with pages[pageCourante] do begin
        commentaireP := Donnees[0];
        for i := 0 to pred(nmes) do begin
           valeurVar[0,i] := valeurVar[0,i]*coeffTempsVellman;
           valeurVar[1,i] := (valeurVar[1,i]-zeroVellman)*coeffCH1Vellman;
           valeurVar[2,i] := (valeurVar[2,i]-zeroVellman)*coeffCH2Vellman;
        end;
     end;
end;

procedure TFormDDE.FormCreate(Sender: TObject);
begin
     donnees := TstringList.create;
end;

procedure TFormDDE.FormDestroy(Sender: TObject);
begin
     donnees.free;
     inherited
end;

end.



