{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit constreg;

  {$MODE Delphi}

interface

uses
  LCLIntf, LCLType,
  SysUtils;

Function stActive(isActive : boolean) : string;
Function stOuiNon(isOui : boolean) : string;

resourcestring

stPageActive = '|Page active';
stPageInactive = '|Page inactive';
stOui = 'Oui';
stNon = 'Non';
stNewPage = 'Nouvelle page';
stIntervalle95 = 'Intervalle de confiance à 95%';
stIncertitudeType = 'Incertitude-type';
erProgAcqDialog = 'Impossible de dialoguer avec ';
okPageVersCol = 'Transformer les pages en colonnes';
okNonNum = 'Utilisation d''une grandeur non numérique';
erNotExists = ' n''existe plus';
erNonNum = 'Variable non numérique';
erEffectifNotInt = 'Effectif non entier !';
erPourCent = 'Somme des fréquences différente de 100 !';
erFrameRate = 'Nombre de trames par seconde incorrecte';

erEchelle  = 'Echelle incorrecte';
erPbImprimante='Impossible de se connecter (HP LaserJet 1600 ?)';
erPageModele='Syntaxe : y[numero de page]=f(x)';
erNomVide='Nom vide';
erNomGrandeurVide='Pas de nom de variable à affecter';
erDebutNom='Un nom doit commencer par une lettre';
erNomInterdit='Nom interdit (j^2=-1 pi=3.1416)';
erNomFonction='Nom interdit (nom de fonction : Im Re Arg Min Max...)';
erNomExistant='Nom interdit (déjà existant)';
erMuette='Nom interdit pour une variable muette (déjà existant)';
erVarTexte='Calcul impossible avec une grandeur texte';
erUnite='Unité incorrecte';
erAire='''->La fonction aire(x,y) détermine l''aire d''une surface fermée';
erNbreVarInf2='Pas assez de variables';
erFileNotExist='Fichier non trouvé';
erCaracNom='Caractère interdit dans un nom';
erParamModeleVersExp='Utilisation d''un paramètre de modélisation (donc susceptible de disparaître)';
erNbreGradManuel='Nombre de graduations <2 ou >64 => retour au mode automatique';
erOrtho='Vérifier l''option "orthonormé" (boite de dialogue coordonnées)';
erInv='Vérifier la graduation inverse (boite de dialogue coordonnées)';
erExpressionVide='Expression vide';
erTexteNotTest='Texte en dehors d''un IF';
erFonctionNil='Fonction non initialisée';
erNombrePetit='Modélisation impossible : nombre de valeurs < nombre de paramètres';
erVide='Expression vide';
erNombre='Nombre mal écrit';
erTropLong='Identificateur trop long';
erInterne='Erreur interne';
erTropComplexe='Expression trop compliquée';
erParF='Manque )';
erParO='Manque (';
erParamSolve='Manque un paramètre : solve(equation,min,max)';
erParamMoyenne='Manque un paramètre : mean(variable,debut,fin)';
erVarInterdite='Variable interdite';
erParamInterdit='Paramètre interdit';
erTropParam='Trop de paramètres';
erVirgule='Manque ,';
erVarAttendue='Variable attendue';
erFonctNonCpx='Fonction interdite dans les complexes';
erOperNonCpx='Operateur interdit dans les complexes';
erReel='Le résultat doit être réel (Re;Im;Arg;Abs)';
erSyntaxe='Erreur de syntaxe';
erSyntaxeModele='Syntaxe du modèle : Y(X)=f(X)';
erSyntaxeIncert='Syntaxe de l''incertitude type : u(X)';
erVarInconnue='Variable non reconnue';
erDomaine='Fonction hors de son domaine de définition';
erDivergence='Divergence de la modélisation';
erTimeOut='Arrêt de la modélisation au bout de 32 itérations';
erPageCalc='Pas de zone commune entre les différentes pages';
erNombreVar='Pas assez de variables';
erDerNonDef='Dérivée non définie';
erSyst1Var='Système d''équa diff nécessite la même variable explicative';
erNonInstallee ='Fonction non installée';
erVarDef='Grandeur déjà définie';
erMaxModele='Nombre maximum de modèles : 4';
erEgalAbsent='Syntaxe des fonctions : nom=''expression';
erCaracInterdit='Caractère interdit';
erGenreModele='Les modèles doivent être de même type : fonction OU équa. diff. d''ordre 1 OU 2';
erVarOrNombre='Doit être un paramètre ou une valeur numérique';
erModeleGlb='Modélisation par fonction uniquement dans l''espace des paramètres';
erCrochet='[] interdit y[min] de Regressi DOS remplacé par min(y)';
erCurseurModele='Modélisation non accessible';
erNmesInf16='Nombre de points trop faibles <16';
erFormat='Format fichier incorrect';
erConst3D ='Crochets non permis';
erFonctionPageNonPermis ='Fonction page[n] non permise';
erParamInconnu ='Paramètre inconnu';
erGrandTableau ='Impression de grand tableau interdite';
erXOR='Opérateur interdit en dehors de IF';
erFileAdd='Erreur à la lecture du fichier';
erFileLoad='Impossible d''ajouter le fichier';
erModeleGr='Données non cohérentes avec le modèle';
erInitAdefinir='Donnez une valeur initiale dans l''onglet paramètres';
erConstNonDef='Valeur de paramètre expérimental absente';
erParamAdefinir='Donnez une valeur aux paramètres';
erSuperGlb='Superposition de fonction en construction';
erCurseurTangente='Impossible : abscisse<>première variable';
erGradNonLineaire='Impossible : graduations non linéaires';
erPrimeRRR='Caractère '' interdit dans les variables (réservé aux équa. différentielles)';
erDiffSimul='Le nombre maxi d''équations dans un système est de ';
erEcart='Ecart modèle-expérience très grand vérifier vos paramètres';
erFonctionGlbIsole ='Les fonctions Intg Diff Harm Filtre ... doivent être utilisées isolément';
erVarFreq='Nom réservé pour les séries de Fourier';
erPlotter='Installer un traceur dans le panneau de configuration d''imprimantes';
erColPrinter='Nombre de colonnes trop grand : penser au mode paysage ou au couper-coller';
erBadDataNom='Données incompatibles : noms différents';
erSuperposePage='Positionnement de l''origine une page à la fois';
erBadDataInf='Données incompatibles : trop de grandeurs';
erBadDataSup='Données incompatibles : trop de grandeurs';
erSyntaxeDDE='Syntaxe Macro DDE : Fonction(Paramètre)';
erDDEone='Un message DDE à la fois SVP';
erPuissEnt='Puissance non entière';
erPointModele='Un point non pris en compte : n°';
erDiff0='La variable explicative des équa. diff. est la première colonne';
erRemplitLog='Remplissage exponentiel avec valeur <= 0';
erModeleSystDiff='Système diff. uniquement';
erFileData='Données du fichier incorrectes';
erProgAcq='Programme d''acquisition inconnu';
erDDE='Liaison DDE impossible avec ';
erIndexNotVariab ='Interdit en dehors des variables';
erCrochetF ='] attendu';
erPosMin ='Posmin; posmax remplacés par pos(variable;équation;[up|down|mini|maxi])';
erTangenteAgauche ='L''axe doit être à gauche';
erOrigineExp ='Modification uniquement pour grandeur expérimentale';
erModeleIncorrect ='Désactiver le mode superposition de pages';
erWriteFile ='Erreur d''écriture du fichier';
erReadFile ='Erreur de lecture du fichier';
erReadVotable ='N''est pas un fichier Votable';
erReadRegressiXML ='N''est pas un fichier Regressi XML';
erDOMXML = 'Parser XML non trouvé';
erPostScript ='Ne peut créer *.EPS';
erFonctionPage ='page(nombre)';
erOriginePage ='Nécessite une seule page active';
erExposantCpx ='Exposant uniquement entier à l''intérieur des complexes';

OkDelFile='Ecraser le fichier existant';
OkImprGr='Impression du graphe';
OkImprTab='Impression des tableaux';
OkDelData='Suppression des données sélectionnées';
OkDelAll='Effacement de la page courante';
OkUniteInconnue='Unité non reconnue vous confirmez ?';
OkNoParamAuto='Pas d''expression. Désactiver génération auto ?';
OkVarTri='Garder %s comme variable triée';
OkSauve='Sauvegarde de l''état courant ?';
OkMobile2='Deuxième mobile ?';
OkDelPage='Suppression de la page n°%s';
OkModifGr='Modification du graphe selon modèle ?';
OkImprModele='Impression de la modélisation';
OkInitParam='Initialisation des paramètres ?';
OkDelVariab='Ecraser l''ancienne variable %s';
OkBadDataNom='Données incompatibles (noms différents) on continue ?';
OkBadDataNewFile='Données incompatibles avec les données courantes => Créer un nouveau fichier ?';
OkImprTabVariab='Impression du tableau des variables';
OkModifOrigine='Effectuer le changement d''origine ?';
OkImprCurseur ='Impression des valeurs modélisées';
OkAngleDegre ='Angle en degré';
OkAngleRadian ='Angle en radian';
OkPaysage ='Impression avec orientation paysage';
okSupprIdentCoord ='Suppression des identifications de courbes';

trFiltreModele ='Modélisation (*.mod)|*.mod';
trFiltreWMF ='Metafichier Windows (*.emf)|*.emf';
trFiltreExe ='Exécutable (*.exe)|*.exe|Batch (*.bat)|*.bat';
erPrefixe ='Essayer de travailler en S.I. sans préfixe m k ... (sauf kg !)';
erUneLigneSurN ='Impression d''une ligne sur %s';
erAxeLog ='Axe log. ou dB impossible : valeurs <= 0 ?';
erDiff2 ='Equation différentielle d''ordre 1 ou 2';
hSupprPointActif ='|Suppr : suppression du point cliqué ; barre : désactivation du point';
hSupprPointNonActif ='|Suppr : suppression du point cliqué ; barre : activation du point';
hMove ='|Cliquer/déplacer pour modification de la position du zoom';
hValeurModele ='|Entrez une valeur et validez pour calculer';
hGlisser ='|Glisser pour déplacer';
hDbleTexte ='Clic droit pour modifier le texte ou supprimer';
hDbleCouleur ='Clic droit pour choix de couleur/style de la ligne ou suppression';
hOrigine ='|Positionner la verticale à la nouvelle origine puis cliquer';
hSegment ='|Cliquer à l''intersection du réticule puis faire glisser ; F10:enregistre la position courante ; ESC=fin';
hReticuleModele ='|Cliquer à l''intersection du réticule puis faire glisser ; F10:enregistre la position courante ; ESC=fin';
hCurseurFrequence ='|F10:enregistre la position courante';
hClicDroit ='|Utiliser le clic droit pour ouvrir le menu local';
hHistoResidu ='Histogramme des résidus';
hExpFFT ='|Tapez une expression fonction de %s uniquement';
hindice ='%s va de 0 à %s-1';
hEuler =' signifie valeur de x au point i';
hMoyenne = 'Abscisse=moyenne ; ordonnée=maximum';
hEcartType = 'Abscisse=moyenne+(ecart type)';
hDemiLargeur = 'Abscisse=moyenne+(demi largeur)';
hDebutModele ='Modéliser|Ouvrir le volet de modélisation';
hFinModele ='Fin modélisation|Fermer le volet de modélisation';
trSupprVar ='Suppression de %s';
hRowUnit ='|Double-clic dans l''en-tête pour modifier unité ; incertitude';
hMovePoint ='|Déplacer le point de référence';
hPeriode ='|Abscisse=période ordonnée=décalage de zéro';
hMaxi ='|Maximum du sinus : position et valeur';
hCteTemps ='|Abscisse=constante de temps';
hAsymptote ='; ordonnée=asymptote';
hDirection = '|Direction';
hEchelle = '|Echelle';
hOrigineXY = '|Origine des coordonnées';
hValeur0 ='; ordonnée=valeur initiale';
hClicPage ='(double clic sur le point pour changer l''état)';
hFreqCoupure ='abscisse : fréquence caractéristique';
hGainMax ='ordonnée : gain maximal';
erMotdepasse='Suppression du mot de passe';
hValeurAsymptote =' 	Intersection des asymptotes';
erVttAdd ='Impossible de fusionner des fichiers LAB';
erBoucleFor ='Syntaxe de boucle for i:=exp to exp do';
erFinBoucle ='end permis en fin de boucle';
okSupprAutresPages ='Suppresssion des points de toutes les pages actives';
erDataCan ='Données non modifiables (voir Options Acquisition)';
erChargeOldReg ='Rechargement du dernier fichier';
okrazModele ='Remise à 1 des paramètres';
okrazParam ='Suppression de la modélisation';
erForward ='Définir la grandeur prédéfinie par x= ou x[0]=a';
okSauveOptions ='Sauvegarde des options pour tous les utilisateurs';
OkDataInf ='Pas assez de variables ; on continue';
OkDataSup ='Trop de variables : on continue';
ErGommeStat ='Sélectionnez les points sur l''axe des abscisses';
hFinGomme ='Arrêt de suppression de points';
erLigneNulle ='Définition de ligne par glisser';
erObjetGraphiqueIsole ='';
erNomObjetGraphique ='';
erFileCalc ='Nom de variables non compatibles';
erNbreData ='Pas assez de données';
erIndiceEuler = 'Syntaxe : x[i]= et non x[i+1]=';
erPeigneSimul = 'Peigne disponible uniquement en mode simulation';
erParamIF = 'UN argument manquant à IF(condition,si vraie,sifausse)';
erParamPW = 'Syntaxe PieceWise([test,then],else)';
erRadian = 'Passage en radian';
erVerifRadian = 'Actuellement en degré. Passage en radian ?';
erOneCol = 'Une seule colonne à la fois';
erValeurNoDef = 'Valeur non définie dans la page n° ';
erServeur = 'Impossible de dialoguer avec le serveur';

stResiduStudent = 'Résidu studentisé ';
stLimiteStudent95 = ' (limite de Student à 95% : ';
stMoyenne = 'Moyenne ';
stAnimation = 'Animation';
stFinAnimation = 'Fin animation';
stMediane = 'Médiane ';
stEcartType = 'Ecart-type ';
stMini = 'Mini';
stMaxi = 'Maxi';
stValeur = 'Valeur ';
stMinimum = 'Minimum';
stMaximum = 'Maximum';
stIntensite = 'Intensité relative';
stCible = 'Cible ';
stTaille = 'Taille ';
stOuvrir = 'Ouvrir';
hInitialise = '|Initialise';
stUnGraphe = 'Un graphe';
stDeuxGraphes = 'Deux graphes';
stEtendue = 'Etendue : ';
stNom = 'Nom';
stSymbole = 'Symbole';
stUnite = 'Unité';
stSignif = 'Signification';
hBornes = 'Bornes PUIS expression|Définir un nouveau modèle par un rectangle (bornes) PUIS taper la fonction';
trBornesModele = 'Bornes et nouveau modèle';
stPente = 'Pente';
stPvaleur='P-valeur(';
stIntersection = 'Intersection' ;
stAngle = 'Angle';
stRayon = 'Rayon';
stAbscisse = 'Abscisse';
stOrdonnee = 'Ordonnée';
stInexactitude = 'Inexactitude';
stEquivalence = 'Equivalence';
stTangente = 'Tangente';
stBorne = 'Bornes de ';
stBornes = 'Bornes';
stCourbe = 'Courbe ';
stReticule = 'Réticule';
stStatistique = 'Statistique : ';
stTableau = 'Tableau ';
stNbrePas = 'Nombre de pas : ';
stPage = 'Page';
stIncertitude = 'Incertitude';
stIncert = 'Incert. sur ';
stPages = 'Pages';
stOption = 'Option';
stPhase = 'Phase';
stPhaseContinue = 'Phase continue';
stContinu = 'Continu ';
stCouleur = 'Couleur';
stTest1 = 'Résultat d''un réglage manuel';
stTest2 = 'des paramètres. Pour optimiser,';
stTest4 = 'Optimisation des paramètres';
stTest5 = 'à effectuer';
stTest3 = 'cliquer sur ajuster';
stTest6 = 'cliquer sur le bouton "coche rouge"';
stVariables = 'Variables';
stGrandeurs = 'Grandeurs';
stModelisation = 'Modélisation';
stAxes = 'Axes';
stIteration = 'Itération n°';
stIntervalle = 'ème intervalle';
stDerivee = 'Dérivée';
stAbsolue='absolue';
stRelative='relative';
stDbDecade = 'dB/décade';
stNbreDec ='Nombre de décimales';
stNbreChiffres ='Nombre de chiffres';
stNbreBits ='Nombre de bits';
stFreqreduite ='Fréquence réduite';
stDebutDe = ' le début de ';
stFinDe = ' la fin de ';
stEcartQuad = 'Ecart quad. ';
stCoeffCorrel = 'Coeff. corrélation';
stGrad = 'Grad.';
stSubdiv = 'Nbre div.';
stClicVecteur = 'Cliquer sur un point pour tracer (ou effacer) un vecteur';
stClicIndicateur = 'Clic droit pour changer d''indicateur';
stCaracStat = 'Caractéristique statistique de ';
stNomVariab = 'Nom des variables :';
stFinModele = 'Fin modélisation';
stValeurReticule = 'Valeurs du réticule';
stValeurModele = 'Valeurs modélisées';
stNomModele = 'Nom de la grandeur modélisée';
stCommentaire = 'Commentaire';
stCalcVitesse = 'Calculer la vitesse !';
stCalcAcceleration = 'Calculer l''accélération !';
stCopiePage = 'Recopie de la page';
stGridValeurs = '&Tableau valeurs';
stRazValeurs = 'Rà&Z des valeurs';
stGridTangente = '&Tableau tangentes';
stRazTangente = 'Rà&Z des tangentes';
stResultModele = 'Résultat de la modélisation';
stEcartRelatif = 'Ecart données-modèle';
stTriVariab = 'Tri des données selon une variable';
stTriPages = 'Tri des pages selon un paramètre';
stSupprGrandeur = 'Suppression d''une grandeur';
stCadreGTS = 'Recadrage de données acquises par Orphy GTS';
stBorneSelect = 'Bornes à la souris|Définir les bornes en traçant un rectangle';
stBarreReticuleFFT = '|F10: enregistre position courante ; Barre d''espace: marque le pic proche';
stBarreReticule1 = '|F10:enregistre position courante; Barre d''espace:laisse une trace (cf. clic droit tableau) ; clic:mesure d''écart ; ESC:fin';
stBarreReticule3 = '|F10:enregistre position courante; clic:suppression deuxième curseur ; ESC:fin';
stBarreReticule2 = '|F10:enregistre position courante; clic:fixe l''écart ; ESC:fin';
stExper = ' expérimentale';
stVariable = 'Variable';
stConstante = 'Paramètre';
stParam = 'Paramètre de modélisation';
stTexte = ' texte';
stSigmaResidu = '(sigma des résidus)';
stEcart = 'écart = ';
stComment = 'Commentaires';
stAcqBy = 'Acquisition par ';
stAjouteMod = '&Ajouter modèle';
stRemplaceMod = '&Remplacer modèle';
stCibleAuto = 'Cliquer sur la cible à suivre';
stNEuler = 'Nombre de points : N=';
stModif = 'modifications de ';
stData = 'données';
stSauve  = 'Sauvegarde des ';
stFinZoom =
'|Glisser puis relâcher pour définir le deuxième point du rectangle délimitant la zone concernée';
stDebutZoom =
'|Cliquer pour définir le premier point du rectangle délimitant la zone concernée';
stChoixPagesGr = 'Choix des pages à regrouper en une seule';
stGroupeSuper = 'Attention : regroupe ne superpose pas';
stChoixPages = 'Choix des pages actives';
stUndo = '&Annuler suppression de ';
stNoAcces = 'Impossible d''accéder à ';
stMajImpossible = 'Impossible d''accéder au site de mise à jour';
stUndoGr = '&Annuler grouper';
stEchantillon = 'Echantillonnage';
stCaracde = 'Caractéristiques de ';
stSigmay = 'Ecart-type sur ';
stImportCalc = 'Importer les traitements';
stUser = 'Utilisateur';
stName = 'Prénom nom';
stPrint = 'Impression';
stEnTete = 'En-tête';


hGommeStat = '|Sélectionner à la souris (cliquer  déplacer) les points à supprimer  (Ctrl enfoncée pour recommencer)';
hTriDataDlg = 'tri des données';
hAideValide = '(validez avec Entrée)';
hTriVariab = 'selon la variable sélectionnée';
hSupprCalc = 'les variables calculées sont supprimées';
hSupprLigne = 'en supprimant la ligne du mémo expression';
hTriPageDlg = 'tri des pages';
hTriParam = 'selon le paramètre sélectionné';
hTriPhase = 'rend continue la variable sélectionnée';
hMod2Pi = 'en effectuant un modulo 360°';
hTopSigmoide = 'Ordonnée=plateau sup.';
hBottomSigmoide = 'Ordonnée=plateau inf.';
hMidSigmoide = 'Point d''inflexion de la sigmoïde';
hPenteSigmoide = ' Ecart entre les deux points = pente';
hZoomManuelDebloq = 'Echelle bloquée|Modification de l''échelle ou déblocage de celle-ci';
hZoomManuelBloq = 'Echelle manuelle|Définition manuelle de l''échelle et blocage de celle-ci';
hZoomAutoBloq = 'Echelle bloquée => retour à l''échelle automatique';
hCopyExpressions = 'Copier expressions|Copier l''ensemble du texte dans le presse-papiers';
hCopyGrid = 'Copier tableau|Copier le tableau dans le presse-papiers';
hTriData = 'Trier données|Trier les données selon la grandeur courante';
hDelta = 'Précision|Affiche, si enfoncé, l''incertitude sur les ';
hTriPage = 'Trier page|Trier les pages selon un paramètre';
hAltTab = 'Utiliser Alt-Tab';
hGrid = '|Clic droit : menu local ; F2 : édition d''une cellule ; Double-clic dans l''en-tête : modification unité, incertitude';
hCurseurOrigine ='Placer le curseur sur la nouvelle origine';
hGainContinu ='ordonnée : gain en continu';
hGainHF ='ordonnée : gain à haute fréquence';
hGainQ ='ordonnée : gain pour la fréquence de coupure';
hSelectPageMod = 'Choix des pages prises en compte dans la modélisation';
hClicInsere = 'cliquer pour insérer';
hEchelleFreq = 'Cliquer déplacer les graduations pour changer l''échelle';
hBasculeGTS = 'Pour basculer vers GTS utiliser Alt-Tab ou la barre des taches';
hLigneRappel='Ligne de rappel|Style de trait des lignes de rappel';
hTraitTangente='Tangente|Style de trait des tangentes';

  TitreBmp = 'Lecture de bitmap';
   hDeplacerPoint = '|Déplacer le point';
   hRepererPoint = '|Cliquer pour enregistrer le point';
   hOrigineBmp = 'Origine|Cliquer sur l''origine';
   trCibleAuto = 'Cliquer sur la cible à suivre';
   hDebutX  = 'Début X|Cliquer sur le début de l''échelle de ';
   hDebutY  = 'Début Y|Cliquer sur le début de l''échelle de ';
   hFinX  = 'Fin X|Cliquer sur la fin de l''échelle de ';
   hFinY  = 'Fin Y|Cliquer sur la fin de l''échelle de ';
   NoZoom = 'Taille réelle';
   hTransfertRegressi = 'Transférer vers Regressi';
   hReglerZoom = '|Réglage du zoom';
   CaptionBmp = 'Lecture de bitmap pour Regressi';
   hRepereBmp = 'Cliquer sur les points à repérer';
   hSerie2Bmp = 'Deuxième série de points';

   erDuree = 'Durée incorrecte';
   erTronque = ' (trop long donc tronqué)';
   hEchelle1 = 'Début|Cliquer sur le début de l''échelle';
   hEchelle2 = 'Fin|Cliquer sur la fin de l''échelle';
   hEchelle1Move = 'Déplacer le début de l''échelle';
   hEchelle2Move = 'Déplacer la fin de l''échelle';

   hGlisserEchelle = '|Glisser jusqu''à la fin de l''échelle';
   hStartRecord = 'Enregistre|Début de l''enregistrement';
   hStopRecord = 'Stop|Fin de l''enregistrement';
   hClicDroitAvi = 'Clic droit pour choix du curseur';
   hEchelleJpeg = '|Echelle ; double-clic pour agrandir';
   hParamBtn = 'Cliquer sur le bouton Paramètres pour configurer';
   hOrigineI = '|Cliquer sur l''origine du point n°';
   hOrigineMobile = '|Il faudra indiquer l''origine avant chaque point';
   hOrigineMouse = '|Indique l''origine à l''aide de la souris';
   hStopMes = '|Arrêt des mesures';
   hStartMes = '|Commencer les mesures';
   hOrigine1 = 'Origine du point n°1';
   hOrigineT = 'cliquer sur le point origine des temps';
   hClicPoint = 'cliquer sur le point à enregistrer';
   trEchelle = 'Echelle';
   trLmetre = 'Longueur en mètre ?';


implementation

Function stActive(isActive : boolean) : string;
begin
    if isActive then result := stPageActive else result := stPageInactive
end;

Function stOuiNon(isOui : boolean) : string;
begin
    if isOui then result := stOui else result := stNon
end;

end.


