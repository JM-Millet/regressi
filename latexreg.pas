{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit latexreg;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, Dialogs, sysutils, SysInfoCtrls,
  constreg, regutil, compile, graphker, graphvar, valeurs;

type
  TLatexDlg = class(TForm)
    PanelOption: TPanel;
    HelpBtn: TBitBtn;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DateCB: TCheckBox;
    GrandeursCB: TCheckBox;
    VariabGB: TGroupBox;
    TableauVariabCB: TCheckBox;
    ModeleVariabCB: TCheckBox;
    GrapheVariabCB: TCheckBox;
    TangenteCB: TCheckBox;
    ParamGB: TGroupBox;
    GrapheParamCB: TCheckBox;
    TableauParamCB: TCheckBox;
    ModeleParamCB: TCheckBox;
    FourierGB: TGroupBox;
    TableauFourierCB: TCheckBox;
    GrapheFourierCB: TCheckBox;
    StatistiqueGB: TGroupBox;
    TableauStatCB: TCheckBox;
    GrapheStatCB: TCheckBox;
    GroupBox2: TGroupBox;
    EnteteEdit: TEdit;
    Panel2: TPanel;
    GraphePageRG: TRadioGroup;
    PagesBtn: TSpeedButton;
    SaveBtn: TBitBtn;
    SaveDialog: TSaveDialog;
    CancelBtn: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure PagesBtnClick(Sender: TObject);
    procedure GraphePageRGClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveBtnClick(Sender: TObject);
  private
    procedure SauveLatex(NomFichier : string);
  public
  end;

var
  LatexDlg: TLatexDlg;

implementation

uses Graphpar, Graphfft, Statisti, Curmodel, Selpage, Latex;

  {$R *.lfm}

procedure TLatexDlg.SauveLatex(NomFichier : string);
var page,sauvePageCourante : codePage;

Procedure ImprimeParam;
var i : integer;
begin
    if GrapheParamCB.checked then
         FgrapheParam.versLatex(NomFichier);
    if ModeleParamCB.visible and ModeleParamCB.checked then begin
         AjouteParagraphe(stModelisation);
         for i := 0 to pred(FgrapheParam.MemoModele.Lines.count) do
             AjouteLigne(FgrapheParam.MemoModele.Lines[i]);
         for i := 0 to pred(FgrapheParam.MemoResultat.Lines.Count) do
             AjouteLigne(FgrapheParam.MemoResultat.Lines[i]);
    end;
    if TableauParamCB.checked then begin
        Fvaleurs.TraceGridParam;
        AjouteGrid(Fvaleurs.GridParam)
    end;
end;

Procedure ImprimeStat;
begin
      if GrapheStatCB.checked then
          FgrapheStat.versLatex(NomFichier);
      if TableauStatCB.checked then begin
         AjouteGrid(FgrapheStat.StatGrid);
         AjouteGrid(FgrapheStat.DistGrid)
      end;
end;

Procedure ImprimeFourier;
begin
        if GrapheFourierCB.checked then
             FgrapheFFT.versLatex(NomFichier);
end;

Procedure ImprimerTableauVariab;
var i,index : integer;
begin
      Fvaleurs.MajGridVariab := true;
      Fvaleurs.TraceGridVariab;
      AjouteParagraphe(pages[pageCourante].TitrePage);
      for i := 0 to pred(ListeConstAff.count) do begin
          index := indexNom(ListeConstAff[i]);
          AjouteLigne(grandeurs[index].FormatNomEtUnite(pages[pageCourante].valeurConst[index]));
      end;
      AjouteGrid(Fvaleurs.GridVariab);
end;

Procedure ImprimerModele(page : codePage);
var i : integer;
begin with pages[page] do begin
      if NbrePages>1 then AjouteParagraphe(TitrePage);
      for i := 0 to pred(TexteResultatModele.Count) do
          AjouteLigne(TexteResultatModele[i]);
end end;

Procedure ImprimerGrapheVariab;
begin
      FgrapheVariab.versLatex(NomFichier);
      if not(FgrapheVariab.graphes[1].superposePage) and
          ModeleVariabCB.visible and
          ModeleVariabCB.checked then
            ImprimerModele(pageCourante);
end;

var i : integer;
    aligne,nomLu,aDate : String;
    posEgal : integer;
    posErreur,longErreur : integer;
    index : integer;
begin
          screen.cursor := crHourGlass;
          enabled := false;
          if dateCB.checked
             then aDate := DateToStr(Now)
             else aDate := '';
          debutDoc(enTeteEdit.text,WinInfo.UserName,aDate);
          try
//  Grandeurs
          if GrandeursCB.checked then begin
             for i := 0 to pred(Fvaleurs.memo.lines.count) do begin
                 aligne := Fvaleurs.Memo.lines[i];
                 if IsLigneComment(aligne)
                    then AjouteLigne(copy(aligne,2,length(aligne)))
                    else begin
                        PosEgal := Pos('=',aligne);
                        if PosEgal=0 then begin
                           grandeurImmediate.fonct.expression := aligne;
                           if grandeurImmediate.compileG(posErreur,longErreur,0) then begin
                              AjouteGrandeur(cImmediate);
                              grandeurImmediate.valeurCourante := calcule(grandeurImmediate.fonct.calcul);
                              AjouteLigne(grandeurImmediate.FormatNombre(grandeurImmediate.valeurCourante));
                            end;
                         end
                         else begin
                             nomLu := copy(aligne,1,pred(posEgal)); // extrait le nom
                             aligne := copy(aligne,succ(posEgal),length(aligne));// pour test forward
                             trimComplet(nomLu);
                             if nomLu[length(nomLu)]='''' then delete(nomLu,length(nomLu),1);
                             if nomLu[length(nomLu)]='''' then delete(nomLu,length(nomLu),1);
                             index := IndexNom(nomLu);
                             if (index<>GrandeurInconnue) and (aligne<>'') then AjouteGrandeur(index);
                          end;
                    end; // ligneCalcul
             end; // for i memo
             ajouteLigne('\begin{itemize}');
             for i := 0 to pred(NbreVariab) do with grandeurs[i] do
                 if (fonct.genreC=g_experimentale) and
                 (fonct.expression<>'') then
                    AjouteLigneItem(nom+' : '+fonct.expression);
              ajouteLigne('\end{itemize}');
           end;
// Valeurs des variables
           if (modeAcquisition=AcqSimulation) and
              not(Fvaleurs.PagesIndependantesCB.checked) then with grandeurs[0] do
                AjouteLigne(nom+'='+
                    formatValeurEtUnite(pages[1].MiniSimulation)+'..'+
                    formatValeurEtUnite(pages[1].MaxiSimulation));
          if TableauVariabCB.visible and
             TableauVariabCB.checked then if GraphePageRG.itemIndex=0
             then ImprimerTableauVariab
             else begin
                 SauvePageCourante := pageCourante;
                 for page := 1 to NbrePages do if pages[page].active then begin
                     pageCourante := page;
                     if (modeAcquisition=AcqSimulation) and
                        (Fvaleurs.PagesIndependantesCB.checked) then with grandeurs[0] do
                              AjouteLigne(nom+'='+
                                   formatValeurEtUnite(pages[page].MiniSimulation)+'..'+
                                   formatValeurEtUnite(pages[page].MaxiSimulation));
                     ImprimerTableauVariab;
                 end;
                 PageCourante := SauvePageCourante;
                 Fvaleurs.MajGridVariab := true;
             end;
{Modélisation}
          if  ModeleVariabCB.visible and ModeleVariabCB.checked then begin
             AjouteParagraphe(stModelisation);
             for i := 0 to pred(TexteModele.count) do begin
                   aligne := TexteModele[i];
                   if IsLigneComment(aligne)
                      then AjouteLigne(copy(aligne,2,length(aligne)))
             end;
             for i := 1 to NbreModele do AjouteModele(fonctionTheorique[i]);
             for i := 1 to NbreFonctionSuper do AjouteModele(fonctionSuperposee[-i]);
          end;
// Graphe et valeurs modélisation
      if GrapheVariabCB.checked
         then if GraphePageRG.itemIndex=0
                 then begin
                     if FgrapheVariab.graphes[1].superposePage and
                        ModeleVariabCB.visible and
                        ModeleVariabCB.checked then
                        for page := 1 to NbrePages do
                            if pages[page].active then ImprimerModele(page);
                     ImprimerGrapheVariab;
                 end
                 else begin
                      SauvePageCourante := pageCourante;
                      for page := 1 to NbrePages do with pages[page] do
                          if active then begin
                             pageCourante := page;
                             for i := 1 to 3 do with FgrapheVariab.Graphes[i] do
                                 if paintBox.Visible then begin
                                    Modif := [gmPage];
                                    PaintBox.Refresh;
                                 end;
                             ImprimerGrapheVariab;
                          end;
                      PageCourante := SauvePageCourante;
                      for i := 1 to 3 do with FgrapheVariab.Graphes[i] do
                          if paintBox.visible then begin
                             Modif := [gmPage];
                             PaintBox.Refresh;
                          end;
          end
          else if ModeleVariabCB.checked and  ModeleVariabCB.visible then
              if (GraphePageRG.itemIndex=0) and
                 not(FgrapheVariab.graphes[1].superposePage)
                 then ImprimerModele(pageCourante)
                 else for page := 1 to NbrePages do
                      if pages[page].active then
                         ImprimerModele(Page);
          if TangenteCB.visible and TangenteCB.checked then begin
             FgrapheVariab.graphes[1].RemplitTableauEquivalence;
             if curseurModeleDlg<>nil then
                 AjouteGrid(curseurModeleDlg.tableau);
             end;
          if FourierGB.visible then ImprimeFourier;
          if StatistiqueGB.visible then ImprimeStat;
          if GrapheParamCB.visible and
             (GrapheParamCB.checked or
              ModeleParamCB.checked or
              TableauParamCB.checked) then ImprimeParam;
          FinDoc;
          except
             on E: Exception do begin
                showMessage(E.message);
             end;
          end;
          Fvaleurs.MajGridVariab := true;
          saveFile(NomFichier);
          enabled := true;
          screen.cursor := crDefault;
end;

procedure TLatexDlg.FormActivate(Sender: TObject);
begin
    TableauParamCB.visible := (NbreConst+NbreParam[paramNormal])>0;
    GrapheParamCB.visible := FgrapheParam<>nil;
    ModeleParamCB.visible := (FgrapheParam<>nil) and
              (FgrapheParam.PanelModele.visible);
    ParamGB.visible := TableauParamCB.visible;
    TangenteCB.caption := stTableau+NomLigneRappel[LigneRappelCourante];
    TangenteCB.visible := FgrapheVariab.graphes[1].equivalences[pageCourante].count>0;
    ModeleVariabCB.visible := not(FgrapheVariab.withModele) or
            (ModeleDefini in FgrapheVariab.etatModele);
    GraphePageRG.visible := (NbrePages>1) and
        not(FgrapheVariab.graphes[1].superposePage);
    GraphePageRG.itemIndex := 0;
    PagesBtn.visible := false;
end;

procedure TLatexDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(807) { TODO : aide latex }
end;

procedure TLatexDlg.PagesBtnClick(Sender: TObject);
begin
    if selectPageDlg=nil then Application.CreateForm(TselectPageDlg, selectPageDlg);
    SelectPageDlg.caption := 'Choix des pages à sauver';
    SelectPageDlg.showModal
end;

procedure TLatexDlg.GraphePageRGClick(Sender: TObject);
begin
    PagesBtn.visible := graphePageRG.itemIndex=1
end;

procedure TLatexDlg.FormCreate(Sender: TObject);
begin
  inherited;
 // LatexCompile := 'c:\texmf\MikTex\bin\latex.exe';
  //LatexView := 'c:\texmf\MikTex\bin\yap.exe';
end;

procedure TLatexDlg.SaveBtnClick(Sender: TObject);
begin
  if SaveDialog.execute then sauveLatex(SaveDialog.FileName);
  close; // ??
end;

end.

