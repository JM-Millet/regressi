{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit CourbesU;

  {$MODE Delphi}

interface

uses
  SysUtils,  Classes,  Graphics, Controls,  Forms,  Dialogs,
  StdCtrls,  ExtCtrls,  Buttons,  Printers,  Math, ComCtrls, clipbrd,
  inifiles, Menus, Spin, grids, colorbox, regUtil, Types;

resourcestring
   hDeplacerPoint = '|Déplacer le point';
   hRepererPoint = '|Cliquer pour enregistrer le point';
   hOrigineBmp = 'Origine|Cliquer sur l''origine';
   hClicDroit = '|Clic droit pour menu zoom';
   hNoZoom = 'Taille réelle';
   hSupprPoint = '|Suppression du point';
   hReglerZoom = '|Réglage du zoom';
   hDebutX  = 'Début X|Cliquer sur le début de l''échelle de ';
   hDebutY  = 'Début Y|Cliquer sur le début de l''échelle de ';
   hFinX  = 'Fin X|Cliquer sur la fin de l''échelle de ';
   hFinY  = 'Fin Y|Cliquer sur la fin de l''échelle de ';
   hTransfertRegressi = 'Traitement des données';
   hRepereBmp = 'Cliquer sur les points à repérer';
   hOrigineXY = '|Origine des coordonnées';
   hDirection = '|Direction';
   hEchelle = '|Echelle';
   hEchelleJpeg = '|Echelle ; double-clic pour agrandir';
   hParamBtn = 'Cliquer sur le bouton Paramètres pour configurer';
   hOrigine = 'Cliquer sur l''origine des axes';
   hOrigineMouse = '|Indique l''origine à l''aide de la souris';
   hStopMes = '|Arrêt des mesures';
   hStartMes = '|Commencer les mesures';
   hClicPoint = 'cliquer sur le point à enregistrer';

   trDimPixel = 'Dimension d''un pixel';
   trLongEchelle = 'Longueur de l''échelle';
   trOrigineEchelle = 'Origine de l''échelle';
   trDistance = 'distance=';
   trEchelle = 'Echelle';

   erPointsProches = 'Points trop proches';
   erEchelle  = 'Echelle incorrecte';

   trOrigineAxe = 'Cliquer sur le point à l''origine des axes';
   trAideEchelle = 'Cliquer sur le premier point puis sur le deuxième de l''échelle';



const
   maxObjet = 6;
   maxPixel = 4096;

type
  indiceObjet = 1..maxObjet;
  Techelle = (eLin,eLog,ePolaire);
  Taxe = (aVertical,aHorizontal,aOblique,aVertHoriz);
  TstyleDrag = (bsNone,bsMesure,bsSuppr,bsOrigine,bsEchelleX2,bsEchelleY2);

  TCourbesForm = class(TForm)
    StatusBar: TStatusBar;
    ZoomMenu: TPopupMenu;
    Zoomx2: TMenuItem;                                            
    Zoom1: TMenuItem;
    Zoomx3: TMenuItem;
    Zoomx4: TMenuItem;
    Zoomx5: TMenuItem;
    Zoomx6: TMenuItem;
    Zoomx7: TMenuItem;
    Zoomx8: TMenuItem;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    ImagePaintBox: TPaintBox;
    Zoom9: TMenuItem;
    Zoom21: TMenuItem;
    Zoom31: TMenuItem;
    Zoom41: TMenuItem;
    Panel3: TPanel;
    ZoomArriereBtn: TSpeedButton;
    ZoomEdit: TEdit;
    ZoomAvantBtn: TSpeedButton;
    SerieLabel: TLabel;
    SerieSE: TSpinEdit;
    Panel4: TPanel;
    PaintBox: TPaintBox;
    Splitter2: TSplitter;
    Grid: TStringGrid;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
    FileBtn: TToolButton;
    StopBtn: TToolButton;
    RazBtn: TToolButton;
    UndoBtn: TToolButton;
    SupprBtn: TToolButton;
    ExiBtn: TToolButton;
    RegressiBtn: TToolButton;
    MesureBtn: TToolButton;
    EchelleBtn: TToolButton;
    EditBidon: TEdit;
    OpenDialog: TOpenDialog;
    ProgressBar: TProgressBar;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CouleurPointsCB: TColorBox;
    CouleurAxeCB: TColorBox;
    SignifEdit: TLabeledEdit;
    NbreSE: TSpinEdit;
    Label1: TLabel;
    ToolButton1: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure RegressiBtnClick(Sender: TObject);
    procedure ImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImagePaint(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure ImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EditBidonKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EchelleBtnClick(Sender: TObject);
    procedure OpenFileBtnClick(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure MesureBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure CouleurPointsCBChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RazBtnClick(Sender: TObject);
    procedure SupprBtnClick(Sender: TObject);
    procedure ZoomAvant(Sender: TObject);
    procedure ZoomMenuPopup(Sender: TObject);
    procedure PaintBoxPaint(Sender: TObject);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ZoomAvantBtnClick(Sender: TObject);
    procedure ZoomArriereBtnClick(Sender: TObject);
    procedure ZoomArriere(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SerieSEChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SignifEditChange(Sender: TObject);
    procedure NbreSEChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    e_PointRepere : array[TstyleDrag] of Tpoint;
    BorneSelect,oldBorne : TstyleDrag;
    PosMenu : Tpoint;
    pointCourant,objetCourant : integer;
    oldStatus : String;
// Vignette 
    pasZoomEntier : double;
    PointDnZoom,PointDnModele : Tpoint;
    RectZoom,RectMove,RectVisible,RectLecture : Trect;
    ZoomEnCours : boolean;
    decaleX,decaleY : integer;
    objetSelect : integer;
    NbreObjet : integer;
    rectCourant : Trect;
    arrondiX,arrondiY : double;
    Procedure envoieDonneesReg;
    Function GetBorne(x,y : integer) : TstyleDrag;
    Function PointProche(x,y : Integer) : integer;
    Procedure AffecteBorne(x,y : integer);
    procedure ImageVersEcran;
    procedure EcranVersImage;
    procedure EffectuerZoom(NewZoom : double);
    Function PointInZoom(x,y : integer) : boolean;
    Procedure AjoutePoint(x,y : integer);
    Procedure SupprimePoint(x,y : integer);
    procedure OuvreFichier(Nom: String);
    Procedure setEchelle;
    Function StrX(j : indiceObjet;i : integer) : String;
    Function StrY(j : indiceObjet;i : integer) : String;
    procedure TraceGrid;
  public
      XY : array[indiceObjet,0..maxPixel] of Tpoint;
      Signif : array[indiceObjet] of String;
      MaxiX,MaxiY : double;
      MiniX,MiniY : double;
      PenteX,PenteY : double;
      ZeroX,ZeroY : double;
      NbrePoints : array[indiceObjet] of integer;
      NomFichier : String;
      axe : Taxe;
      Couleur : array[0..maxObjet] of Tcolor;
      Abitmap,bitmapOriginal : Tbitmap;
      Largeur,Hauteur : integer;
      pasZoom : double;
      PointRepere : array[TstyleDrag] of Tpoint;
      echelleX,echelleY : Techelle;
  end;

var
  CourbesForm: TCourbesForm;

implementation

uses courbesEch, regdde, compile, regmain;

const Magnet = 8;
      Rayon = 5;
      SizeData = 4;
      crTab = #9;
      Titre = 'CourbesBMP';

  {$R *.lfm}

procedure TCourbesForm.FormCreate(Sender: TObject);
var i : tstyleDrag;
    j : integer;
    Fichier : TIniFile;
begin
    NbreSE.MaxValue := maxObjet;
    NbreObjet := 1;
    NomFichier := '';
    for i := low(TstyleDrag) to high(TstyleDrag) do
        PointRepere[i] := point(0,0);
    MaxiX := 1;
    MaxiY := 1;
    MiniX := 0;
    MiniY := 0;
    echelleX := eLin;
    echelleY := eLin;
    Abitmap := Tbitmap.create;
    bitmapOriginal := Tbitmap.create;
    Fichier := TIniFile.create(NomFichierIni);
    for j := 0 to maxObjet do
        Couleur[j] := Fichier.readInteger(Titre,'Couleur'+intToStr(j),clRed);
    CouleurPointsCB.selected := Couleur[1];
    CouleurAxeCB.selected := couleur[0];
    Application.CreateForm(TechelleBmpDlg,echelleBmpDlg);
    MaxiX := 10;
    MaxiY := 10;
    MiniX := 0;
    MiniY := 0;
    with echelleBmpDlg do begin
         uniteX.text := Fichier.readString(Titre,'UniteX','m');
         uniteY.text := Fichier.readString(Titre,'UniteY','m');
         editX.text := Fichier.readString(Titre,'EditX','m');
         editY.text := Fichier.readString(Titre,'EditY','m');
    end;
    Fichier.free;
end;

procedure TCourbesForm.RegressiBtnClick(Sender: TObject);
begin
   StopBtnClick(nil);
   EnvoieDonneesReg;
end;

procedure TCourbesForm.EnvoieDonneesReg;
var tampon : TstringList;

Procedure AjouteOptionGr;
var j : integer;
begin
     tampon.add('&'+intToStr(NbreObjet)+' NOMX');
     if NbreObjet=1
         then tampon.add(EchelleBmpDlg.nomX.text)
         else for j := 1 to NbreObjet do
              tampon.add(EchelleBmpDlg.nomX.text+intToStr(j));
     if echelleX=eLog then begin
        tampon.Add(symbReg2+'1 LOGX');
        tampon.Add(intTostr(ord(true)));
     end;
     tampon.add('&'+intToStr(NbreObjet)+' NOMY');
     if NbreObjet=1
        then tampon.add(EchelleBmpDlg.nomY.text)
        else for j := 1 to NbreObjet do
             tampon.add(EchelleBmpDlg.nomY.text+intToStr(j));
     if echelleY=eLog then begin
        tampon.Add(symbReg2+intToStr(NbreObjet)+' LOGY');
        for j := 1 to nbreObjet do
            tampon.Add(IntToStr(ord(true)));
     end;
     tampon.add('&1 FICHIER');
     tampon.add(NomFichier);
end;

var ligne : String;
    i,j,N : integer;
begin
     tampon := TstringList.create;
     Screen.Cursor := crHourGlass;
     tampon.add(Application.exeName);
     if pageCourante=0
          then tampon.add('Lecture de courbes')
          else tampon.add('');
     tampon.add('Lecture de courbes');
     ligne := 'index';
     if NbreObjet=1
        then begin
            ligne := ligne+crTab+EchelleBmpDlg.nomX.text;
            ligne := ligne+crTab+EchelleBmpDlg.nomY.text;
        end
        else for j := 1 to NbreObjet do begin
           ligne := ligne+crTab+EchelleBmpDlg.nomX.text+intToStr(j);
           ligne := ligne+crTab+EchelleBmpDlg.nomY.text+intToStr(j);
        end;
     tampon.add(ligne);
     ligne := '';
     N := 0;
     for j := 1 to NbreObjet do begin
         ligne := ligne+crTab+EchelleBmpDlg.uniteX.text;
         ligne := ligne+crTab+EchelleBmpDlg.uniteY.text;
         if NbrePoints[j]>N then N := NbrePoints[j];
     end;
     tampon.add(ligne);
     ligne := 'index';
     for j := 1 to NbreObjet do begin
         ligne := ligne+crTab+signif[j];
         ligne := ligne+crTab+signif[j];
     end;
     tampon.add(ligne);
     for i := 0 to pred(N) do begin
         ligne := intToStr(i+1);
         for j := 1 to NbreObjet do begin
             ligne := ligne+crTab+strx(j,i);
             ligne := ligne+crTab+stry(j,i);
         end;
         tampon.add(ligne);
     end;
     if pageCourante=0 then AjouteOptionGr;
     ClipBoard.asText := tampon.text;
     if pageCourante=0
         then with FregressiMain do begin
             grandeursOpen;
             grapheOpen;
             if FormDDE.ImportePressePapier then begin
             end
         end
         else FormDDE.AjoutePressePapier;
     ModeAcquisition := AcqCourbes;
     tampon.free;
     Screen.Cursor := crDefault;
     Close;
end;

Procedure TCourbesForm.SetEchelle;
var PixelX,PixelY : double;
begin
       if PointRepere[bsEchelleY2].y<0 then
              PointRepere[bsEchelleY2].y := 0;
       if PointRepere[bsEchelleY2].y>hauteur then
              PointRepere[bsEchelleY2].y := hauteur;
       if PointRepere[bsEchelleX2].x>largeur then
              PointRepere[bsEchelleX2].x := largeur;
       if PointRepere[bsEchelleX2].x<0 then
              PointRepere[bsEchelleX2].x := 0;
       PixelX := PointRepere[bsEchelleX2].x-PointRepere[bsOrigine].x;
       PixelY := PointRepere[bsEchelleY2].y-PointRepere[bsOrigine].y;
       case echelleX of
            eLog : begin
             penteX := log10(MaxiX/MiniX)/PixelX;
             zeroX := PointRepere[bsOrigine].x;
             if maxiX>miniX
                then arrondiX := power(10,floor(log10(miniX)-1))
                else arrondiX := power(10,floor(log10(maxiX)-1));
             // xr := minix*10^(xi-zeroX) ; xi := zerox+log10(xr/miniX)
          end;
          eLin,ePolaire : begin
             penteX := (MaxiX-MiniX)/PixelX;
             zeroX := PointRepere[bsOrigine].x;
             ArrondiX := power(10,floor(log10(abs(penteX))));
          end;
       end;
       case echelleY of
          eLog : begin
             penteY := log10(MaxiY/MiniY)/PixelY;
             zeroY := PointRepere[bsOrigine].y;
             if maxiY>miniY
                then arrondiY := power(10,floor(log10(miniY)-1))
                else arrondiY := power(10,floor(log10(maxiY)-1));
          end;
          eLin,ePolaire : begin
             penteY := (MaxiY-MiniY)/PixelY;
             zeroY := PointRepere[bsOrigine].y;             
             ArrondiY := power(10,floor(log10(abs(penteY))));
          end;
       end;
       ImagePaintBox.refresh;
       PaintBox.refresh;
       traceGrid;
end;

Function TCourbesForm.GetBorne(x,y : integer) : TstyleDrag;
var i : TstyleDrag;
begin
      result := bsNone;
      x := x - decaleX;
      y := y - decaleY;
      for i := bsOrigine to high(TstyleDrag) do
          if (abs(x-e_PointRepere[i].x)+abs(y-e_PointRepere[i].y) < Magnet)
                then begin
                     result := i;
                     break;
                end;
end;                         

Procedure TCourbesForm.AffecteBorne(x,y : integer);
begin
    if x<0 then x := 0;
    if y<0 then y := 0;
    if y>=rectLecture.bottom then y := rectLecture.bottom-1;
    if x>=rectLecture.right then x := rectLecture.right-1;
    e_PointRepere[BorneSelect] := point(x-decaleX,y-decaleY);
    case BorneSelect of
        bsOrigine : begin
           e_PointRepere[bsEchelleX2].y := e_PointRepere[bsOrigine].y;
           e_PointRepere[bsEchelleY2].x := e_PointRepere[bsOrigine].x;
        end;
        bsEchelleX2 : e_PointRepere[bsOrigine].y := e_PointRepere[bsEchelleX2].y;
        bsEchelleY2 : e_PointRepere[bsOrigine].x := e_PointRepere[bsEchelleY2].x;
    end; // case
    BorneSelect := bsNone;
    EcranVersImage;
    ImagePaintBox.cursor := crDefault;
    setEchelle;
end; // AffecteBorne

procedure TCourbesForm.ImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if BorneSelect=bsMesure then begin
      pointCourant := pointProche(x,y);
      if pointCourant>=0
          then begin
             ImagePaintBox.cursor := crDefault; // CibleMove;
             hint := hDeplacerPoint;
          end
          else begin
             ImagePaintBox.cursor := crDefault; // Cible;
             hint := hRepererPoint;
             pointDnModele := point(x,y);
          end;
      exit;
   end;
   if BorneSelect=bsSuppr then exit;
   BorneSelect := GetBorne(x,y);
   if borneSelect=bsNone
      then begin
         ImagePaintBox.cursor := crDefault;// Cible;
         e_PointRepere[bsNone] := point(x,y);
         pointRepere[bsNone].x := round(e_pointRepere[bsNone].x/pasZoom);
         pointRepere[bsNone].y := round(e_pointRepere[bsNone].y/pasZoom);
         RectCourant := rect(x,y,x,y);
      end
      else ImagePaintBox.cursor := crDrag;
end;

procedure TCourbesForm.ImageMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var BS : tstyleDrag;
begin
   case BorneSelect of
        bsMesure : if ssLeft in shift
          then
          else begin
            if pointProche(x,y)>=0
               then begin
                  ImagePaintBox.cursor := crDefault; //CibleMove;
                  hint := hDeplacerPoint;
               end
               else begin
                  ImagePaintBox.cursor := crDefault;//Cible;
                  hint := hRepererPoint;
               end;
        end;
        bsSuppr : ;
        bsNone : begin
           BS := GetBorne(x,y);
           if BS=bsNone
             then ImagePaintBox.cursor := crDefault
             else ImagePaintBox.cursor := crDefault;//CibleMove;
           case BS of
               bsOrigine : hint := hOrigineBmp;
               bsEchelleX2 : hint := hFinX+EchelleBmpDlg.nomX.text;
               bsEchelleY2 : hint := hFinY+EchelleBmpDlg.nomY.text;
               else hint := hClicDroit;
           end;
           ImagePaintBox.hint := hint;
           if ssLeft in shift then with ImagePaintBox.canvas do begin
               pen.mode := pmNot;
               moveto(rectCourant.left,rectCourant.top);
               lineto(rectCourant.right,rectCourant.bottom);
               rectCourant.right := x;
               rectCourant.bottom := y;
               moveTo(rectCourant.left,rectCourant.top);
               lineTo(rectCourant.right,rectCourant.bottom);
               pen.mode := pmCopy;
           end;
        end;
        else ;
   end; // case borne select 
end;


procedure TCourbesForm.ImagePaint(Sender: TObject);

Procedure TraceLigne(P1,P2 : Tpoint);
begin with imagePaintBox.canvas do begin
         with P1 do moveto(x+decaleX,y+decaleY);
         with P2 do lineto(x+decaleX,y+decaleY);
end end;

Procedure TraceCercle(Apoint : Tpoint);
begin with Apoint do
      imagePaintBox.canvas.ellipse(x-rayon+decaleX,y-rayon+decaleY,
                                    x+rayon+decaleX+1,y+rayon+decaleY+1)
end;

Procedure TraceCroix(Apoint : Tpoint;taille : integer);
begin with Apoint do begin
         TraceLigne(Point(x-taille,y),point(x+taille,y));
         TraceLigne(point(x,y-taille),point(x,y+taille));
end end;

Procedure TraceCroixData(i : integer;j : indiceObjet);
var Origine : Tpoint;
    Apoint : Tpoint;
    texte : string;
begin
     Apoint := xy[j,i];
     imagePaintBox.canvas.pen.color := couleur[j];
     imagePaintBox.canvas.font.color := couleur[j];
     Origine := Point(round(Apoint.x*pasZoom),
                      round(Apoint.y*pasZoom));
     TraceCroix(Point(Origine.x,Origine.y),SizeData);
     texte := intToStr(i);
     if NbreObjet>1 then texte := texte+'('+intToStr(j)+')';
     imagePaintBox.canvas.textOut(Origine.x+SizeData+decaleX,
                                  Origine.y+SizeData+decaleY,
                                  texte);
     Grid.cells[j*2-2,i+1] := strX(j,i);
     Grid.cells[j*2-1,i+1] := strY(j,i);
end;

Procedure TraceBorne;

Procedure TraceFleche(P1i,P2i : Tpoint);
var delta,zz : Tpoint;
    norme : integer;
begin
            delta.X := P1i.x-P2i.x;
            delta.Y := P1i.y-P2i.y;
            norme := round(sqrt(sqr(delta.X)+sqr(delta.Y)));
            delta.X := round(2*Magnet*delta.X/norme);
            delta.Y := round(2*Magnet*delta.Y/norme);
// x,y = centre de la base de la flèche
            zz.y := P2i.y+delta.Y;
            zz.x := P2i.x+delta.X;
            delta.x := round(delta.x/3);
            delta.y := round(delta.y/3);
            TraceLigne(P1i,P2i);
            TraceLigne(P2i,point(zz.x-delta.Y,zz.y+delta.X));
            TraceLigne(P2i,point(zz.x+delta.Y,zz.y-delta.X));
            TraceCercle(P1i);
end; // traceFleche

begin
if NomFichier='' then exit;
with imagePaintBox.canvas do begin
       decaleX := round(-rectVisible.left*pasZoom+pasZoom/2);
       decaleY := round(-rectVisible.top*pasZoom+pasZoom/2);
       pen.color := couleur[0];
       ImageVersEcran;
       TraceFleche(e_PointRepere[bsOrigine],e_PointRepere[bsEchelleY2]);
       TraceFleche(e_PointRepere[bsOrigine],e_PointRepere[bsEchelleX2]);
       TraceCroix(e_PointRepere[bsOrigine],magnet);
end end; // traceBorne

var i,j : integer;
begin
    if NomFichier='' then exit;
    rectLecture := rect(0,0,
       round((rectVisible.right-rectVisible.left)*pasZoom),
       round((rectVisible.bottom-rectVisible.top)*pasZoom));
    ImagePaintBox.canvas.CopyRect(rectLecture,Abitmap.canvas,rectVisible);
    with imagePaintBox.canvas do begin
       if (NbrePoints[1]+2)>grid.rowCount then
          grid.rowCount := NbrePoints[1]+5;
       brush.style := bsClear;
       for j := 1 to NbreObjet do begin
           for i := 0 to pred(NbrePoints[j]) do
               traceCroixData(i,j);
           for i := NbrePoints[j] to (grid.rowCount-2) do begin
               Grid.cells[j*2-2,i+1] := strX(j,i);
               Grid.cells[j*2-1,i+1] := strY(j,i);
           end;
       end;
       grid.Row := NbrePoints[1]+1;
    end;
    TraceBorne;
    EditBidon.setFocus;
end; // ImagePaint

procedure TCourbesForm.ExitBtnClick(Sender: TObject);
begin
    Close
end;

procedure TCourbesForm.ImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if Button<>mbLeft then exit;
     case BorneSelect of
          bsMesure : ajoutePoint(x,y);
          bsSuppr : supprimePoint(x,y);
          bsNone : with ImagePaintBox.canvas do begin
              pen.mode := pmNot;
              moveTo(rectCourant.left,rectCourant.top);
              lineTo(rectCourant.right,rectCourant.bottom);
              pen.mode := pmCopy;              
              ImagePaintBox.cursor := crDefault;              
          end;
          else affecteBorne(x,y);
     end;
end;

Function TCourbesForm.PointProche(X,Y : Integer) : integer;
var distance, newDistance : double;
    i,j : integer;
begin
         result := -1;
         ObjetSelect := 1;
         Distance := 10;
         for j := 1 to NbreObjet do
         for i := 0 to pred(NbrePoints[j]) do begin
             newDistance := abs(xy[j,i].x*pasZoom-x+decaleX)+
                            abs(xy[j,i].y*pasZoom-y+decaleY);
             if (newDistance<distance) then begin
                distance := newDistance;
                result := i;
                ObjetSelect := j;
             end;
         end;
end;

procedure TCourbesForm.EditBidonKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//var P : Tpoint;
begin
     //GetCursorPos(P);
    (*
     case key of
          vk_left,vk_numpad4 : dec(P.X);
          vk_right,vk_numpad6 : inc(P.X);
          vk_down,vk_numpad2 : inc(P.Y);
          vk_up,vk_numpad8 : dec(P.Y);
          vk_prior :  if ssShift in shift
             then dec(P.X,10)
             else dec(P.Y,10);
          vk_next : if ssShift in shift
              then inc(P.X,10)
              else inc(P.Y,10);
          vk_delete : ;
          vk_space,vk_return : case BorneSelect of
             bsMesure : begin
                P := ImagePaintBox.ScreenToClient(P);
                ajoutePoint(P.x,P.y);
                exit;
             end;
             bsSuppr : begin
                P := ImagePaintBox.ScreenToClient(P);
                supprimePoint(P.x,P.y);
                exit;
             end;
          end;
          else exit;
     end;
     setCursorPos(P.X,P.Y);
    *)
end;

procedure TCourbesForm.EchelleBtnClick(Sender: TObject);
begin
    if EchelleBmpDlg.showModal=mrOK then begin
      setEchelle;
      BorneSelect := bsNone;
      ImagePaintBox.Cursor := crDefault;//Cible;
      statusBar.Panels[0].text := hRepererPoint;
      hint := '';
      StopBtn.visible := false;
      MesureBtn.visible := true;
      EchelleBtn.enabled := true;
      PointCourant := -1;
    end;
end;

procedure TCourbesForm.OpenFileBtnClick(Sender: TObject);
begin
    if OpenDialog.execute then
       ouvreFichier(openDialog.fileName)
end;

procedure TCourbesForm.OuvreFichier(Nom: String);

procedure MajBitmap;
var i : integer;
    j : indiceObjet;
begin
     Largeur := Abitmap.width;
     Hauteur := Abitmap.height;
     for j := 1 to maxObjet do begin
         NbrePoints[j] := 0;
         for i := 0 to maxPixel do
             XY[j,i] := point(0,0);
     end;
     PointRepere[bsOrigine].x := Largeur div 5;
     PointRepere[bsEchelleY2].x := PointRepere[bsOrigine].x;
     PointRepere[bsOrigine].y := 4*Hauteur div 5;
     PointRepere[bsEchelleX2].y := PointRepere[bsOrigine].y;
     PointRepere[bsEchelleX2].x := 4*Largeur div 5;
     PointRepere[bsEchelleY2].y := Hauteur div 5;
     ImageVersEcran;
     BorneSelect := bsNone;
     StopBtn.visible := false;
     MesureBtn.visible := false;
     EchelleBtn.enabled := true;
     UndoBtn.enabled := false;
     RegressiBtn.enabled := false;
     pasZoom := 1;
     objetCourant := 1;
     NbreObjet := 1;
     ZoomEdit.text := hNoZoom;
     ImageVersEcran;
     rectVisible := rect(0,0,panel1.width,panel1.height);
     ImagePaintBox.refresh;
     Caption := 'Lecture de courbes ['+ExtractFileName(NomFichier)+']';
     statusBar.Panels[0].text := hRepereBmp;
     setEchelle;
end; // MajBitmap


var extension : String;
    Ajpeg : TjpegImage;
begin
     screen.cursor := crHourGlass;
     NomFichier := AnsiUpperCase(Nom);
     extension := AnsiUpperCase(extractFileExt(NomFichier));
     if (extension='.BMP') then BitmapOriginal.LoadFromFile(NomFichier);
     if (extension='.JPG') or (extension='.JPEG') then begin
         Ajpeg := TjpegImage.create;
         Ajpeg.LoadFromFile(NomFichier);
         with bitmapOriginal do begin
                pixelFormat := pf24bit;
                height := hauteur; // ??
                width := largeur;
         end;
         bitmapOriginal.assign(Ajpeg);
         Ajpeg.free;
     end;
     Abitmap.assign(bitmapOriginal);
     MajBitMap;
     screen.cursor := crDefault;
     EchelleBtnClick(nil);
     statusBar.Panels[0].text := hparamBtn;
end;

Procedure TCourbesForm.ImageVersEcran;
    var i : TstyleDrag;
    begin
       for i := low(TstyleDrag) to High(TstyleDrag) do begin
           e_pointRepere[i].x := round(pointRepere[i].x*pasZoom);
           e_pointRepere[i].y := round(pointRepere[i].y*pasZoom);
       end;
    end;

    procedure TCourbesForm.EcranVersImage;
    var i : TstyleDrag;
    begin
       for i := low(TstyleDrag) to High(TstyleDrag) do begin
           pointRepere[i].x := round(e_pointRepere[i].x/pasZoom);
           pointRepere[i].y := round(e_pointRepere[i].y/pasZoom);
       end;
    end;

procedure TCourbesForm.StopBtnClick(Sender: TObject);
begin
     BorneSelect := bsNone;
     MesureBtn.visible := true;
     UndoBtn.enabled := false;
     StopBtn.visible := false;
     EchelleBtn.enabled := true;
     Screen.cursor := crDefault;
     statusBar.Panels[0].text := hTransfertRegressi;
end;

procedure TCourbesForm.MesureBtnClick(Sender: TObject);
begin
    BorneSelect := bsMesure;
    ImagePaintBox.Cursor := crDefault;//Cible;
    statusBar.Panels[0].text := hRepererPoint;
    hint := '';
    StopBtn.visible := true;
    MesureBtn.visible := false;
    EchelleBtn.enabled := false;
    PointCourant := -1;
    grid.Col := 0;
    grid.Row := 1;
    imagePaintBox.refresh;
    paintBox.refresh;
end;

procedure TCourbesForm.NbreSEChange(Sender: TObject);
begin
       NbreObjet := NbreSE.value;
       SerieSE.maxValue := NbreObjet;
       if serieSE.Value > NbreObjet then SerieSE.value := 1;
       SerieSE.visible := NbreObjet > 1;
       SerieLabel.visible := SerieSE.visible;
       traceGrid;
end;

procedure TCourbesForm.UndoBtnClick(Sender: TObject);
begin
     if NbrePoints[objetCourant]>0 then dec(nbrePoints[ObjetCourant]);
     UndoBtn.enabled := nbrePoints[ObjetCourant]>0;
     SupprBtn.enabled := NbrePoints[ObjetCourant]>0;     
     ImagePaintBox.refresh;
     PaintBox.refresh;
end;

procedure TCourbesForm.CouleurPointsCBChange(Sender: TObject);
begin
      if nomFichier='' then exit;
      (*
      if sender=CouleurAxeCB
         then couleur[0] := couleurAxeCB.selected
         else couleur[objetCourant] := couleurPointsCB.selected;
         *)
      ImagePaintBox.refresh;
      PaintBox.refresh;
end;

procedure TCourbesForm.FormDestroy(Sender: TObject);
begin
    AbitMap.free;
    bitMapOriginal.free;
    courbesForm := nil;
end;

procedure TCourbesForm.FormResize(Sender: TObject);
begin
  inherited;
  signifEdit.width := Panel3.width - signifEdit.left - 8; 
end;

procedure TCourbesForm.RazBtnClick(Sender: TObject);
var i,j : integer;
begin
     for j := 1 to maxObjet do NbrePoints[j] := 0;
     for i := 1 to pred(grid.rowCount) do
         for j := 0 to pred(grid.colCount) do
             grid.cells[j,i] := '';
     ObjetCourant := 1;
     NbreObjet := 1;
     RazBtn.enabled := false;
     UndoBtn.enabled := false;
     SupprBtn.enabled := false;
     RegressiBtn.enabled := false;
     MesureBtn.visible := true;
     EchelleBtn.enabled := true;
     imagePaintBox.refresh;
     PaintBox.refresh;
end;

procedure TCourbesForm.SupprBtnClick(Sender: TObject);
begin
      OldBorne := BorneSelect;
      BorneSelect := bsSuppr;
      ImagePaintBox.cursor := crDefault;
      oldStatus := statusBar.Panels[0].text;
      statusBar.Panels[0].text := hSupprPoint;
      hint := '';
end;

procedure TCourbesForm.ZoomAvant(Sender: TObject);
begin
     EffectuerZoom((sender as TmenuItem).tag)
end;

procedure TCourbesForm.EffectuerZoom(newZoom : double);
var P1,P : Tpoint;
    ecart : integer;
begin
    if newZoom>1 then newZoom := round(newZoom);
    if newZoom<1 then newZoom := 1/round(1/newZoom);
    if nomFichier='' then exit;
    imagePaintBox.visible := false;
    P := ImagePaintBox.ScreenToClient(PosMenu);
// position souris sur l'image
    P1.x := round(P.x/pasZoom) + rectVisible.left;
    P1.y := round(P.y/pasZoom) + rectVisible.top;
// position sur l'image de taille 1
    pasZoom := newZoom;
    rectVisible.left := round(P1.x-P.x/pasZoom);
    rectVisible.top := round(P1.y-P.y/pasZoom);
// position sur l'image de la nouvelle taille
// doit coïncider avec l'ancienne position
    if rectVisible.left<0 then rectVisible.left := 0;
    rectVisible.Right := rectVisible.left+round(panel1.width/pasZoom);
    if rectVisible.right>largeur then rectVisible.right := largeur;
    ecart := panel1.width-round(pasZoom*(rectVisible.right-rectVisible.left));
    if ecart>0 then begin { il reste de la plece }
       rectVisible.left := rectVisible.left-round(ecart/pasZoom);
       if rectVisible.left<0 then rectVisible.left := 0;
       rectVisible.Right := rectVisible.left+round(panel1.width/pasZoom);
       if rectVisible.right>largeur then rectVisible.right := largeur;
    end;
    if rectVisible.top<0 then rectVisible.top := 0;
    rectVisible.bottom := rectVisible.top+round(panel1.height/pasZoom);
    if rectVisible.bottom>hauteur then rectVisible.bottom := hauteur;
    ecart := panel1.height-round(pasZoom*(rectVisible.bottom-rectVisible.top));
    if ecart>0 then begin { il reste de la plece }
       rectVisible.top := rectVisible.top-round(ecart/pasZoom);
       if rectVisible.top<0 then rectVisible.top := 0;
       rectVisible.bottom := rectVisible.top+round(panel1.height/pasZoom);
       if rectVisible.bottom>hauteur then rectVisible.bottom := hauteur;
    end;
    ImageVersEcran;
    ImagePaintBox.visible := true;
    ImagePaintBox.refresh;
    PaintBox.refresh;
    EditBidon.setFocus;
    ZoomEdit.text := hNoZoom;
    if pasZoom>1.1 then ZoomEdit.text := 'Zoom X '+IntToStr(round(pasZoom));
    if pasZoom<0.9 then ZoomEdit.text := 'Zoom / '+IntToStr(round(1/pasZoom));
end;

procedure TCourbesForm.ZoomMenuPopup(Sender: TObject);
begin
    //GetCursorPos(PosMenu) // position souris sur l'écran à l'appel du menu
end;

procedure TCourbesForm.PaintBoxPaint(Sender: TObject);
var TailleLoc : integer;

Procedure TracePoints;

Procedure TraceLigne(P1,P2 : Tpoint);
begin with PaintBox.canvas do begin
         with P1 do moveto(round(pasZoomEntier*x),
                           round(pasZoomEntier*y));
         with P2 do lineto(round(pasZoomEntier*x),
                           round(pasZoomEntier*y));
end end;

Procedure TraceCroix(Apoint : Tpoint);
begin with Apoint do begin
      TraceLigne(point(x-TailleLoc,y),Point(x+TailleLoc,y));
      TraceLigne(point(x,y-TailleLoc),Point(x,y+TailleLoc));
end end;

Procedure TraceCercle(Apoint : Tpoint);
begin
      with Apoint do PaintBox.canvas.ellipse(
                              round(pasZoomEntier*x)-rayon,
                              round(pasZoomEntier*y)-rayon,
                              round(pasZoomEntier*x)+rayon+1,
                              round(pasZoomEntier*y)+rayon+1)
end;

Procedure TraceFleche(P1i,P2i : Tpoint);
var delta,zz : Tpoint;
    norme : integer;
begin
            delta.X := P1i.x-P2i.x;
            delta.Y := P1i.y-P2i.y;
            norme := round(sqrt(sqr(delta.X)+sqr(delta.Y)));
            delta.X := round(2*TailleLoc*delta.X/norme);
            delta.Y := round(2*TailleLoc*delta.Y/norme);
// x,y = centre de la base de la flèche
            zz.y := P2i.y+delta.Y;
            zz.x := P2i.x+delta.X;
            delta.x := round(delta.x/3);
            delta.y := round(delta.y/3);
            TraceLigne(P1i,P2i);
            TraceLigne(P2i,point(zz.x-delta.Y,zz.y+delta.X));
            TraceLigne(P2i,point(zz.x+delta.Y,zz.y-delta.X));
            TraceCercle(P1i);
end;

var i,j : integer;
    texte : String;
begin with PaintBox.canvas do begin
       TailleLoc := round(SizeData/pasZoomEntier);
       if TailleLoc<1 then TailleLoc := 1;
       for j := 1 to NbreObjet do begin
           pen.color := couleur[j];
           font.color := couleur[j];           
           for i := 0 to pred(NbrePoints[j]) do begin
               traceCroix(xy[j,i]);
               texte := intToStr(i);
               if NbreObjet>1 then texte := texte+'('+intToStr(j)+')';
               textOut(round(pasZoomEntier*(xy[j,i].x+tailleLoc)),
                       round(pasZoomEntier*(xy[j,i].y+tailleLoc)),texte);
           end;
       end;
       pen.color := couleur[0];
       TailleLoc := round(Magnet/pasZoomEntier);
       if TailleLoc<1 then TailleLoc := 1;
       TraceFleche(PointRepere[bsOrigine],PointRepere[bsEchelleY2]);
       TraceFleche(PointRepere[bsOrigine],PointRepere[bsEchelleX2]);
       TraceCroix(PointRepere[bsOrigine]);
end end; // TracePoints

procedure traceZoom;

function Conversion(x : integer) : integer;
begin
     result := round(x*pasZoomEntier)
end;

begin with PaintBox.canvas do begin
     pen.color := couleur[0];
     brush.style := bsClear;
     rectZoom := rect(conversion(rectVisible.left),
          conversion(rectVisible.top),
          conversion(rectVisible.right),
          conversion(rectVisible.bottom));
     rectangle(rectZoom.left,rectZoom.top,
          rectZoom.right,rectZoom.bottom)
end end;

var zoomH,zoomV : double;
begin
    if NomFichier='' then exit;
    ZoomH := Panel4.Height/hauteur;
    ZoomV := Panel4.Width/largeur;
    if zoomV<zoomH
       then pasZoomEntier := ZoomV
       else pasZoomEntier := ZoomH;
    PaintBox.height := round(hauteur*pasZoomEntier);
    PaintBox.width := round(largeur*pasZoomEntier);
    PaintBox.canvas.StretchDraw(
             rect(0,0,
                  PaintBox.width,
                  PaintBox.height),Abitmap);
    TraceZoom;
    TracePoints;
    ImagePaint(nil);
    EditBidon.setFocus;
end;

procedure TCourbesForm.PaintBoxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var dx,dy : integer;
begin
     if (ssLeft in shift) and ZoomEnCours
        then begin
          dx := x-pointDnZoom.x;
          dy := y-pointDnZoom.y;
          with PaintBox.canvas do begin
            pen.color := couleur[0];
            pen.mode := pmXOR;
            brush.style := bsClear;
            if dx<-rectMove.left then dx := -rectMove.left;
            if dy<-rectMove.top then dy := -rectMove.top;
            rectangle(rectMove.left,rectMove.top,
                      rectMove.right,rectMove.bottom); { efface }
            rectMove := rect(rectZoom.left+dx,rectZoom.top+dy,
               rectZoom.right+dx,rectZoom.bottom+dy);
            rectangle(rectMove.left,rectMove.top,
                     rectMove.right,rectMove.bottom); { trace }
            pen.mode := pmCopy;         
          end;
        end
        else begin
            if pointInZoom(x,y)
               then begin
                  PaintBox.cursor := crHandPoint;
                  hint := hReglerZoom;
               end
               else begin
                  PaintBox.cursor := crDefault;
                  hint := '';
               end;
        end
end;

procedure TCourbesForm.PaintBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    PointDnZoom := point(x,y);
    ZoomEnCours := pointInZoom(x,y);
    if zoomEnCours
       then begin
          PaintBox.cursor := crHandPoint;
          rectMove := rectZoom
       end
       else PaintBox.cursor := crDefault;
end;

Function TCourbesForm.PointInZoom(X,Y : Integer) : boolean;
begin with rectZoom do
     result := (x>left) and (x<right) and (y>top) and (y<bottom)
end;

procedure TCourbesForm.PaintBoxMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

function Conversion(x : integer) : integer;
begin
     result := round(x/pasZoomEntier)
end;

begin
     if ZoomEnCours then with rectVisible do begin
        left := left+conversion(x-pointDnZoom.x);
        if left<0 then left := 0;
        top := top+conversion(y-pointDnZoom.y);
        if top<0 then top := 0;
        right := left + round(panel1.width/pasZoom);
        if right>largeur
           then if largeur>(panel1.width/pasZoom)
              then begin
                 right := largeur;
                 left := right-round(panel1.width/pasZoom);
              end
              else begin
                 left := 0;
                 right := largeur;
              end;
        bottom := top + round(panel1.height/pasZoom);
        if bottom>hauteur
           then if hauteur>(panel1.height/pasZoom)
              then begin
                bottom := hauteur;
                top := bottom-round(panel1.height/pasZoom);
              end
              else begin
                bottom := hauteur;
                top := 0;
              end;
        PaintBox.cursor := crDefault;
        PaintBox.refresh;
        zoomEnCours := false;
        ImagePaintBox.refresh;
     end;
end;

Procedure TCourbesForm.AjoutePoint(x,y : integer);
var N,j : integer;
begin
    if pointCourant>=0
       then XY[ObjetSelect,PointCourant] :=
                point(round(x/pasZoom-1/2)+rectVisible.left,
                      round(y/pasZoom-1/2)+rectVisible.top)
       else begin
               XY[ObjetCourant,NbrePoints[ObjetCourant]] :=
                        point(round(x/pasZoom)+rectVisible.left,
                              round(y/pasZoom)+rectVisible.top);
               inc(NbrePoints[ObjetCourant]);
       end;
    pointCourant := -1;
    imagePaintBox.cursor := crDefault;//Cible;
    imagePaintBox.refresh;
    UndoBtn.enabled := NbrePoints[ObjetCourant]>0;
    SupprBtn.enabled := NbrePoints[ObjetCourant]>0;
    N := 0;
    for j := 1 to NbreObjet do
        N := N+NbrePoints[j];
    RazBtn.enabled := N>1;
    RegressiBtn.enabled := N>3;
    PaintBox.refresh;
end;

Procedure TCourbesForm.SupprimePoint(x,y : integer);
var i,num,N : integer;
begin
      Num := PointProche(x,y);
      if num>=0 then begin
         dec(NbrePoints[objetSelect]);
         for i := num to pred(NbrePoints[ObjetSelect]) do
             xy[ObjetSelect,i] := xy[ObjetSelect,succ(i)];
         imagePaintBox.refresh;
         PaintBox.refresh;
         UndoBtn.enabled := NbrePoints[ObjetCourant]>0;
         SupprBtn.enabled := NbrePoints[ObjetCourant]>0;
         N := 0;
         for i := 1 to NbreObjet do
             N := N+NbrePoints[i];
         RazBtn.enabled := N>1;
         RegressiBtn.enabled := N>3;
      end;
      BorneSelect := oldBorne;
      statusBar.Panels[0].text := oldStatus;
end;

procedure TCourbesForm.ToolButton1Click(Sender: TObject);
begin
end;

procedure TCourbesForm.ZoomAvantBtnClick(Sender: TObject);
begin
     if pasZoom<8 then effectuerZoom(2*pasZoom)
end;

procedure TCourbesForm.ZoomArriereBtnClick(Sender: TObject);
begin
     if pasZoom>1/8 then EffectuerZoom(pasZoom/2)
end;

procedure TCourbesForm.ZoomArriere(Sender: TObject);
begin
     effectuerZoom(1/((sender as TmenuItem).tag))
end;

procedure TCourbesForm.Splitter1Moved(Sender: TObject);
begin
     effectuerZoom(pasZoom)
end;

procedure TCourbesForm.SignifEditChange(Sender: TObject);
begin
  signif[objetCourant] := signifEdit.text
end;


procedure TCourbesForm.FormClose(Sender: TObject; var Action: TCloseAction);
var Fichier : TIniFile;
    j : integer;
begin
     try
     Fichier := TIniFile.create(NomFichierIni);
     try
     for j := 0 to maxObjet do
         Fichier.writeInteger(Titre,'Couleur'+intToStr(j),Couleur[j]);
     with echelleBmpDlg do begin
         Fichier.writeString(Titre,'UniteX',uniteX.text);
         Fichier.writeString(Titre,'UniteY',uniteY.text);
         Fichier.writeString(Titre,'EditX',editX.text);
         Fichier.writeString(Titre,'EditY',editX.text);
     end;
     finally
     Fichier.free;
     end;
     except
     end;
     inherited;
end;

procedure TCourbesForm.SerieSEChange(Sender: TObject);
begin
   ObjetCourant := round(SerieSE.Value);
//   CouleurPointsCB.selected := couleur[objetCourant];
   signifEdit.text := signif[objetCourant];
end;

Function TCourbesForm.StrX(j : indiceObjet;i : integer) : String;
var xx,yy : double;
begin
    if i>=NbrePoints[j] then result := '' else begin
    xx := penteX*(xy[j,i].x-zeroX);
    case echelleX of
       eLog : xx := MiniX*power(10,xx);
       eLin : xx := MiniX+xx;
       ePolaire : begin
            yy := penteY*(xy[j,i].y-zeroY);
            xx := radToDeg(arcTan2(yy,xx));
       end;
    end;
    result := FloatToStrPoint(ArrondiX*round(xx/ArrondiX));
    end;
end;

Function TCourbesForm.StrY(j : indiceObjet;i : integer) : String;
var yy,xx : double;
begin
    if i>=NbrePoints[j] then result := '' else begin
    yy := penteY*(xy[j,i].y-zeroY);
    case echelleY of
       eLog : yy := MiniY*power(10,yy);
       eLin : yy := MiniY+yy;
       ePolaire : begin
           xx := penteX*(xy[j,i].x-zeroX);
           yy := sqrt(sqr(xx)+sqr(yy));
       end;
    end;
    result := FloatToStrPoint(ArrondiY*round(yy/ArrondiY))
    end;
end;

procedure TCourbesForm.GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var s : String;
begin
   TstringGrid(sender).canvas.Font.Color := couleur[1+(Acol div 2)];
   S := TstringGrid(sender).cells[Acol,Arow];
   with TstringGrid(sender).canvas do
        TextOut(Rect.Left + 2, Rect.Top + 2,S);
end;

procedure TCourbesForm.FormActivate(Sender: TObject);
begin
   windowState := wsMaximized;
end;

Procedure TCourbesForm.TraceGrid;
var i,j : integer;
begin
    Grid.ColCount := NbreObjet*2;
    for j := 1 to NbreObjet do begin
        Grid.cells[j*2-2,0] := EchelleBmpDlg.nomX.text+intToStr(j)+'('+
                               EchelleBmpDlg.uniteX.text+')';
        Grid.cells[j*2-1,0] := EchelleBmpDlg.nomY.text+intToStr(j)+'('+
                               EchelleBmpDlg.uniteY.text+')';
    end;
    for j := 1 to NbreObjet do begin
       for i := 0 to pred(NbrePoints[j]) do begin
           Grid.cells[j*2-2,i+1] := strX(j,i);
           Grid.cells[j*2-1,i+1] := strY(j,i);
       end;
       for i := NbrePoints[j] to (grid.rowCount-2) do begin
           grid.cells[j*2-2,i+1] := '';
           grid.cells[j*2-1,i+1] := '';
       end;
    end;
end;

end.


