{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit options;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, Spin, sysUtils, ComCtrls, Dialogs,
  inifiles, maths, regutil, uniteker, compile,
  graphker, aidekey;

type

  { TOptionsDlg }

  TOptionsDlg = class(TForm)
    ClavierAvecGrapheCB: TCheckBox;
    PageControl: TPageControl;
    CalculTS: TTabSheet;
    TabSheet2: TTabSheet;
    DegreRG: TRadioGroup;
    Panel1: TPanel;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    TabSheet7: TTabSheet;
    FontSizeImprRG: TRadioGroup;
    Label8: TLabel;
    Label9: TLabel;
    PermuteCB: TCheckBox;
    TempDirRG: TRadioGroup;
    TriCB: TCheckBox;
    UseMilliCB: TCheckBox;
    Label3: TLabel;
    ChiffresSignifSE: TSpinEdit;
    LissageLabel: TLabel;
    OrdreFiltrageSE: TSpinEdit;
    Label1: TLabel;
    NbreEdit: TEdit;
    NbreSpinButton: TSpinEdit;
    GridPrintCB: TCheckBox;
    TabSheet4: TTabSheet;
    TraceDefautRG: TRadioGroup;
    GrilleCB: TCheckBox;
    IncertitudeCB: TCheckBox;
    Label11: TLabel;
    Label17: TLabel;
    OrdreSplineSE: TSpinEdit;
    DimPointSE: TSpinEdit;
    OptionsBtn: TSpeedButton;
    GraduationZeroCB: TCheckBox;
    AngleEnDegreCB: TCheckBox;
    GraduationPiCB: TCheckBox;
    IncertitudeRG: TRadioGroup;
    ConstUniversBtn: TBitBtn;
    Memo1: TMemo;
    CoeffEllipseRG: TRadioGroup;
    GroupBox3: TGroupBox;
    LabelTaille: TLabel;
    SpinEditHauteur: TSpinEdit;
    NbreLabel: TLabel;
    NbreSE: TSpinEdit;
    ExtrapoleDerCB: TCheckBox;
    fontSizeMemoRG: TRadioGroup;
    FenetreRG: TRadioGroup;
    Label12: TLabel;
    grapheClipRG: TRadioGroup;
    ImpMonoCB: TCheckBox;
    UniteParCB: TCheckBox;
    IncertitudeHelpBtn: TSpeedButton;
    GroupBox1: TGroupBox;
    AjustageAutoGrCB: TCheckBox;
    AjustageAutoLinCB: TCheckBox;
    UseChi2CB: TCheckBox;
    LevenbergCB: TCheckBox;
    UniteSICB: TCheckBox;
    procedure NbreSpinButtonDownClick(Sender: TObject);
    procedure NbreSpinButtonUpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DegreRGClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure NbreEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure UniteSICBClick(Sender: TObject);
    procedure OptionsBtnClick(Sender: TObject);
    procedure ModifGrapheClick(Sender: TObject);
    procedure ConstUniversBtnClick(Sender: TObject);
    procedure PageControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure CoeffEllipseRGClick(Sender: TObject);
    procedure SpinEditHauteurChange(Sender: TObject);
    procedure UseMPCBClick(Sender: TObject);
    procedure IncertitudeHelpBtnClick(Sender: TObject);
    procedure UseChi2CBClick(Sender: TObject);
    procedure IncertitudeCBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    modifConfig : boolean;
    procedure LitConfig;
  public
    modifPreferences,modifDerivee,modifUniteSI,
    modifPolice,modifGraphe : boolean;
    Procedure ResetConfig;
  end;

var
  OptionsDlg: TOptionsDlg;
  OptionsMenuFichier,OptionsPermises : boolean;

implementation

uses  fft, optcolordlg, constante, AideIncertitudeU;

const TailleFonte : array[0..2] of integer = (10,12,14);

  {$R *.lfm}

procedure TOptionsDlg.NbreSpinButtonDownClick(Sender: TObject);
begin
     if NbrePointDerivee>MinPointsDerivee then begin
        dec(NbrePointDerivee,2);
        ModifDerivee := true;
        NbreEdit.text := IntToStr(NbrePointDerivee);
     end;
end;

procedure TOptionsDlg.NbreSpinButtonUpClick(Sender: TObject);
begin
     if NbrePointDerivee<MaxPointsDerivee then begin
        inc(NbrePointDerivee,2);
        ModifDerivee := true;
        NbreEdit.text := IntToStr(NbrePointDerivee);
     end;
end;

procedure TOptionsDlg.FormActivate(Sender: TObject);
begin
       inherited;
       ClavierAvecGrapheCB.checked :=  ClavierAvecGraphe;
//       widthEcranSE.value := penWidthVGA;
//       penwidthaxeSE.value := penWidthAxeConfig;

       TriCB.Checked := DataTrieGlb;
       IncertitudeRG.itemIndex := ord(TraceIncert);
       uniteParCB.checked := UniteParenthese;
       LevenbergCB.checked := LevenbergMarquardt;
       ExtrapoleDerCB.checked := ExtrapoleDerivee;
       GridPrintCB.checked := GridPrint;

       GraduationPiCB.checked := GraduationPi;
       NbreEdit.text := IntToStr(NbrePointDerivee);
       DegreRG.itemIndex := DegreDerivee-1;

       TraceDefautRG.itemIndex := ord(TraceDefaut);
       GrilleCB.checked := OgQuadrillage in OptionGrapheDefault;
       grapheClipRG.ItemIndex := ord(grapheClip);
       OrdreSplineSE.Value := OrdreLissage;
       OrdreFiltrageSE.Value := OrdreFiltrage;
       DimPointSE.Value := DimPointVGA;
       IncertitudeCB.checked := avecEllipse;
       UseChi2CB.checked := avecChi2;
       UniteSICB.checked := uniteSIglb;
       impmonocb.checked := imprimanteMonochrome;
       ModifDerivee := false;
       ModifPolice := false;
       ModifUniteSI := false;
       ModifPreferences := false;
       ModifGraphe := false;
       PermuteCB.checked := permuteColRow;
       OrdreSplineSE.Value := OrdreLissage;
       OrdreFiltrageSE.Value := OrdreFiltrage;
       AngleEnDegreCB.checked := angleEnDegre;       
       ChiffresSignifSE.value := precision;
       if coeffEllipse<1.1
          then CoeffEllipseRG.ItemIndex := 0
          else if coeffEllipse<2
              then CoeffEllipseRG.ItemIndex := 1
              else CoeffEllipseRG.ItemIndex := 2;
       spinEditHauteur.Value := texteGrapheSize;
       NbreSE.Value := NbreTexteMax;              
       case fontSizeMemo of
            10 : FontSizeMemoRG.itemIndex := 0;
            12 : FontSizeMemoRG.itemIndex := 1;
            14 : FontSizeMemoRG.itemIndex := 2;
            else FontSizeMemoRG.itemIndex := 1;
       end;
       case fontSizeImpr of
            10 : FontSizeImprRG.itemIndex := 0;
            12 : FontSizeImprRG.itemIndex := 1;
            else FontSizeImprRG.itemIndex := 0;
       end;
       GraduationZeroCB.checked := OgZeroGradue in optionGrapheDefault;
       if not OptionsPermises then pageControl.activePage := calculTS;
end;

procedure TOptionsDlg.DegreRGClick(Sender: TObject);
begin
      DegreDerivee := 1+DegreRG.itemIndex;
      ModifDerivee := true;
end;

procedure TOptionsDlg.OKBtnClick(Sender: TObject);

procedure SauveOptions;
var i : integer;
    Rini : TIniFile;
begin
    try
    Rini := TIniFile.create(NomFichierIni);
    try

    RIni.writeInteger('Graphe','Clip',ord(grapheClip));
    Rini.WriteInteger('PenStyle','Tangente',ord(PstyleTangente));
    Rini.WriteInteger('Color','Tangente',pcolorTangente);
    Rini.WriteInteger('PenStyle','Reticule',ord(PstyleReticule));
    Rini.WriteInteger('Color','Reticule',pcolorReticule);
    Rini.WriteInteger('Color','FondReticule',FondReticule);
    Rini.writeInteger('Dérivée','Nombre',NbrePointDerivee);
    Rini.writeBool('Dérivée','Extrapole',ExtrapoleDerivee);
    Rini.writeInteger('Calcul','OrdreFiltrage',OrdreFiltrage);
    Rini.WriteBool('Calcul','UniteSI',UniteSIglb);
    Rini.writeBool('Calcul','Cosinus',ModeleCosinus);
    Rini.writeBool('Graphe','Clavier',ClavierAvecGraphe);
    Rini.writeInteger('Point','Taille',dimPointVGA);
    Rini.writeInteger('Color','Axe',couleurGrille);
    Rini.writeInteger('Dérivée','Degré',DegreDerivee);
    Rini.writeInteger('Graphe','ReperePage',ord(reperePage));
    Rini.writeInteger('Format','Chiffres',Precision);
    Rini.writeInteger('Fonte','TailleMemo',FontSizeMemo);
    Rini.writeInteger('Fonte','TexteGraphe',TexteGrapheSize);
    Rini.writeInteger('Graphe','NbreTexte',NbreTexteMax);
    Rini.writeBool('Graphe','ZeroGradue',OgZeroGradue in OptionGrapheDefault);
    Rini.writeBool('Calcul','ModelePourCent',ModelePourCent);
    Rini.writeBool('CoeffCorrel','Affiche',WithCoeffCorrel);
    Rini.writeBool('Graphe','Pvaleur',WithPvaleur);
    Rini.writeInteger('Graphe','AffIncertParam',ord(AffIncertParam));
    Rini.writeBool('Graphe','BandeConfiance',WithBandeConfiance);
    Rini.writeBool('FFT','Optimise',NbreHarmoniqueOptimise);
    Rini.writeBool('FFT','HarmAff',HarmoniqueAff);
    Rini.writeBool('Graphe','VisuAjuste',VisualisationAjustement);
    Rini.writeBool('Graphe','Ellipse',avecEllipse);
    Rini.writeBool('Regressi','Chi2',avecChi2);
    Rini.WriteBool('Regressi','AjusteAutoLin',AjustageAutoLinCB.checked);
    Rini.WriteBool('Regressi','Levenberg',LevenbergMarquardt);
    Rini.WriteBool('Regressi','AjusteAutoGr',AjustageAutoGrCB.checked);
    Rini.WriteBool('Regressi','Permute',PermuteColRow);
    Rini.WriteBool('Regressi','DataTrie',DataTrieGlb);
    Rini.WriteBool('Regressi','UnitePar',UniteParenthese);
    Rini.WriteInteger('Regressi','IncertRect',ord(TraceIncert));
    Rini.WriteBool('Regressi','GraduationPi',GraduationPi);
    Rini.WriteInteger('Graphe','CoeffEllipse',CoeffEllipseRG.itemIndex);
    Rini.WriteBool('Graphe','Rappel3',RappelTroisGraphes);   
    Rini.WriteBool('Calcul','Degre',AngleEnDegre);
    Rini.WriteBool('Calcul','Decibel',ModeleDecibel);
    Rini.WriteBool('Calcul','Qualite',ModeleFacteurQualite);
    Rini.writeBool('Calcul','Cosinus',ModeleCosinus);
    Rini.writeInteger('Impression','Fonte',FontSizeImpr);
    Rini.writeInteger('Graphe','WidthEcran',penWidthVGA);
    Rini.writeInteger('Graphe','WidthGrid',penWidthGrid);
    Rini.writeInteger('Graphe','TailleTick',tailleTick);
    Rini.DeleteKey('Impression','Bold');

    Rini.EraseSection('Acquisition');
    Rini.WriteString('Repertoire','Data',MesDocsDir);
    Rini.WriteString('Fonte','Name',FontName);
    for i := 0 to pred(NbreCouleur) do begin
       Rini.writeInteger('Color','C'+IntToStr(i),couleurPages[i]);
       Rini.writeInteger('PenStyle','C'+IntToStr(i),ord(stylePages[i]));
       Rini.writeInteger('PointStyle','C'+IntToStr(i),ord(MotifPages[i]));
    end;
    for i := 1 to MaxOrdonnee do begin
       Rini.writeInteger('Color','O'+IntToStr(i),couleurInit[i]);
    //  PstyleInit[i] := TpenStyle(Ini.ReadInteger('PenStyle','O'+IntToStr(i),ord(PstyleInit[i])));
       Rini.writeInteger('PointStyle','O'+IntToStr(i),ord(MotifInit[i]));
    end;
    finally
    Rini.free;
    end;
    except
    end;
end;

begin // OKBtnClick
     modifConfig := true;
     UniteParenthese := uniteParCB.checked;
     DataTrieGlb := TriCB.Checked;
     ClavierAvecGraphe := ClavierAvecGrapheCB.checked;
     FichierTrie := DataTrieGlb;

     TraceDefaut := TtraceDefaut(TraceDefautRG.itemIndex);
     if GraduationZeroCB.checked
        then include(optionGrapheDefault,OgZeroGradue)
        else exclude(optionGrapheDefault,OgZeroGradue);
   //  penWidthVGA := widthEcranSE.value;
   //  penWidthAxeConfig := penWidthAxeSE.value;
     avecEllipse := IncertitudeCB.checked;
     DimPointVGA := DimPointSE.value;
     imprimanteMonochrome := impmonocb.checked;
     grapheClip := TgrapheClipBoard(grapheClipRG.itemIndex);
     OrdreLissage := OrdreSplineSE.Value;
     OrdreFiltrage := OrdreFiltrageSE.Value;     
     ModifDerivee := ModifDerivee or (ExtrapoleDerivee<>ExtrapoleDerCB.checked);
     ExtrapoleDerivee := ExtrapoleDerCB.checked;
     LevenbergMarquardt := LevenbergCB.checked;
     if GrilleCB.checked
         then include(OptionGrapheDefault,OgQuadrillage)
         else exclude(OptionGrapheDefault,OgQuadrillage);

     GraduationPi := GraduationPiCB.checked;

     GridPrint := GridPrintCB.checked;
     fontSizeMemo := tailleFonte[FontSizeMemoRG.itemIndex];
     TexteGrapheSize := spinEditHauteur.value;
     NbreTexteMax := NbreSE.value;
     fontSizeImpr := tailleFonte[FontSizeImprRG.itemIndex];
     precision := chiffresSignifSE.value;
     permuteColRow := PermuteCB.checked;
     AngleEnDegre := AngleEnDegreCB.checked;
     UniteSIglb := UniteSICB.checked;
     avecChi2 := UseChi2CB.checked;
     if avecChi2 then avecEllipse := true;
     OrdreLissage := OrdreSplineSE.Value;
     OrdreFiltrage := OrdreFiltrageSE.Value;     
     TraceIncert := Tincertitude(IncertitudeRG.itemIndex);
     case CoeffEllipseRG.ItemIndex of
         0 : coeffEllipse := 1; // 68 %
         1 : coeffEllipse := 1.96; // 95 %
         2 : coeffEllipse := 2.58; // 99 %
     end;
     try
     SauveOptions;
     except
     end;
end; // OKBtnClick

procedure TOptionsDlg.NbreEditChange(Sender: TObject);
begin
     ModifDerivee := true
end;

procedure TOptionsDlg.FormCreate(Sender: TObject);
begin
  OptionsMenuFichier := false;
  ModifConfig := false;
  OptionsPermises := true;
  try
  litConfig;
  except
  end;
// vérification de l'existence du répertoire
  if (mesDocsDir<>'') and
     not(DirectoryExists(mesDocsDir))
        then mesDocsDir := '';
  ChiffresSignifSE.minValue := precisionMin;
  ChiffresSignifSE.maxValue := precisionMax;
end; // FormCreate

procedure TOptionsDlg.FormShow(Sender: TObject);
begin
end;

procedure TOptionsDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_Options)
end;

procedure TOptionsDlg.PageControlChange(Sender: TObject);
begin
end;

procedure TOptionsDlg.IncertitudeCBClick(Sender: TObject);
begin
    ModifGraphe := true;
    usechi2CB.checked := incertitudeCB.checked;
end;

procedure TOptionsDlg.IncertitudeHelpBtnClick(Sender: TObject);
begin
 if AideIncertitudeForm=nil then
     Application.CreateForm(TAideIncertitudeForm, AideIncertitudeForm);
  AideIncertitudeForm.pageControl1.activePage := AideIncertitudeForm.tabSheet3;
  AideIncertitudeForm.show;
end;

procedure TOptionsDlg.UseChi2CBClick(Sender: TObject);
begin
    incertitudeCB.Checked := usechi2CB.checked;
end;

procedure TOptionsDlg.UseMPCBClick(Sender: TObject);
begin
  inherited;
  modifPreferences := true;
end;

procedure TOptionsDlg.UniteSICBClick(Sender: TObject);
begin
    ModifUniteSI := true
end;

procedure TOptionsDlg.OptionsBtnClick(Sender: TObject);
begin
   OptionCouleurDlg := TOptionCouleurDlg.create(self);
   OptionCouleurDlg.DlgGraphique := nil;
   OptionCouleurDlg.ShowModal;
   OptionCouleurDlg.free;
   ModifGraphe := true;
end;

procedure TOptionsDlg.ModifGrapheClick(Sender: TObject);
begin
    ModifGraphe := true;
end;

procedure TOptionsDlg.LitConfig;
// sinon relecture
var Rini : TIniFile;
begin
  try
  Rini := TIniFile.create(NomFichierIni);
  try
  grapheClip := TgrapheClipBoard(Rini.ReadInteger('Graphe','Clip',0));
  ModeleCosinus := Rini.ReadBool('Calcul','Cosinus',ModeleCosinus);
  ClavierAvecGraphe := Rini.readBool('Graphe','Clavier',ClavierAvecGraphe);
//  PstyleTangente := TpenStyle(Rini.ReadInteger('PenStyle','Tangente',psDot));
  PcolorTangente := Rini.ReadInteger('Color','Tangente',clBlack);
 // PstyleReticule := TpenStyle(Rini.ReadInteger('PenStyle','Reticule',psDot));
  PcolorReticule := Rini.ReadInteger('Color','Reticule',clBlack);
  FondReticule := Rini.ReadInteger('Color','FondReticule',clInfoBk);
  OrdreFiltrage := Rini.ReadInteger('Calcul','OrdreFiltrage',OrdreFiltrage);
  CoeffEllipseRG.itemIndex := Rini.ReadInteger('Graphe','CoeffEllipse',0);
  ModelePourCent := Rini.readBool('Calcul','ModelePourCent',ModelePourCent);
  //UniteSIglb := Rini.ReadBool('Calcul','UniteSI',UniteSIglb);
  GraduationZeroCB.checked := Rini.ReadBool('Graphe','ZeroGradue',ogZeroGradue in OptionGrapheDefault);
  if GraduationZeroCB.checked then
     include(OptionGrapheDefault,ogZeroGradue);
  LevenbergMarquardt := Rini.ReadBool('Regressi','Levenberg',false);

  AngleEnDegre := Rini.ReadBool('Calcul','Degre',true);
  ModeleDecibel := Rini.ReadBool('Calcul','Decibel',false);
  ModeleFacteurQualite := Rini.ReadBool('Calcul','Qualite',true);
  ExtrapoleDerivee := Rini.ReadBool('Dérivée','Extrapole',true);
  NbreHarmoniqueOptimise := Rini.ReadBool('FFT','Optimise',false);
  HarmoniqueAff := Rini.ReadBool('FFT','HarmAff',HarmoniqueAff);
  DimPointVGA := Rini.ReadInteger('Point','Taille',3);
  DataTrieGlb := Rini.ReadBool('Regressi','DataTrie',false);
  TraceIncert := Tincertitude(Rini.ReadInteger('Regressi','IncertRect',ord(TraceIncert)));
  UniteParenthese := Rini.ReadBool('Regressi','UnitePar',UniteParenthese);
  VisualisationAjustement := Rini.ReadBool('Graphe','VisuAjuste',false);
  fontSizeImpr := Rini.ReadInteger('Impression','Fonte',10);
  fontSizeImpr := 2*(fontSizeImpr div 2);
  penWidthVGA := 1;
  penWidthGrid := Rini.readInteger('Graphe','WidthGrid',1);
  tailleTick := Rini.readInteger('Graphe','TailleTick',1);
  if (fontSizeImpr<8) or (fontSizeImpr>12) then fontSizeImpr := 10;
  Precision := Rini.ReadInteger('Format','Chiffres',4);
  if precision<precisionMin then precision := precisionMin;
  if precision>precisionMax then precision := precisionMax;
  NbrePointDerivee := Rini.ReadInteger('Dérivée','Nombre',5);
  DegreDerivee := Rini.ReadInteger('Dérivée','Degré',2);
  if degreDerivee<1 then degreDerivee := 1;
  if degreDerivee>3 then degreDerivee := 3;
  TraceDefaut := TTraceDefaut(Rini.ReadInteger('Graphe','TracePoint',ord(tdPoint)));
  AjustageAutoLinCB.checked := Rini.ReadBool('Regressi','AjusteAutoLin',true);
  AjustageAutoGrCB.checked := Rini.ReadBool('Regressi','AjusteAutoGr',false);
  GraduationPi := Rini.ReadBool('Regressi','GraduationPi',true);
  WithCoeffCorrel := Rini.ReadBool('CoeffCorrel','Affiche',false);
  WithPvaleur := Rini.ReadBool('Graphe','Pvaleur',false);
  AffIncertParam := TAffIncertParam(Rini.readInteger('Graphe','AffIncertParam',ord(i95)));
  WithBandeConfiance := Rini.ReadBool('Graphe','BandeConfiance',false);

  PermuteColRow := Rini.ReadBool('Regressi','Permute',false);
  avecEllipse := Rini.ReadBool('Graphe','Ellipse',false);
  RappelTroisGraphes := Rini.ReadBool('Graphe','Rappel3',false);
  avecChi2 := Rini.ReadBool('Regressi','Chi2',false);
  if avecChi2 then avecEllipse := true;
  fontSizeMemo := Rini.ReadInteger('Fonte','TailleMemo',12);
  reperePage := TreperePage(Rini.ReadInteger('graphe','ReperePage',ord(SPcouleur)));
  TexteGrapheSize := Rini.ReadInteger('Fonte','TexteGraphe',5);
  NbreTexteMax := Rini.ReadInteger('Graphe','NbreTexte',NbreTexteMax);
  GridPrint := Rini.ReadBool('Impression','Grid',false);
  FontName := Rini.ReadString('Fonte','Name',FontName);

  mesDocsDir := Rini.ReadString('Repertoire','Data',mesDocsDir);
  finally
  Rini.Free;
  end;
  except
  end;
end; // LitConfigUtilisateur

Procedure TOptionsDlg.ResetConfig;
begin
    if not ModifConfig then litConfig;
end;

procedure TOptionsDlg.ConstUniversBtnClick(Sender: TObject);
begin
  ConstanteUnivDlg.showModal
end;

procedure TOptionsDlg.PageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
    allowChange := optionsPermises
end;

procedure TOptionsDlg.CoeffEllipseRGClick(Sender: TObject);
begin
     case CoeffEllipseRG.ItemIndex of
         0 : coeffEllipse := 1; // 68 %
         1 : coeffEllipse := 1.96; // 95 %
         2 : coeffEllipse := 2.58; // 99 %
     end;
end;

procedure TOptionsDlg.SpinEditHauteurChange(Sender: TObject);
begin
  modifGraphe := true;
end;

end.


