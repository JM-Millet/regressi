{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit affectenom;

  {$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, inifiles,
  regutil, compile;

type
  TAffecteNomDlg = class(TForm)
    StringGrid: TStringGrid;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    TriCB: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure StringGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
      IniMeteo : TIniFile;
      NomRef,NomMeteo,UniteMeteo,SignifMeteo : TstringList;
      IndexMeteo : array[codeGrandeur] of codeGrandeur;
  public
  end;

var
  AffecteNomDlg: TAffecteNomDlg;

implementation

  {$R *.lfm}

procedure TAffecteNomDlg.FormCreate(Sender: TObject);
var i : integer;
    zzz : string;
begin
     NomRef := TstringList.create;
     NomMeteo := TstringList.create;
     UniteMeteo := TstringList.create;
     SignifMeteo := TstringList.create;
     try
     IniMeteo := TIniFile.create(NomFichierIni);
     IniMeteo.ReadSection('NomMeteo',NomRef);
     for i := 0 to pred(NomRef.count) do begin
         zzz := IniMeteo.ReadString('NomMeteo',NomRef[i],'');
         NomMeteo.add(zzz);
         zzz := IniMeteo.ReadString('UniteMeteo',NomRef[i],'');
         UniteMeteo.add(zzz);
         zzz := IniMeteo.ReadString('SignifMeteo',NomRef[i],'');
         SignifMeteo.add(zzz);
     end;
     except
     end;
     stringGrid.ColWidths[0] := 140;
     stringGrid.ColWidths[1] := 75;
     stringGrid.ColWidths[2] := 75;
     stringGrid.cells[0,0] := 'Signification';
     stringGrid.cells[1,0] := 'Nom';
     stringGrid.cells[2,0] := 'Unité';
end;

procedure TAffecteNomDlg.FormActivate(Sender: TObject);
var i,j,ligne,imax : integer;
    trouve : boolean;
begin
     inherited;
     StringGrid.rowCount := succ(NbreVariab);
     TriCB.Checked := DataTrieGlb;
     if grandeurs[0].nom='var1'
        then begin
            stringGrid.FixedCols := 0;
            if nbreVariab<nomMeteo.count
               then imax := (NbreVariab-1)
               else imax := nomMeteo.count-1;
            for i := 0 to imax do with grandeurs[i] do
                if nomMeteo[i]<>'' then nom := nomRef[i];
        end
        else stringGrid.FixedCols := 1;
     for i := 0 to pred(NbreVariab) do with grandeurs[i] do begin
         ligne := succ(i);
         trouve := false;
         for j := 0 to pred(NomRef.count) do
             if nom=nomRef[j] then begin
                nom := nomMeteo[j];
                nomUnite := uniteMeteo[j];
                fonct.expression := signifMeteo[i];
                indexMeteo[i] := j;
                trouve := true;
                break;
             end;
         if not trouve then begin
            NomRef.add(nom);
            NomMeteo.add(nom);
            UniteMeteo.add(nomUnite);
            SignifMeteo.add(fonct.expression);
            indexMeteo[i] := pred(NomRef.count);
         end;
         stringGrid.cells[1,ligne] := nom;
         stringGrid.cells[2,ligne] := nomUnite;
         stringGrid.cells[0,ligne] := fonct.expression;
     end;
end;

procedure TAffecteNomDlg.OKBtnClick(Sender: TObject);
var i,j,ligne : integer;
begin
     for i := 0 to pred(NbreVariab) do with grandeurs[i] do begin
         ligne := succ(i);
         nom := stringGrid.cells[1,ligne];
         nomUnite := stringGrid.cells[2,ligne];
         if stringGrid.FixedCols=0 then fonct.expression := stringGrid.cells[0,ligne];
         j := indexMeteo[i];
// Sauvegarde pour effectuer modif auto la prochaine fois
         IniMeteo.WriteString('NomMeteo',NomRef[j],nom);
         nomMeteo[j] := nom;
         IniMeteo.WriteString('UniteMeteo',NomRef[j],nomUnite);
         uniteMeteo[j] := nomUnite;
         IniMeteo.WriteString('SignifMeteo',NomRef[j],fonct.expression);
         signifMeteo[j] := fonct.expression;
     end;
     FichierTrie := TriCB.checked;
end;

procedure TAffecteNomDlg.StringGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
     //sendVirtualKey(vk_F2)
end;

procedure TAffecteNomDlg.FormDestroy(Sender: TObject);
begin
     NomRef.free;
     NomMeteo.free;
     UniteMeteo.free;
     SignifMeteo.free;
     IniMeteo.free;
     inherited
end;

procedure TAffecteNomDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(0)  { TODO : aide modif nom }
end;

end.
