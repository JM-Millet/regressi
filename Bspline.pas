{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

procedure CalculBSpline(const X,Y : vecteur;ordre : integer;
                        var N : integer;var Xspline,Yspline : vecteur);
// B-spline : approximation ; spline : interpolation
// B pour polyn�me d'approximation de Bernstein
// X,Y : donn�es
// Xspline,Yspline : courbe B-spline
// N : nombre de points de X � l'entr�e, de xspline � la sortie
// ordre : degr� du polynome

Const Nmax = 1024; // 1 pixel sur 2 pour �cran 2048
var
   xi : vecteur;
   Nij : array[1..ordreMaxSpline] of vecteur;
   nspl,ix : integer;  // variable de boucles
   pas : extended; //selon x pour xspline
   t : extended; // variable interm�diaire pour stocker pas*i=xspline[i]
   xx,yy : extended; // variable interm�diaire pour sommation
   e1,e2 : extended;
   i,k,j : integer; // variable de boucles

  procedure calculX;
  var i : integer;
  begin
     for i := 1 to ordre do xi[i] := 0;
     for i := ordre+1 to ordre+n do
         if i>n
            then xi[i] := N+1-ordre
            else xi[i] := i-ordre
  end;

  var coeff : extended; // variable interm�diaire
  begin // CalculBSpline
    copyVecteur(Xspline,X);
    copyVecteur(Yspline,Y);
    if ordre>ordreMaxSpline then ordre := ordreMaxSpline;
    if ordre<ordreMinSpline then ordre := ordreMinSpline;
    if (N<=ordre) or (N>=(Nmax div 2)) then exit;
    setLength(xi,N+OrdreMaxSpline+3);
    for j := 1 to ordre do setLength(Nij[j],N+OrdreMaxSpline+3);
    calculX;
    pas := (N+1-ordre)/Nmax;
    setLength(Xspline,Nmax+1);
    setLength(Yspline,Nmax+1);
    Xspline[0] := X[0];
    Yspline[0] := Y[0];
// balayage de Xspline
    ix := ordre;
    for nspl := 1 to pred(Nmax) do begin
        t := nspl*pas;
// recherche de xi
        while t>xi[succ(ix)] do inc(ix);
// xi[ix]<=t<=xi[ix+1]
// initialisation Nij
		    for j := 1 to ordre do
			      for i := 1 to n+ordre do
				        Nij[j,i] := 0;
// affectation de Ni1
  		  Nij[1,ix] := 1;
// Calcul Ni2
        try
        Coeff := 1/(xi[succ(ix)]-xi[ix]);
		    Nij[2,ix]:=(t-xi[ix])*coeff;
		    Nij[2,pred(ix)]:=(xi[succ(ix)]-t)*coeff;
        except
        end;
// Calcul Nij j>2
		    for j := 3 to ordre do begin
		       for k := ix-j+1 to ix do begin
               e1 := xi[k+j-1]-xi[k];
               if e1<>0 then e1:=(t-xi[k])*Nij[j-1,k]/e1;
               e2 := xi[k+j]-xi[k+1];
               if e2<>0 then e2:=(xi[k+j]-t)*Nij[j-1,k+1]/e2;
               Nij[j,k] := e1+e2;
		       end;// for k
		    end;// for j
// calcul de Xspline, Yspline
		    xx := 0;
		    yy := 0;
		    for k := 0 to pred(N) do begin
		       xx := X[k]*Nij[ordre,k+1]+xx;
		       yy := Y[k]*Nij[ordre,k+1]+yy;
		    end;// for k
		    Xspline[nspl] := xx;
		    Yspline[nspl] := yy;
	 end; // for nspl
   Xspline[Nmax] := X[pred(N)];
   Yspline[Nmax] := Y[pred(N)];
   N := succ(Nmax);
   xi := nil;
   for j := 1 to ordre do Nij[j] := nil;
end; // CalculBSpline

procedure InterpoleSinc(const X,Y : vecteur;ordre : integer;
                        var N : integer;var Xspline,Yspline : vecteur);
Const Nmax = 2048; // tous les pixels d'un �cran 2048
      a = 5; // cinq points � droite et � gauche soit 11 points
      ordreMaxSinc = 15;
      NijMax = (ordreMaxSinc+1)*(2*a+1); // soit 176
var
   Nij : array[0..NijMax] of double;
   i,j,k,Nsinc : integer;
   imin,imax : integer;
   yy,deltaX,w : double;
begin
    copyVecteur(Xspline,X);
    copyVecteur(Yspline,Y);
    if (N<ordreMaxSinc) then exit;
    if ordre>ordreMaxSinc then ordre := ordreMaxSinc;
    if ordre<2 then ordre := 2;
    while ((N*ordre)>Nmax) and (ordre>2) do dec(ordre);
    Nsinc := N*ordre;
    if Nsinc>Nmax then exit;
    setLength(Xspline,Nsinc+1);
    setLength(Yspline,Nsinc+1);
    Nij[0] := 1; // pour �viter div par 0
		for j := 1 to NijMax do begin // calcul du noyau
        yy := pi*j/ordre;
        Nij[j] := sin(yy)*sin(yy/a)*a/sqr(yy);
    end;
    deltaX := (X[N-1]-X[0])/(N-1)/ordre;
// calcul de Xspline, Yspline
    for j := 0 to Nsinc-1 do begin
        k := round(j/ordre);
        Xspline[j] := X[0]+j*deltaX;
        imin := k-a;
        if imin<0 then imin := 0;
        imax := imin+2*a; // 2*a+1 points
        if imax>=N then begin
           imax := N-1;
           imin := imax-2*a;
           if imin<0 then imin := 0;
        end;
        yy := 0;
        w := 0;
        for i := imin to imax do begin
            yy := yy + Y[i]*Nij[abs(i*ordre-j)];
            w := w + Nij[abs(i*ordre-j)];
        end;
	   	  Yspline[j] := yy/w;
    end;
    N := Nsinc;
end; // InterpoleSinc

procedure RemoveGlitche(Y,Ylisse : vecteur;ordre,N : integer);
var
   ordreG,j : integer;
   ecart,coeff : extended;
   Yg,dY : vecteur;
begin
   setLength(dy,N+1);
   ordreG := ordre div 3;
   if ordreG<3 then ordreG := 3;
   coeff := 1/ordreG;
   copyVecteur(Yg,Y);
   ecart := 0;
   for j := 1 to N-1 do dY[j] := y[j]-y[j-1];
   for j := 1 to 2*ordreG do ecart := ecart+abs(dY[j]);
   for j := ordreG to N-ordreG-1 do begin
       ecart := ecart+abs(dy[j+ordreG])-abs(dy[j-ordreG])-abs(dy[j])-abs(dy[j+1]);
       if (dy[j]*dy[j+1]<0) and
          (abs(dy[j])>ecart*coeff) and
          (abs(dy[j+1])>ecart*coeff) then begin
              Yg[j] := (y[j+1]+yg[j-1])/2;
              dy[j] := yg[j]-yg[j-1];
              dy[j+1] := y[j+1]-yg[j];
       end;
       ecart := ecart+abs(dy[j])+abs(dy[j+1]);
   end;
   for j := N-ordreG to N-2 do begin
       ecart := ecart-dy[j]-dy[j+1];
       if (dy[j]*dy[j+1]<0) and
          ((abs(dy[j])>ecart*coeff) or
           (abs(dy[j+1])>ecart*coeff)) then begin
              Yg[j] := (y[j+1]+yg[j-1])/2;
              dy[j] := yg[j]-yg[j-1];
              dy[j+1] := y[j+1]-yg[j];
       end;
       ecart := ecart+dy[j]+dy[j+1];
   end;
   if (abs(dy[N-1])>abs(dy[N-2])*1.5) or
      (abs(dy[N-1])<abs(dy[N-2])*0.5) then
        Yg[N-1] := Yg[N-2]+dY[N-2];
   ecart := 0;
   for j := 1 to 2*ordreG do ecart := ecart+dY[j];
   for j := 1 to ordreG-1 do begin
       ecart := ecart-dy[j]-dy[j+1];
       if (dy[j]*dy[j+1]<0) and
          ((abs(dy[j])>ecart*coeff) or
           (abs(dy[j+1])>ecart*coeff)) then begin
              Yg[j] := (y[j+1]+yg[j-1])/2;
              dy[j] := yg[j]-yg[j-1];
              dy[j+1] := y[j+1]-yg[j];
       end;
       ecart := ecart+dy[j]+dy[j+1];
   end;
   if (abs(dy[1])>abs(dy[2])*1.5) or
      (abs(dy[1])<abs(dy[2])*0.5) then
        Yg[0] := Yg[1]-dY[2];
   copyVecteur(Ylisse,yg);
   yg := nil;
   dy := nil;
end; // RemoveGlitche

procedure MoyGlissante(Y,Ylisse : vecteur;ordre,N : integer);
const OrdreMax = 32;
      OrdreMin = 1;
var j : integer;
    a : extended;
begin  // MoyGlissante
   if ordre>ordreMax then ordre := ordreMax;
   if ordre<ordreMin then ordre := ordreMin;
   RemoveGlitche(Y,Ylisse,ordre,N);
   a := exp(-1/ordre);
   Ylisse[0] := Y[0];
   for j := 1 to pred(N) do
       Ylisse[j] := Y[j]*(1-a)+a*Ylisse[j-1];
 // (1-a) de mani�re � avoir une r�ponse de 1 � une entr�e de 1
end; // MoyGlissante

procedure MoyCentree(Y,Ylisse : vecteur;ordre,N : integer);
const OrdreMax = 32;
      OrdreMin = 1;
var j,k,indexCourant : integer;
begin  // MoyCentree
   if ordre>ordreMax then ordre := ordreMax;
   if ordre<ordreMin then ordre := ordreMin;
   RemoveGlitche(Y,Ylisse,ordre,N);
   for j := 0 to pred(ordre) do begin
       Ylisse[j] := 0;
       for k := 0 to 2*j do
          Ylisse[j] := Ylisse[j]+Y[k];
       Ylisse[j] := Ylisse[j]/(2*j+1);
   end;
   for j := ordre to pred(N-ordre) do begin
       Ylisse[j] := 0;
       for k := -ordre to ordre do
           Ylisse[j] := Ylisse[j]+Y[j+k];
       Ylisse[j] := Ylisse[j]/(2*ordre+1);
   end;
   for j := 0 to pred(ordre) do begin
       indexCourant := j+N-ordre;
       Ylisse[indexCourant] := 0;
       for k := -j to j do
           Ylisse[indexCourant] := Ylisse[indexCourant]+Y[indexCourant+k];
       Ylisse[indexCourant] := Ylisse[indexCourant]/(2*j+1);
   end;
end; // MoyCentree

Procedure CalculIntLineaire(const X,Y : vecteur;Debut,Fin,NbreLisse : integer;
          var XLisse,YLisse : vecteur);
var
      pas,t : extended;
      i,ix : integer;
begin
      pas := (X[fin]-X[debut])/pred(NbreLisse);
      Xlisse[0] := X[debut];
      Ylisse[0] := Y[debut];
      Ylisse[pred(NbreLisse)] := Y[fin];
      Xlisse[pred(NbreLisse)] := X[fin];
      ix := debut;
      for i := 1 to NbreLisse-2 do begin
          t := X[debut]+i*pas;
          Xlisse[i] := t;
// recherche de ix
	  while t>x[succ(ix)] do inc(ix);
// x[ix]<t<=x[ix+1]
          try
	      Ylisse[i] := Y[ix]+(t-X[ix])*(Y[succ(ix)]-Y[ix])/(X[succ(ix)]-X[ix]);
          except
          Ylisse[i] := Nan;
          end
      end; // for i
// interpolation � gauche
      try
      Ylisse[NbreLisse] := Y[fin]+pas*(Y[pred(fin)]-Y[fin])/(X[pred(fin)]-X[fin]);
      except
      Ylisse[NbreLisse] := Y[fin];
      end;
      Xlisse[NbreLisse] := X[fin]+pas;
end; // CalculIntLineaire 



