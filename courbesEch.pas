{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit courbesEch;

  {$MODE Delphi}

interface

uses SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, Spin,
     CourbesU, regutil;

type
  TEchelleBmpDlg = class(TForm)
    GroupBox1: TGroupBox;
    EditY: TEdit;
    LabelX: TLabel;
    LabelY: TLabel;
    EditX: TEdit;
    UniteX: TEdit;
    UniteY: TEdit;
    Label2: TLabel;
    LabelEchelle: TLabel;
    Label4: TLabel;
    NomX: TEdit;
    NomY: TEdit;
    LabelOrigine: TLabel;
    EditX0: TEdit;
    EditY0: TEdit;
    Label7: TLabel;
    logxcb: TCheckBox;
    logycb: TCheckBox;
    OKBtn: TBitBtn;
    BitBtn2: TBitBtn;
    PolaireCB: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure PolaireCBClick(Sender: TObject);
  private
  public
  end;

var
  EchelleBmpDlg: TEchelleBmpDlg;

implementation

  {$R *.lfm}

procedure TEchelleBmpDlg.FormActivate(Sender: TObject);
begin with courbesForm do begin
      NbreSE.MaxValue := maxObjet;
      EditX.text := FloatToStrF(MaxiX,ffGeneral,5,0);
      EditY.text := FloatToStrF(MaxiY,ffGeneral,5,0);
      EditX0.text := FloatToStrF(MiniX,ffGeneral,5,0);
      EditY0.text := FloatToStrF(MiniY,ffGeneral,5,0);
      logxcb.checked := echelleX in [elog];
      logycb.checked := echelleY in [elog];
end end;

procedure TEchelleBmpDlg.OKBtnClick(Sender: TObject);
begin with courbesForm do begin
      echelleX := elin;
      echelleY := elin;
      if logxcb.checked and not polaireCB.checked then echelleX := elog;
      if logycb.checked and not polaireCB.checked then echelleY := elog;
      if polaireCB.checked then begin
         echelleX := ePolaire;
         echelleY := ePolaire;
      end;

      try
      MaxiX := StrToFloatWin(EditX.text);
      except
         MaxiX := 1;
      end;
      if (echelleX=elog) and (maxiX<=0) then maxiX := 1;
      try
      MiniX := StrToFloatWin(EditX0.text);
      except
         MiniX := 0;
      end;
      if (echelleX=elog) and (miniX<=0) then miniX := 2;
      if maxiX=miniX then maxiX := miniX+1;

      try
      MaxiY := StrToFloatWin(EditY.text);
      except
         MaxiY := 1;
      end;
      if (echelleY=elog) and (maxiY<=0) then maxiY := 1;
      try
      MiniY := StrToFloatWin(EditY0.text);
      except
         MiniY := 0;
      end;
      if (echelleY=elog) and (miniY<=0) then miniY := 2;
      if maxiY=miniY then maxiY := miniY+1;
end end;

procedure TEchelleBmpDlg.PolaireCBClick(Sender: TObject);
begin
  inherited;
  if polaireCB.Checked then begin
     labelX.caption := 'angle';
     labelY.caption := 'rayon';
  end
  else begin
     labelX.caption := 'axe horizontal';
     labelY.caption := 'axe vertical';
  end;
  logxcb.Visible := not  polaireCB.Checked;
  logycb.Visible := not  polaireCB.Checked;
end;

end.
