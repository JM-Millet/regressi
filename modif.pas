{  
This file is part of Regressi, software
    Copyright (C) 2024  Jean-Michel Millet
    Contact: regressi@orange.fr
    Web site: https://regressi.fr/WordPress

Regressi is distribued under the terms of the GNU General Public License.

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the 
Fre Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

unit modif;

  {$MODE Delphi}

interface

uses Classes, Graphics, Forms, Controls, Buttons,
  StdCtrls, ExtCtrls, Spin, sysutils, ComCtrls, strUtils, math,
  constreg, regutil, uniteker, compile, aideKey;

type
  TModifDlg = class(TForm)
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    HelpBtn: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    EditNom: TEdit;
    EditUnite: TEdit;
    PrecisionCB: TComboBox;
    Label4: TLabel;
    Bevel1: TBevel;
    RazIncertitudeBtn: TButton;
    AffSignifCB: TCheckBox;
    GenreLabel: TLabel;
    LabelPrecision: TLabel;
    PrecisionSE: TSpinEdit;
    CalculExpGB: TGroupBox;
    ExpressionEdit: TLabeledEdit;
    Memo1: TMemo;
    CalculVersExpCB: TCheckBox;
    Image2: TImage;
    EditComm: TLabeledEdit;
    EditIncertitude: TLabeledEdit;
    IncertitudeHelpBtn: TSpeedButton;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure PrecisionCBChange(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure RazIncertitudeBtnClick(Sender: TObject);
    procedure IncertitudeHelpBtnClick(Sender: TObject);
  private
  public
      grandeurModif : Tgrandeur;
      index : integer;
      oldFormatU : TnombreFormat;
  end;

var
  ModifDlg: TModifDlg;

implementation

uses Valeurs, graphvar, AideIncertitudeU;

  {$R *.lfm}

procedure TModifDlg.FormActivate(Sender: TObject);
begin
inherited;
if index=cFrequence
   then grandeurModif := grandeurs[cFrequence]
   else if index<maxGrandeurs then
        grandeurModif := grandeurs[index];
with grandeurModif do begin
     EditNom.text := nom;
     EditNom.enabled := fonct.genreC=g_experimentale;
     CalculExpGB.visible := (fonct.genreC=g_experimentale) and (genreG=variable);
     if (modeAcquisition=AcqSimulation) and
        (index=0) then begin
           CalculExpGB.visible := false;
           GenreLabel.caption := 'Variable de contrôle de la simulation'
        end
        else GenreLabel.caption := NomGenreGrandeur[genreG]+' '+nomGenreCalcul[fonct.genreC];
     CalculVersExpCB.visible := (fonct.genreC=g_fonction) and (genreG=variable);
     CalculVersExpCB.checked := false;
     expressionEdit.Text := '';
     EditUnite.text := nomUnite;
     EditUnite.Enabled := fonct.genreC=g_experimentale;
     EditUnite.Visible := (fonct.genreC<>g_texte);
     EditComm.text := fonct.expression;
     EditComm.enabled := EditNom.enabled;
     PrecisionCB.itemIndex := ord(formatU);
     oldFormatU := formatU;     
     PrecisionSE.value := precisionU;
     PrecisionCBChange(nil);
     EditIncertitude.text := incertCalc.expression;
     EditIncertitude.visible := (index<maxGrandeurs) and
                                (fonct.genreC=g_experimentale);
     RazIncertitudeBtn.visible := EditIncertitude.visible;
     AffSignifCB.checked := AffSignif;
end end;

procedure TModifDlg.OKBtnClick(Sender: TObject);

Procedure SetNom;
var newNom,oldNom : String;
    longueurNom : integer;

Function ModifExpression(tampon : String) : String;
var posNom,suiteNom : integer;
    debutCorrect,FinCorrect : boolean;
begin
     posNom := PosEx(oldNom,tampon,0);
     while posNom>0 do begin
           debutCorrect := (posNom=1) or
               charinset(tampon[pred(posNom)],['=',' ','(','''','*','+','-','/']);
           suiteNom := posNom+LongueurNom;
           finCorrect := (suiteNom=length(tampon)+1) or
                  charinset(tampon[suiteNom],['*','+','-','/',':',')','^',',']);
           if debutCorrect and finCorrect then begin
                delete(tampon,posNom,LongueurNom);
                insert(NewNom,tampon,posNom);
           end;
           posNom := PosEx(oldNom,tampon,posNom);
      end;
      result := tampon;
end;

var i : integer;
begin
       oldNom := grandeurModif.nom;
       newNom := editNom.text;
       LongueurNom := length(oldNom);
       for i := 0 to pred(Fvaleurs.memo.lines.count) do
           Fvaleurs.memo.lines[i] := ModifExpression(Fvaleurs.memo.lines[i]);
       for i := 0 to pred(NbreGrandeurs) do with grandeurs[i] do
           incertCalc.expression := ModifExpression(incertCalc.expression);
       grandeurModif.nom := newNom;
       if index<MaxGrandeurs then begin
          Application.MainForm.Perform(WM_reg_maj,MajNom,index);
          oldFormatU := grandeurModif.formatU; { mise à jour faite }
       end;
end;

Function SetUnites : boolean;
var U : Tunite;
    UniteOK : boolean;
    i : integer;
begin
    result := true;
    if grandeurModif.nomUnite<>EditUnite.Text then begin
          U := Tunite.create;
          U.NomUnite := editUnite.text;
          UniteOK := U.correct;
          U.free;
          if not UniteOK and not OKReg(OkUniteInconnue,HELP_Unites) then begin
             EditUnite.setFocus;
             result := false;
             ModalResult := mrNone;
             exit;
          end;
          grandeurModif.NomUnite := EditUnite.text;
          if (Index<>cFrequence)  then
             for i := 0 to pred(NbreGrandeurs) do with grandeurs[i] do
                 if (nomUnite='') or not uniteDonnee then SetUnite;
          if index>maxGrandeurs
             then Application.MainForm.Perform(WM_Reg_Maj,MajUnitesParam,0)
             else Application.MainForm.Perform(WM_Reg_Maj,MajUnites,0);
     end;
end; // SetUnites

Function SetIncertitude : boolean;
var posErreur,LongErreur,k : integer;
    p : codePage;
begin
    result := true;
    if index=cFrequence then exit;
    if (index<maxGrandeurs) and
       (grandeurModif.IncertCalc.expression<>editIncertitude.text) then begin
           grandeurModif.IncertCalc.expression := editIncertitude.text;
           TrimComplet(grandeurModif.IncertCalc.expression);
           if grandeurModif.compileIncertitude(index,posErreur,LongErreur) then begin
                  for p := 1 to NbrePages do begin
                      for k := index to pred(NbreGrandeurs) do
                          pages[p].recalculIncert(k);
                  end;
                  Application.MainForm.Perform(WM_Reg_Maj,MajIncertitude,0)
              end
              else begin
                 afficheErreur(codeErreurC,0);
                 editIncertitude.setFocus;
                 result := false;
                 ModalResult := mrNone;
              end;
       end;
end; // SetIncertitude

procedure remplir;
var posErreur,longErreur,i : integer;
begin with pages[pageCourante] do begin
    grandeurImmediate.fonct.expression := expressionEdit.Text;
    if grandeurImmediate.compileG(posErreur,longErreur,0) and
       (grandeurImmediate.fonct.calcul<>nil)
        then begin
             affecteConstParam;
             affecteVariableP(false);
             for i := 0 to pred(nmes) do begin
                 affecteVariableE(i);
                 try
                     valeurVar[index,i] :=  calcule(grandeurImmediate.fonct.calcul);
                 except
                 on E:exception do valeurVar[index,i] := Nan;
                 end;// except
             end; // for i
        end
        else afficheErreur('Erreur dans l''expression',0);
end end;

procedure convertirExp;
var zz : String;
    i : integer;
begin
     libere(grandeurModif.fonct.calcul);
     grandeurModif.fonct.genreC := g_experimentale;
     zz := grandeurModif.nom+'=';
     for i := 0 to Fvaleurs.memo.lines.Count - 1 do
         if pos(zz,Fvaleurs.memo.lines[i])=1 then begin
            Fvaleurs.Memo.lines.delete(i);
            break;
         end;
end;

begin
     if not setIncertitude then exit;
     if not setUnites then exit;
     if (expressionEdit.Text<>'') and
        OKreg('Calcul d''une grandeur expérimentale ?!',0) then remplir;
     if calculVersExpCB.checked and
        OKreg('Conversion d''une grandeur calculée en grandeur expérimentale ?!',0) then convertirExp;
     with grandeurModif do begin
         formatU := TnombreFormat(PrecisionCB.itemIndex);
         if editNom.enabled and (nom<>EditNom.Text) then setNom;
         nom := EditNom.Text;
         fonct.expression := EditComm.text;
         precisionU := precisionSE.value;
         AffSignif := AffSignifCB.checked;
     end;
     if grandeurModif.formatU<>oldFormatU then
        FgrapheVariab.Perform(WM_Reg_Maj,MajNom,0);
end;

procedure TModifDlg.PrecisionCBChange(Sender: TObject);
const
     text : array[1..5] of string =
          ('chiffres','décimales','chiffres',
           'bits','bits');
     Min : array[1..5] of integer = (2,0,2,1,1);
     Max : array[1..5] of integer = (12,12,12,8,8);
begin
     LabelPrecision.visible := (PrecisionCB.itemIndex>0) and
                               (PrecisionCB.itemIndex<6);
     PrecisionSE.visible := LabelPrecision.visible;
     if LabelPrecision.visible then begin
         LabelPrecision.caption := 'Nombre de '+text[PrecisionCB.itemIndex];
         PrecisionSE.minValue := min[PrecisionCB.itemIndex];
         PrecisionSE.maxValue := max[PrecisionCB.itemIndex];
     end;
end;

procedure TModifDlg.HelpBtnClick(Sender: TObject);
begin
     Application.HelpContext(HELP_Grandeurs)
end;

procedure TModifDlg.IncertitudeHelpBtnClick(Sender: TObject);
begin
  inherited;
  if AideIncertitudeForm=nil then
     Application.CreateForm(TAideIncertitudeForm, AideIncertitudeForm);
  AideIncertitudeForm.pageControl1.activePage := AideIncertitudeForm.tabSheet1;
  AideIncertitudeForm.show;
end;

procedure TModifDlg.RazIncertitudeBtnClick(Sender: TObject);
var p : codePage;
    j : integer;
begin
     if index>=maxGrandeurs then exit;
     grandeurModif.IncertCalc.expression := '';
     editIncertitude.text := '';
     for p := 1 to NbrePages do with pages[p] do
         case grandeurModif.genreG of
              variable : for j := 0 to pred(nmes) do
                  incertVar[index,j] := Nan;
              constante : incertConst[index] := Nan;
              constanteGlb : grandeurModif.incertCourante := Nan;
         end;
end;

end.
